﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumast.Claro.Vantive.DataAcces.Abstract
{
    public interface ITareasVantiveDataRepository
    {
        IEnumerable<IVantiveDataModel> SelectAll();
    }
}
