﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Jumast.Sisifo.Coordinacion.ViewModels
{
    public class CoordinacionesDataGridViewModel : INotifyPropertyChanged, IEnumerable<CoordinacionViewModel.CoordinacionViewModel>
    {
        public ObservableCollection<CoordinacionViewModel.CoordinacionViewModel> Coordinaciones { get; set; } = new ObservableCollection<CoordinacionViewModel.CoordinacionViewModel>();

        private CoordinacionViewModel.CoordinacionViewModel _selectedCoordinacion;
        public CoordinacionViewModel.CoordinacionViewModel SelectedCoordinacionViewModel
        {
            get { return _selectedCoordinacion; }
            set
            {
                _selectedCoordinacion = value;
                OnPropertyChanged();
            }
        }

        public void Add(CoordinacionViewModel.CoordinacionViewModel coordinacionViewModel)
        {
            Coordinaciones.Add(coordinacionViewModel);
        }

        public void Remove(CoordinacionViewModel.CoordinacionViewModel coordinacionViewModel)
        {
            Coordinaciones.Remove(coordinacionViewModel);
        }


        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public IEnumerator<CoordinacionViewModel.CoordinacionViewModel> GetEnumerator()
        {
            return Coordinaciones.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
