﻿using System;
using System.Collections.Generic;
using Jumast.Sisifo.Contratista.DataAcces;
using Jumast.Sisifo.Coordinacion.Common;
using Jumast.Wpf.Mvvm.BindableProperties;

namespace Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel
{
    public class CoordinacionViewModelBuilder :
        IWithCliente,
        IWithDireccion,
        IWithPartido,
        IWithEnlace,
        IWithTipoDeCoordinacion,
        IWithContratista,
        IWithFecha,
        IWithEstado,
        IWithTareaPendiente,
        IWithCoordinador,
        IWithAnalistaMdD,
        IWithResponsable,
        IWithResultado,
        IWithResponsableResultado,
        IWithObservacionesResultado,
        IBuild
    {
        private readonly CoordinacionViewModel _product;

        private CoordinacionViewModelBuilder()
        {
            _product = new CoordinacionViewModel();
        }

        public static IWithCliente Init(ICoordinadoresRepository coordinadoresRepo)
        {

            var builder = new CoordinacionViewModelBuilder();
            builder.Coordinadores = coordinadoresRepo.NombresDeSupervisores();
            return builder;

        }

        public IWithDireccion WithCliente(string cliente)
        {
            _product.ClienteProperty = new ReadOnlyProperty<string>(cliente);
            return this;
        }


        public IWithPartido WithDireccion(string direccion)
        {
            _product.DireccionProperty = new ReadOnlyProperty<string>(direccion);
            return this;
        }

        public IWithEnlace WithPartido(string partido)
        {
            _product.PartidoProperty = new ReadOnlyProperty<string>(partido);
            return this;
        }

        public List<string> Coordinadores { get; set; }

        public IWithTipoDeCoordinacion WithEnlace(string id)
        {
            _product.NumerosDeEnlaceProperty = new ReadWriteProperty<string>(id);
            return this;
        }

        public IWithContratista WithTipoDeCoordinacion(TipoDeCoordinacionEnum tipoDeCoordinacion)
        {
            _product.TipoDeTareaProperty = new TipoDeTareaEnumProperty(tipoDeCoordinacion);
            return this;
        }

        public IWithFecha WithContratista(string contratista)
        {
            _product.ContratistaProperty = new ContratistaProperty(contratista);
            return this;
        }

        public IWithEstado WithFecha(DateTime? fecha)
        {
            _product.FechaProperty = new ReadWriteProperty<DateTime?>(fecha);
            return this;
        }

        public IWithTareaPendiente WithEstado(EstadoDeCoordinacionEnum estado)
        {
            var estadoProperty = new EstadoDeCoordinacionEnumProperty(estado);
            _product.EstadoProperty = estadoProperty;
            return this;
        }

        public IWithCoordinador WithTareaPendiente(string tareaPendiente)
        {
            _product.TareaPendienteProperty = new TareaPendienteParaCoordinacionProperty(tareaPendiente);
            return this;
        }

        public IWithAnalistaMdD WithCoordinador(string coordinador)
        {
            _product.CoordinadorProperty = new CoordinadorProperty(coordinador, Coordinadores);
            return this;
        }

        public IWithResponsable WithAnalistaMdD(string analistaMdD)
        {
            _product.AnalistaMdDProperty = new ReadOnlyProperty<string>(analistaMdD);
            return this;
        }

        public IWithResultado WithResponsable(ResponsableDeCoordinacionEnum responsable)
        {
            _product.ResponsableProperty = new ResponsableCoordinacionEnumProperty(responsable);
            return this;
        }

        public IWithResponsableResultado WithResultado(string resultado)
        {
            _product.ResultadoProperty = new ResultadoDeCoordinacionProperty(resultado);
            return this;
        }

        public IWithObservacionesResultado WithResponsableResultado(string responsableResultado)
        {
            _product.ResponsableResultadoProperty = new ResponsableDeResultadoDeCoordinacionProperty(responsableResultado);
            return this;
        }

        public IBuild WithObservacionesResultado(string observacionesResultado)
        {
            _product.ObservacionesResultadoProperty = new ReadWriteProperty<string>(observacionesResultado);
            return this;
        }

        public CoordinacionViewModel Build()
        {
            _product.EstadoProperty.PropertyChanged += (sender, args) =>
            {
                _product.ResponsableProperty.Value = ResponsableDeCoordinacionEnum.none;
            };

            _product.ResponsableProperty.PropertyChanged += (sender, args) =>
            {
                _product.TareaPendienteProperty.Value = "";
            };

            _product.ResponsableProperty.AllowedValueDependsOn(_product.EstadoProperty);
            _product.TareaPendienteProperty.AllowedValuesDependOn(_product.EstadoProperty, _product.ResponsableProperty);

            var descripcion = $"{_product.ClienteProperty.Value} ({_product.DireccionProperty.Value}, {_product.PartidoProperty.Value}) Enlace:{_product.NumerosDeEnlaceProperty.Value}";
            _product.DescripcionProperty = new ReadOnlyProperty<string>(descripcion);
            return _product;
        }

    }

}
