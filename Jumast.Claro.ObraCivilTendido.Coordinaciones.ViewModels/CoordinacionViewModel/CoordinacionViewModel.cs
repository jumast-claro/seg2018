﻿using System;
using System.Runtime.Remoting.Messaging;
using Jumast.Sisifo.Coordinacion.Common;
using Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataModel;
using Jumast.Wpf.Mvvm.BindableProperties;

namespace Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel
{

    public interface ICoordinacionViewModel
    {

        IBindableProperty<string> Cliente { get; }

        IEnumBindableProperty<TipoDeCoordinacionEnum> TipoDeTareaProperty { get; }
        IEnumBindableProperty<EstadoDeCoordinacionEnum> EstadoProperty { get; }
        IEnumBindableProperty<ResponsableDeCoordinacionEnum> ResponsableProperty { get; }
        TareaPendienteParaCoordinacionProperty TareaPendienteProperty { get; }
        IBindableProperty<DateTime?> FechaProperty { get; }
        ContratistaProperty ContratistaProperty { get; }
        CoordinadorProperty CoordinadorProperty { get; }
        ResultadoDeCoordinacionProperty ResultadoProperty { get; }
        ResponsableDeResultadoDeCoordinacionProperty ResponsableResultadoProperty { get; }
        IBindableProperty<string> ObservacionesResultadoProperty { get; }
        IBindableProperty<string> NumerosDeEnlaceProperty { get; }
    }

    public class CoordinacionViewModel 
    {
        //-------------------------------------------------------------------//
        // Constructor
        //-------------------------------------------------------------------//
        internal CoordinacionViewModel()
        {
            IdProperty = new ReadWriteProperty<string>("");

            AnioProperty = new LambaBindbableProperty<int>((() => FechaProperty.Value?.Year ?? 0));
            MesProperty = new LambaBindbableProperty<int>((() => FechaProperty.Value?.Month ?? 0));
            DiaProperty = new LambaBindbableProperty<int>((() => FechaProperty.Value?.Day ?? 0));

            PeriodoProperty = new LambaBindbableProperty<string>(() =>
                {
                    var tieneFecha = FechaProperty.Value.HasValue;
                    if (tieneFecha)
                    {
                        var fecha = FechaProperty.Value.Value;
                        var hoy = DateTime.Today;
                        var ayer = hoy.AddDays(-1);
                        var maniana = hoy.AddDays(1);

                        if (fecha < ayer) return "1-Pasado";
                        if (fecha.Year == ayer.Year && fecha.Month == ayer.Month && fecha.Day == ayer.Day) return "2-Ayer";
                        if (fecha.Year == maniana.Year && fecha.Month == maniana.Month && fecha.Day == maniana.Day) return "4-Mañana";
                        if (fecha > maniana) return "5-Futuro";
                        if (fecha.Year == hoy.Year && fecha.Month == hoy.Month && fecha.Day == hoy.Day) return "3-Hoy";
                    }

                    return "6-Pendiente";

                }
            
            
            );

        }

        public IBindableProperty<string> IdProperty { get; internal set; }
        public IBindableProperty<string> PeriodoProperty { get; internal set; }

        public IBindableProperty<string> ClienteProperty { get; internal set; }
        public IBindableProperty<string> DireccionProperty { get; internal set; }
        public IBindableProperty<string> PartidoProperty { get; internal set; }
        public IBindableProperty<string> AnalistaMdDProperty { get; internal set; }

        public IBindableProperty<int> AnioProperty { get; }
        public IBindableProperty<int> MesProperty { get; }
        public IBindableProperty<int> DiaProperty { get; }

        public TipoDeTareaEnumProperty TipoDeTareaProperty { get; set; }
        public EstadoDeCoordinacionEnumProperty EstadoProperty { get; internal set; }
        public ResponsableCoordinacionEnumProperty ResponsableProperty { get; set; }
        public TareaPendienteParaCoordinacionProperty TareaPendienteProperty { get; internal set; }
        public IBindableProperty<DateTime?> FechaProperty { get; set; }
        public ContratistaProperty ContratistaProperty { get; internal set; }
        public CoordinadorProperty CoordinadorProperty { get; internal set; }
        public ResultadoDeCoordinacionProperty ResultadoProperty { get; internal set; }
        public ResponsableDeResultadoDeCoordinacionProperty ResponsableResultadoProperty { get; internal set; }
        public IBindableProperty<string> ObservacionesResultadoProperty { get; internal set; }
        public IBindableProperty<string> NumerosDeEnlaceProperty { get; internal set; }

        public IBindableProperty<string> DescripcionProperty { get; internal set; }


        public CoordinacionDataModel GetDataModel()
        {
            var coordinacionViewModel = this;
            var coordinacionDataModelBuilder = CooordinacionDataModelBuilder.Init()
                .WithId(coordinacionViewModel.IdProperty.Value)
                .WithEnlace(coordinacionViewModel.NumerosDeEnlaceProperty.Value)
                .WithTipoDeCoordinacion(coordinacionViewModel.TipoDeTareaProperty.Value)
                .WithContratista(coordinacionViewModel.ContratistaProperty.Value)
                .WithFecha(coordinacionViewModel.FechaProperty.Value)
                .WithEstado(coordinacionViewModel.EstadoProperty.Value)
                .WithTareaPendiente(coordinacionViewModel.TareaPendienteProperty.Value)
                .WithCoordinador(coordinacionViewModel.CoordinadorProperty.Value)
                .WithResponsable(coordinacionViewModel.ResponsableProperty.Value)
                .WithResultado(coordinacionViewModel.ResultadoProperty.Value)
                .WithResponsableResultado(coordinacionViewModel.ResponsableResultadoProperty.Value)
                .WithObservacionesResultado(coordinacionViewModel.ObservacionesResultadoProperty.Value);

            var coordinacionDataModel = coordinacionDataModelBuilder.Build();

            return coordinacionDataModel;
        }
    }
}
