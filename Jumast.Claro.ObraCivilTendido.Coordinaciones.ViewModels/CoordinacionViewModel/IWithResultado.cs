namespace Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel
{
    public interface IWithResultado
    {
        IWithResponsableResultado WithResultado(string resultado);
    }
}