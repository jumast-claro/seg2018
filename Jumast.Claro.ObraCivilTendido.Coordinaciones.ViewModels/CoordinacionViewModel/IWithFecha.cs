using System;

namespace Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel
{
    public interface IWithFecha
    {
        IWithEstado WithFecha(DateTime? fecha);
    }
}