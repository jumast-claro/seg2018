using Jumast.Sisifo.Coordinacion.Common;

namespace Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel
{
    public interface IWithTipoDeCoordinacion
    {
        IWithContratista WithTipoDeCoordinacion(TipoDeCoordinacionEnum tipoDeCoordinacion);
    }
}