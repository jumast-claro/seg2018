namespace Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel
{
    public interface IWithTareaPendiente
    {
        IWithCoordinador WithTareaPendiente(string tareaPendiente);
    }
}