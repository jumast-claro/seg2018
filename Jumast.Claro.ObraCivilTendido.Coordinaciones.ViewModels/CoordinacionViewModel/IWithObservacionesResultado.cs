namespace Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel
{
    public interface IWithObservacionesResultado
    {
        IBuild WithObservacionesResultado(string observacionesResultado);
    }
}