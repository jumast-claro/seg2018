namespace Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel
{
    public interface IWithEnlace
    {
        IWithTipoDeCoordinacion WithEnlace(string id);
    }

    public interface IWithCliente
    {
        IWithDireccion WithCliente(string cliente);
    }

    public interface IWithDireccion
    {
        IWithPartido WithDireccion(string direccion);
    }

    public interface IWithPartido
    {
        IWithEnlace WithPartido(string partido);
    }
}