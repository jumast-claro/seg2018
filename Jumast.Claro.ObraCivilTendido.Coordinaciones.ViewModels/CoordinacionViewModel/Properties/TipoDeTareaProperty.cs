﻿using Jumast.Sisifo.Coordinacion.Common;
using Jumast.Wpf.Mvvm.BindableProperties;

namespace Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel
{

    public sealed class TipoDeTareaEnumProperty : EnumBindableProperty<TipoDeCoordinacionEnum>
    {
        private readonly TipoDeTareaEnumConverter _enumConverter = new TipoDeTareaEnumConverter();

        public TipoDeTareaEnumProperty(TipoDeCoordinacionEnum initialVvalue) : base(initialVvalue)
        {
        }

        protected override string EnumToString(TipoDeCoordinacionEnum @enum)
        {
            return _enumConverter.EnumToString(@enum);
        }

        protected override TipoDeCoordinacionEnum StringToEnum(string s)
        {
            return _enumConverter.StringToEnum(s);
        }
    }
}