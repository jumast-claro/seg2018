﻿using System.Collections.Generic;
using Jumast.Sisifo.Coordinacion.Common;
using Jumast.Wpf.Mvvm.BindableProperties;

namespace Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel
{

    public sealed class EstadoDeCoordinacionEnumProperty : EnumBindableProperty<EstadoDeCoordinacionEnum>
    {

        private readonly EstadoDeCoordinacionEnumConverter _estadoDeCoordinacionEnumConverter = new EstadoDeCoordinacionEnumConverter();

        public EstadoDeCoordinacionEnumProperty(EstadoDeCoordinacionEnum initialValue) : base(initialValue)
        {
        }

        protected override string EnumToString(EstadoDeCoordinacionEnum @enum)
        {
            return _estadoDeCoordinacionEnumConverter.ToString(@enum);
        }

        protected override EstadoDeCoordinacionEnum StringToEnum(string s)
        {
            return _estadoDeCoordinacionEnumConverter.ToEnum(s);
        }
    }
}