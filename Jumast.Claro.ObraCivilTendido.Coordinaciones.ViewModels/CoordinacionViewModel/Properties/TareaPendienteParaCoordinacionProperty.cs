﻿using System;
using System.Collections.Generic;
using Jumast.Sisifo.Coordinacion.Common;
using Jumast.Wpf.Mvvm.BindableProperties;

namespace Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel
{
    public sealed class TareaPendienteParaCoordinacionProperty : ReadWritePropertyWithRestrictedValues<string>
    {
        private EstadoDeCoordinacionEnumProperty _estadoProperty;
        private ResponsableCoordinacionEnumProperty _responsableProperty;


        public TareaPendienteParaCoordinacionProperty(string initialVvalue) : base(initialVvalue)
        {
        }

        public override IEnumerable<string> AllowedValues
        {
            get
            {
                var estado = _estadoProperty.Value;
                var responsable = _responsableProperty.Value;
                //var tareaPendienteList = _tareaPendienteItemsSource.ContainsKey(responsable) ? _tareaPendienteItemsSource[responsable] : new List<string>() { "" };
                //return tareaPendienteList;

                switch (estado)
                {
                    case EstadoDeCoordinacionEnum.Pendiente:
                        switch (responsable)
                        {
                            case ResponsableDeCoordinacionEnum.MdD: case ResponsableDeCoordinacionEnum.PM: case ResponsableDeCoordinacionEnum.Cliente:
                                return new List<string>(){"", "Primer contacto con el cliente", "Informar requisitos", "Autorización del cliente", "Sitio en construcción", "Sala de rack en construcción", "Provisión de rack"};
                            case ResponsableDeCoordinacionEnum.InstalacionesFO:
                                return new List<string>(){"", "Asignar a contratista", "Analizar informe", "Solicitar documentación", "Enviar documentación", "Primer contacto con el cliente"};
                            case ResponsableDeCoordinacionEnum.Contratista:
                                return new List<string>() {"", "Coordinar", "Iniciar ejecución", "Asignar relevador", "Proponer fecha", "Confirmar fecha", "Nómina de personal", "Enviar documentación"};
                            case ResponsableDeCoordinacionEnum.none:
                                return new List<string>();
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                    case EstadoDeCoordinacionEnum.Coordinacion:
                        switch (responsable)
                        {
                            case ResponsableDeCoordinacionEnum.MdD: case ResponsableDeCoordinacionEnum.PM: case ResponsableDeCoordinacionEnum.Cliente:
                                return new List<string>() {"", "Confirmar fecha", "Ok documentación", "Datos de contacto"};
                            case ResponsableDeCoordinacionEnum.InstalacionesFO:
                                return new List<string>() {"", "Proponer fecha a MdD", "Confirmar fecha a contratista", "Solicitar documentación", "Enviar documentación", "Coordinar con cliente"}; 
                            case ResponsableDeCoordinacionEnum.Contratista:
                                return new List<string>() {"", "Confirmar fecha", "Confirmar asistencia", "Nómina de personal", "Enviar documentación"};
                            case ResponsableDeCoordinacionEnum.none:
                                return new List<string>();
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                    case EstadoDeCoordinacionEnum.Coordinado:
                        return new List<string>();
                    case EstadoDeCoordinacionEnum.Cancelado:
                        return new List<string>();
                    case EstadoDeCoordinacionEnum.NoConfirmado:
                        return new List<string>();
                    case EstadoDeCoordinacionEnum.NoCoordinado:
                        return new List<string>();
                    case EstadoDeCoordinacionEnum.Terminado:
                        return new List<string>();
                    default:
                        throw new ArgumentOutOfRangeException();
                }

            }
        }

        public void AllowedValuesDependOn(EstadoDeCoordinacionEnumProperty estadoProperty, ResponsableCoordinacionEnumProperty responsableProperty)
        {
            if (_estadoProperty != null || _responsableProperty != null)
            {
                throw new InvalidOperationException();
            }

            _estadoProperty = estadoProperty;
            _responsableProperty = responsableProperty;

            responsableProperty.PropertyChanged += (sender, args) =>
            {
                this.Value = "";
            };

        }
    }
}