using System.Collections.Generic;
using Jumast.Wpf.Mvvm.BindableProperties;

namespace Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel
{
    public sealed class ResultadoDeCoordinacionProperty : ReadWritePropertyWithRestrictedValues<string>
    {
        public ResultadoDeCoordinacionProperty(string initialVvalue) : base(initialVvalue)
        {
            _allowedValues = new List<string>() { "Ejecuci�n", "�xito", "�xito parcial", "Fracaso" };
        }
    }
}