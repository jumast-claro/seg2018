using System.Collections.Generic;
using Jumast.Wpf.Mvvm.BindableProperties;

namespace Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel
{
    public sealed class ContratistaProperty : ReadWritePropertyWithRestrictedValues<string>
    {
        public ContratistaProperty(string initialValue) : base(initialValue)
        {
            _allowedValues = new List<string>()
            {
                "",
                "Aucatek",
                "Ibercom",
                "Netacom",
                "G�mez",
                "Cycsa/Ibercom",
                "Cycsa",
                "Henshi Fusi�n",
                "Lavtel"
            };
        }
    }
}