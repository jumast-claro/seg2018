using System.Collections.Generic;
using Jumast.Wpf.Mvvm.BindableProperties;

namespace Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel
{
    public sealed class ResponsableDeResultadoDeCoordinacionProperty : ReadWritePropertyWithRestrictedValues<string>
    {
        public ResponsableDeResultadoDeCoordinacionProperty(string initialVvalue) : base(initialVvalue)
        {
            _allowedValues = new List<string>() {"", "MdD", "Contratista", "Inst. FO", "Cliente"};
        }
    }
}