using System.Collections.Generic;
using System.Linq;
using Jumast.Sisifo.Coordinacion.Common;
using Jumast.Wpf.Mvvm.BindableProperties;

namespace Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel
{
    public sealed class ResponsableCoordinacionEnumProperty : EnumBindableProperty<ResponsableDeCoordinacionEnum>
    {
        private IEnumBindableProperty<EstadoDeCoordinacionEnum> _estado;
        private static readonly ResponsableEnumConverter _responsableEnumConverter = new ResponsableEnumConverter();

        private readonly Dictionary<EstadoDeCoordinacionEnum, List<ResponsableDeCoordinacionEnum>> _dic = new Dictionary<EstadoDeCoordinacionEnum, List<ResponsableDeCoordinacionEnum>>()
            {
                {EstadoDeCoordinacionEnum.Pendiente, new List<ResponsableDeCoordinacionEnum>(){ResponsableDeCoordinacionEnum.none, ResponsableDeCoordinacionEnum.MdD, ResponsableDeCoordinacionEnum.PM, ResponsableDeCoordinacionEnum.InstalacionesFO, ResponsableDeCoordinacionEnum.Contratista, ResponsableDeCoordinacionEnum.Cliente}},
                {EstadoDeCoordinacionEnum.Coordinacion, new List<ResponsableDeCoordinacionEnum>(){ResponsableDeCoordinacionEnum.none, ResponsableDeCoordinacionEnum.MdD, ResponsableDeCoordinacionEnum.PM, ResponsableDeCoordinacionEnum.InstalacionesFO, ResponsableDeCoordinacionEnum.Contratista, ResponsableDeCoordinacionEnum.Cliente}},
                {EstadoDeCoordinacionEnum.Cancelado, new List<ResponsableDeCoordinacionEnum>(){ResponsableDeCoordinacionEnum.none, ResponsableDeCoordinacionEnum.MdD, ResponsableDeCoordinacionEnum.PM, ResponsableDeCoordinacionEnum.InstalacionesFO, ResponsableDeCoordinacionEnum.Contratista, ResponsableDeCoordinacionEnum.Cliente}},
                {EstadoDeCoordinacionEnum.NoConfirmado, new List<ResponsableDeCoordinacionEnum>(){ResponsableDeCoordinacionEnum.none, ResponsableDeCoordinacionEnum.MdD, ResponsableDeCoordinacionEnum.PM, ResponsableDeCoordinacionEnum.InstalacionesFO, ResponsableDeCoordinacionEnum.Contratista, ResponsableDeCoordinacionEnum.Cliente}},
                {EstadoDeCoordinacionEnum.NoCoordinado, new List<ResponsableDeCoordinacionEnum>(){ResponsableDeCoordinacionEnum.none, ResponsableDeCoordinacionEnum.MdD, ResponsableDeCoordinacionEnum.PM, ResponsableDeCoordinacionEnum.InstalacionesFO, ResponsableDeCoordinacionEnum.Contratista, ResponsableDeCoordinacionEnum.Cliente}},
            };


        public ResponsableCoordinacionEnumProperty(ResponsableDeCoordinacionEnum initialVvalue) : base(initialVvalue)
        {
        }

        protected override string EnumToString(ResponsableDeCoordinacionEnum @enum)
        {
            return _responsableEnumConverter.EnumToString(@enum);
        }

        protected override ResponsableDeCoordinacionEnum StringToEnum(string s)
        {
            return _responsableEnumConverter.StringToEnum(s);
        }

        public void AllowedValueDependsOn(IEnumBindableProperty<EstadoDeCoordinacionEnum> estado)
        {
            //if (_estado != null)
            //{
            //    throw new InvalidOperationException();
            //}
            _estado = estado;
        }

        public override IEnumerable<string> AllowedValues
        {
            get
            {
                var tipoDeTarea = this._estado.Value;
                var enums = _dic.ContainsKey(tipoDeTarea) ? _dic[tipoDeTarea] : null;

                return enums?.Select(r => _responsableEnumConverter.EnumToString(r)).ToList();
            }
        }
    }

    
}