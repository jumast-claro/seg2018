﻿using System;
using Jumast.Sisifo.Coordinacion.Common;

namespace Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel
{
    internal sealed class TipoDeTareaEnumConverter
    {
        public string EnumToString(TipoDeCoordinacionEnum @enum)
        {
            switch (@enum)
            {
                case TipoDeCoordinacionEnum.Relevamiento:
                    return "4-Relevamiento";
                case TipoDeCoordinacionEnum.Sondeos:
                    return "5-Sondeos";
                case TipoDeCoordinacionEnum.ObraCivil:
                    return "7-Obra civil";
                case TipoDeCoordinacionEnum.ObraCivilInterna:
                    return "7-Obra civil interna";
                case TipoDeCoordinacionEnum.Tendido:
                    return "8-Tendido";
                case TipoDeCoordinacionEnum.Empalmes:
                    return "9-Empalmes";
                case TipoDeCoordinacionEnum.EmpalmesME:
                    return "9-Empalmes (ME)";
                case TipoDeCoordinacionEnum.EmpalmesGPON:
                    return "9-Empalmes (GPON)";
                case TipoDeCoordinacionEnum.Mudanza:
                    return "10-Mudanza";
                default:
                    throw new ArgumentOutOfRangeException(nameof(@enum), @enum, null);
            }
        }

        public TipoDeCoordinacionEnum StringToEnum(string s)
        {
            switch (s)
            {

                case "4-Relevamiento":
                    return TipoDeCoordinacionEnum.Relevamiento;
                case "5-Sondeos":
                    return TipoDeCoordinacionEnum.Sondeos;
                case "7-Obra civil":
                    return TipoDeCoordinacionEnum.ObraCivil;
                case "7-Obra civil interna":
                    return TipoDeCoordinacionEnum.ObraCivilInterna;
                case "8-Tendido":
                    return TipoDeCoordinacionEnum.Tendido;
                case "9-Empalmes":
                    return TipoDeCoordinacionEnum.Empalmes;
                case "9-Empalmes (ME)":
                    return TipoDeCoordinacionEnum.EmpalmesME;
                case "9-Empalmes (GPON)":
                    return TipoDeCoordinacionEnum.EmpalmesGPON;
                case "10-Mudanza":
                    return TipoDeCoordinacionEnum.Mudanza;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}