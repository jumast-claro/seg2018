namespace Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel
{
    public interface IWithCoordinador
    {
        IWithAnalistaMdD WithCoordinador(string coordinador);
    }

    public interface IWithAnalistaMdD
    {
        IWithResponsable WithAnalistaMdD(string analistaMdD);
    }
}