using Jumast.Sisifo.Coordinacion.Common;

namespace Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel
{
    public interface IWithResponsable
    {
        IWithResultado WithResponsable(ResponsableDeCoordinacionEnum responsable);
    }
}