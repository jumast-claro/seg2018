using Jumast.Sisifo.Coordinacion.Common;

namespace Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel
{
    public interface IWithEstado
    {
        IWithTareaPendiente WithEstado(EstadoDeCoordinacionEnum estado);
    }
}