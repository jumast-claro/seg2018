namespace Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel
{
    public interface IBuild
    {
        CoordinacionViewModel Build();
    }
}