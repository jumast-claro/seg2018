namespace Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel
{
    public interface IWithResponsableResultado
    {
        IWithObservacionesResultado WithResponsableResultado(string responsableResultado);
    }
}