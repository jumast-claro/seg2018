namespace Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel
{
    public interface IWithContratista
    {
        IWithFecha WithContratista(string contratista);
    }
}