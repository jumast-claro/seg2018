using System.Collections.Generic;
using Jumast.Wpf.Mvvm.BindableProperties;

namespace Jumast.Claro.ObraCivilTendido.Mvvm
{
    public sealed class EtapaProperty : ReadWritePropertyWithRestrictedValues<string>
    {
        public EtapaProperty(string initialVvalue) : base(initialVvalue)
        {
            _allowedValues = new List<string>()
            {
                "1-Análisis",
                "2-Definición de proyecto",
                "3-Asignación a contratista",
                "4-Relevamiento",
                "5-Sondeos",
                "6-Permisos",
                "7-Obra civil",
                "7-Obra civil interna",
                "8-Tendido",
                "9-Empalmes",
                "9-Empalmes (ME)",
                "9-Empalmes (GPON)",
                "10-Mudanza",
                "11-Terminada"
            };
        }
    }
}