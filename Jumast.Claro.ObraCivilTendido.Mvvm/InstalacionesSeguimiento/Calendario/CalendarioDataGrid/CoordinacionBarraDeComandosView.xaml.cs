﻿using System.Windows;
using System.Windows.Controls;
using Syncfusion.UI.Xaml.Grid;

namespace Jumast.Claro.ObraCivilTendido.Mvvm.BarraDeComandos
{
    /// <summary>
    /// Interaction logic for BarraDeComandosView.xaml
    /// </summary>
    public partial class CoordinacionBarraDeComandosView : UserControl
    {
        public static DependencyProperty SfDataGridProperty = DependencyProperty.Register("SfDataGrid", typeof(SfDataGrid), typeof(CoordinacionBarraDeComandosView));

        public CoordinacionBarraDeComandosView()
        {
            InitializeComponent();
        }

        public SfDataGrid SfDataGrid
        {
            get { return (SfDataGrid)GetValue(SfDataGridProperty); }
            set
            {
                SetValue(SfDataGridProperty, value);
            }
        }
    }
}
