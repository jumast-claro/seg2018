﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Data;
using System.Windows.Input;
using Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel;
using Jumast.Sisifo.Instalacion.ViewModel;
using Jumast.Wpf.Mvvm;

namespace Jumast.Claro.ObraCivilTendido.Mvvm.CalendarioDataGrid
{

  

    public class CalendarioDataGridViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<CoordinacionViewModel> _coordinaciones { get; set; } = new ObservableCollection<CoordinacionViewModel>();
        private readonly ICollectionView _collectionView;


        public void RemoverTodo()
        {
            _coordinaciones.Clear();
        }

        public CalendarioDataGridViewModel()
        {
            _collectionView = CollectionViewSource.GetDefaultView(_coordinaciones);
        }

        public ICollectionView Coordinaciones => _collectionView;


        public void AgregarCoordinacion(CoordinacionViewModel coordinacionViewModel)
        {
            _coordinaciones.Add(coordinacionViewModel);
        }

        public void RemoverCoordinacion(CoordinacionViewModel coordinacionViewModel)
        {
            _coordinaciones.Remove(coordinacionViewModel);
        }

        private CoordinacionViewModel _coordinacionSeleccionada;

        public CoordinacionViewModel CoordinacionSeleccionada
        {
            get => _coordinacionSeleccionada;
            set
            {
                _coordinacionSeleccionada = value;
                OnPropertyChanged();
            }
        }

        //public ICommand EliminarCoordinacionCommand { get; private set; }

        //private bool _removerCanExecute(object o)
        //{
        //    return CoordinacionSeleccionada != null;
        //}

        //private void _removerExecuted(object o)
        //{
        //    _coordinaciones.Remove(CoordinacionSeleccionada);
        //}

        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }



    }


    public class CoordinacionesDataGridViewModel : INotifyPropertyChanged, IEnumerable<CoordinacionViewModel>
    {
        public ObservableCollection<CoordinacionViewModel> Coordinaciones { get; set; } = new ObservableCollection<CoordinacionViewModel>();

        private CoordinacionViewModel _selectedCoordinacion;
        public CoordinacionViewModel SelectedCoordinacionViewModel
        {
            get { return _selectedCoordinacion; }
            set
            {
                _selectedCoordinacion = value;
                OnPropertyChanged();
            }
        }


        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public IEnumerator<CoordinacionViewModel> GetEnumerator()
        {
            return Coordinaciones.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
