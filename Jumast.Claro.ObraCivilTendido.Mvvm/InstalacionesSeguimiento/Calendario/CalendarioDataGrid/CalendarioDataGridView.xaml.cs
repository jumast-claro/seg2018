﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Jumast.Claro.ObraCivilTendido.Mvvm.CalendarioDataGrid
{
    /// <summary>
    /// Interaction logic for CalendarioDataGridView.xaml
    /// </summary>
    public partial class CalendarioDataGridView : UserControl
    {
        public CalendarioDataGridView()
        {
            InitializeComponent();
        }

        private void _sfDataGrid_OnLayoutUpdated(object sender, EventArgs e)
        {
        }
    }
}
