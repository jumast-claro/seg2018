using Jumast.Sisifo.Instalacion.ViewModel;

namespace Jumast.Claro.ObraCivilTendido.Mvvm
{
    public interface ISendMailStrategy
    {
        void SendMail(InstalacionViewModel instalacion);
    }
}