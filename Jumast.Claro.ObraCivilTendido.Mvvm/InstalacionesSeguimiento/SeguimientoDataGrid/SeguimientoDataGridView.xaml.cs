﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Syncfusion.UI.Xaml.Grid;

namespace Jumast.Claro.ObraCivilTendido.Mvvm
{
    /// <summary>
    /// Interaction logic for SeguimientoDataGridView.xaml
    /// </summary>
    public partial class SeguimientoDataGridView : UserControl
    {

        public static DependencyProperty SfDataGridProperty = DependencyProperty.Register("SfDataGrid", typeof(SfDataGrid), typeof(SeguimientoDataGridView));

        public SeguimientoDataGridView()
        {
            
            InitializeComponent();
            SfDataGrid = _sfDataGridWrapper._sfDataGrid;
        }


        public SfDataGrid SfDataGrid
        {
            get { return (SfDataGrid) GetValue(SfDataGridProperty); }
            set
            {
                SetValue(SfDataGridProperty, value);
            }
        }
      
    }
}
