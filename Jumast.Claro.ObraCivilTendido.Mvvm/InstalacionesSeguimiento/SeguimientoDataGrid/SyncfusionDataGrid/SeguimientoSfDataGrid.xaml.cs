﻿using System.Windows.Controls;
using Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel;
using Syncfusion.UI.Xaml.Grid;
using Syncfusion.UI.Xaml.Grid.Helpers;

namespace Jumast.Claro.ObraCivilTendido.Mvvm.SyncfusionDataGrid
{
    /// <summary>
    /// Interaction logic for SeguimientoSfDataGrid.xaml
    /// </summary>
    public partial class SeguimientoSfDataGrid : UserControl
    {
        public SeguimientoSfDataGrid()
        {
            InitializeComponent();
        }


        private void SfDataGrid_OnSelectionChanged(object sender, GridSelectionChangedEventArgs e)
        {
            var detailsGrid = (e.OriginalSender as DetailsViewDataGrid);
            var selectedItem = detailsGrid.SelectedItem as CoordinacionViewModel;

            var masterGrid = detailsGrid.GetParentDataGrid();
            var dataContext = masterGrid.DataContext as SeguimientoDataGridViewModel;
            dataContext.SelectedCoordinacionViewModel = selectedItem;

        }
    }
}
