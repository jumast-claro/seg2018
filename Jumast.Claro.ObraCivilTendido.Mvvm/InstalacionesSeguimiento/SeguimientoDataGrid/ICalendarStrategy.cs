using System.Collections.Generic;
using Jumast.Sisifo.Instalacion.ViewModel;

namespace Jumast.Claro.ObraCivilTendido.Mvvm
{
    public interface ICalendarStrategy
    {
        void ShowCalendar(IEnumerable<InstalacionViewModel> instalaciones);
    }
}