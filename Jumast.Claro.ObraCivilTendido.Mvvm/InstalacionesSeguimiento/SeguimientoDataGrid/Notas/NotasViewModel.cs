using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using Jumast.Claro.ObraCivilTendido.Notas;
using Jumast.Wpf.Mvvm;
using Syncfusion.Data.Extensions;

namespace Jumast.Claro.ObraCivilTendido.Mvvm.Notas
{
    public class NotasViewModel : INotifyPropertyChanged
    {
        private readonly INotasRepository _notasRepository;

        //-------------------------------------------------------------------//
        // Private fields
        //-------------------------------------------------------------------//
        private string _enlace;
        private NotaModel _nota;
        private ObservableCollection<INota> _notas = new ObservableCollection<INota>();

        //-------------------------------------------------------------------//
        // Constructor
        //-------------------------------------------------------------------//
        public NotasViewModel(INotasRepository notasRepository)
        {
            _notasRepository = notasRepository;
            AgregarNotaCommand = new RelayCommand(agregarNotaExecuted, agregarNotaCanExecute);
            EliminarNotaCommand = new RelayCommand(eleminarNotaExecuted, eliminarNotaCanExecute);
            GuardarNotasCommand = new RelayCommand(_guardarNotasExecuted, o => true);
        }

        //-------------------------------------------------------------------//
        // Public properties
        //-------------------------------------------------------------------//
        public string Enlace
        {
            get { return _enlace; }
            set
            {
                _enlace = value;
                var notas = _notasRepository.TodasLasNotas().Where(n => value.Contains(n.Enlace));
                Notas = notas.ToObservableCollection();
            }
        }

        public ObservableCollection<INota> Notas
        {
            get
            {
                return _notas;
            }
            set
            {
                _notas = value.OrderByDescending(n => n.Fecha).ToObservableCollection();
                OnPropertyChanged();
            }
        }

      
        public NotaModel Nota
        {
            get
            {
                return _nota;
            }
            set
            {
                _nota = value;
                OnPropertyChanged();
            }
        }


        public void AgregarNota(string nota)
        {
            var nuevaNota = new NotaModel { Fecha = DateTime.Now, Enlace = Enlace, Nota = nota};
            Notas.Insert(0, nuevaNota);
            _notasRepository.AgregarNota(nuevaNota);
        }

        //-------------------------------------------------------------------//
        // Conmmands
        //-------------------------------------------------------------------//
        public ICommand AgregarNotaCommand { get; set; }
        private void agregarNotaExecuted(object o)
        {
            var nuevaNota = new NotaModel { Fecha = DateTime.Now, Enlace = Enlace };
            Notas.Insert(0, nuevaNota);
            _notasRepository.AgregarNota(nuevaNota);
        }
        private bool agregarNotaCanExecute(object o)
        {
            return !string.IsNullOrEmpty(Enlace);
        }

        public ICommand EliminarNotaCommand { get; set; }
        private void eleminarNotaExecuted(object o)
        {
            _notasRepository.EliminarNota(Nota);
            Notas.Remove(Nota);
        }
        private bool eliminarNotaCanExecute(object o)
        {
            return Nota != null;
        }

        public ICommand GuardarNotasCommand { get; set; }
        private void _guardarNotasExecuted(object o)
        {
            try
            {
                _notasRepository.Guardar();
            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
            }

        }


        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}