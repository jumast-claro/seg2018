﻿using System.Windows;
using System.Windows.Controls;
using Syncfusion.UI.Xaml.Grid;

namespace Jumast.Claro.ObraCivilTendido.Mvvm.BarraDeComandos
{
    /// <summary>
    /// Interaction logic for BarraDeComandosView.xaml
    /// </summary>
    public partial class BarraDeComandosView : UserControl
    {
        public static DependencyProperty SfDataGridProperty = DependencyProperty.Register("SfDataGrid", typeof(SfDataGrid), typeof(BarraDeComandosView));

        public BarraDeComandosView()
        {
            InitializeComponent();
        }

        public SfDataGrid SfDataGrid
        {
            get { return (SfDataGrid)GetValue(SfDataGridProperty); }
            set
            {
                SetValue(SfDataGridProperty, value);
            }
        }
    }
}
