﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using Jumast.Claro.ObraCivilTendido.DataAcces.Abstract;
using Jumast.Claro.ObraCivilTendido.Notas;
using Jumast.Sisifo.Contratista.DataAcces;
using Jumast.Sisifo.Coordinacion.DataAcces;
using Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataModel;
using Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel;
using Jumast.Sisifo.Instalacion.Common;
using Jumast.Sisifo.Instalacion.ViewModel;
using Jumast.Sisifo.MesaDeDespacho.DataAcces;

namespace Jumast.Claro.ObraCivilTendido.Mvvm.InstalacionesSeguimiento.SeguimientoDataGrid
{
    public sealed class DataGridViewModelPersistor : IDataGridViewModelPersistor
    {
        private readonly IInstalacionesFOExcelRepository _excelRepository;
        private readonly InfoAdicionalRepository _infoAdicionalRepository;
        private readonly IMdDRepository _analistasMdDRepository;
        private readonly ICoordinadoresRepository _coordinadoresRepo;
        private readonly ICoordinacionesRepository _coordinacionesRepo;


        private void agregarCoordinacionesAInstalaciones(IEnumerable<InstalacionViewModel> instalaciones)
        {
            var coordinaciones = _coordinacionesRepo.Obtener();
            foreach (var dataModel in coordinaciones)
            {
                var enlace = dataModel.Enlace;
                var instalacion = instalaciones.Where(i => i.NumeroDeEnlaceProperty.Value == enlace).FirstOrDefault();
                if (instalacion == null)
                {
                    continue;
                }

                var coordinacionViewModel = CoordinacionViewModelBuilder.Init(_coordinadoresRepo)
                    .WithCliente(instalacion.ClienteProperty.Value)
                    .WithDireccion(instalacion.DireccionProperty.Value)
                    .WithPartido(instalacion.PartidoProperty.Value)
                    .WithEnlace(dataModel.Enlace)
                    .WithTipoDeCoordinacion(dataModel.TipoDeTarea)
                    .WithContratista(dataModel.Contratista)
                    .WithFecha(dataModel.Fecha)
                    .WithEstado(dataModel.Estado)
                    .WithTareaPendiente(dataModel.TareaPendiente)
                    .WithCoordinador(dataModel.Coordinador)
                    .WithAnalistaMdD(instalacion.AnalistaMdDProperty.Value)
                    .WithResponsable(dataModel.Responsable)
                    .WithResultado(dataModel.Resultado)
                    .WithResponsableResultado(dataModel.ResponsableResultado)
                    .WithObservacionesResultado(dataModel.ObservacionesResultado)
                    .Build();

                coordinacionViewModel.IdProperty.Value = dataModel.Id;


                instalacion.AgregarCoordinacion(coordinacionViewModel);
                //CalendarioDataGridViewModel.Coordinaciones.Add(coordinacionViewModel);
            }
        }

        private void guardarInfoAdicional(IEnumerable<InstalacionViewModel> instalaciones)
        {
            var aux = new List<InfoAdicional>();
            foreach (var instalacion in instalaciones)
            {
                var id = instalacion.NumeroDeEnlaceProperty.Value;
                var etapa = instalacion.EtapaProperty.StringValue;
                var estado = instalacion.EstadoProperty.Value;
                var observaciones = instalacion.ObservacionesProperty.Value;
                var detalle = instalacion.ResponsableProperty.Value;

                var infoAdicional = new InfoAdicional
                {
                    Id = instalacion.NumeroDeEnlaceProperty.Value,
                    Etapa = etapa,
                    Proceso = estado,
                    Coordinador = instalacion.CoordinadorProperty.Value,
                    AnalistaMdD = instalacion.AnalistaMdDProperty.Value,
                    Observaciones = observaciones,
                    Detalle = detalle,
                    Fecha = instalacion.FechaProperty.Value,
                    TareaPendiente = instalacion.TareaPendienteProperty.Value
                };

                aux.Add(infoAdicional);

            }

            try
            {
                _infoAdicionalRepository.Guardar(aux);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }


        //private void guardarCoordinaciones(IEnumerable<InstalacionViewModel> instalaciones)
        //{
        //    var data = new List<CoordinacionDataModel>();
        //    foreach (var instalacion in instalaciones)
        //    {
        //        foreach (var coodinacion in instalacion.Coordinaciones)
        //        {
        //            var coordinacionDataModelBuilder = CooordinacionDataModelBuilder.Init()
        //                .WithEnlace(instalacion.NumeroDeEnlaceProperty.Value)
        //                .WithTipoDeCoordinacion(coodinacion.TipoDeTareaProperty.Value)
        //                .WithContratista(coodinacion.ContratistaProperty.Value)
        //                .WithFecha(coodinacion.FechaProperty.Value)
        //                .WithEstado(coodinacion.EstadoProperty.Value)
        //                .WithTareaPendiente(coodinacion.TareaPendienteProperty.Value)
        //                .WithCoordinador(coodinacion.CoordinadorProperty.Value)
        //                .WithResponsable(coodinacion.ResponsableProperty.Value)
        //                .WithResultado(coodinacion.ResultadoProperty.Value)
        //                .WithResponsableResultado(coodinacion.ResponsableResultadoProperty.Value)
        //                .WithObservacionesResultado(coodinacion.ObservacionesResultadoProperty.Value);

        //            data.Add(coordinacionDataModelBuilder.Build());
        //        }
        //    }

        //    try
        //    {
        //        _coordinacionesRepo.Guardar(data);
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //    }
        //}


        public DataGridViewModelPersistor(IInstalacionesFOExcelRepository excelRepository, InfoAdicionalRepository infoAdicionalRepository, IMdDRepository analistasMdDRepository, ICoordinadoresRepository coordinadoresRep, ICoordinacionesRepository coordinacionesRepo)
        {
            _excelRepository = excelRepository;
            _infoAdicionalRepository = infoAdicionalRepository;
            _analistasMdDRepository = analistasMdDRepository;
            _coordinadoresRepo = coordinadoresRep;
            _coordinacionesRepo = coordinacionesRepo;

            
        }

        public ObservableCollection<InstalacionViewModel> ObtenerFilas()
        {
            try
            {
                var infoAdicional = _infoAdicionalRepository.Obtener();
                var viewModels = new ObservableCollection<InstalacionViewModel>();

                _excelRepository.Actualizar();
                var instalaciones = _excelRepository.ObtenerTodasLasInstalaciones();

                foreach (var instalacion in instalaciones)
                {
                    //var instalacionViewModel = new InstalacionViewModel(instalacion, inf);
                    InstalacionViewModel instalacionViewModel = null;
                    var inf = infoAdicional.Where(i => i.Id == instalacion.Enlace).FirstOrDefault();
                    if (inf == null)
                    {
                        instalacionViewModel = InstalacionViewModelBuilder.Init(_analistasMdDRepository, _coordinadoresRepo)
                            .WithEnlace(instalacion.Enlace)
                            .WithNumeroDeOrden(instalacion.Orden)
                            .WithCliente(instalacion.Cliente)
                            .WithDireccion(instalacion.Direccion)
                            .WithPartido(instalacion.Partido)
                            .WithFechaDeRecibido(instalacion.FechaDeRecibido)
                            .WithFechaDeCompromio(instalacion.FechaDeCompromisoVantive)
                            .WithTipoDeOrden(instalacion.TipoDeOrden)
                            .WithTecnologia(instalacion.Tecnologia)
                            .WithContratistaEnCurso(instalacion.ContratistaEnCurso)
                            .WithDiasDeRecibido(instalacion.DiasDeRecibido)
                            .WithFechaDeCierre(instalacion.FechaDeCierre)
                            .WithAnalistaInstalacionesFO(instalacion.Hipervisor)
                            .WithEtapa(EtapaEnum.indefinida)
                            .WithEstado("")
                            .WithResponsable("")
                            .WithTareaPendiente("")
                            .WithFecha(null)
                            .WithRelevador("")
                            .WithAnalistaMdD("")
                            .WithObservaciones("")
                            .WithHiperlink("")
                            .Build();
                    }
                    else
                    {
                        instalacionViewModel = InstalacionViewModelBuilder.Init(_analistasMdDRepository, _coordinadoresRepo)
                            .WithEnlace(instalacion.Enlace)
                            .WithNumeroDeOrden(instalacion.Orden)
                            .WithCliente(instalacion.Cliente)
                            .WithDireccion(instalacion.Direccion)
                            .WithPartido(instalacion.Partido)
                            .WithFechaDeRecibido(instalacion.FechaDeRecibido)
                            .WithFechaDeCompromio(instalacion.FechaDeCompromisoVantive)
                            .WithTipoDeOrden(instalacion.TipoDeOrden)
                            .WithTecnologia(instalacion.Tecnologia)
                            .WithContratistaEnCurso(instalacion.ContratistaEnCurso)
                            .WithDiasDeRecibido(instalacion.DiasDeRecibido)
                            .WithFechaDeCierre(instalacion.FechaDeCierre)
                            .WithAnalistaInstalacionesFO(instalacion.Hipervisor)
                            .WithEtapa(new EtapaEnumConverter().StringToEnum(inf.Etapa))
                            .WithEstado(inf.Proceso)
                            .WithResponsable(inf.Detalle)
                            .WithTareaPendiente(inf.TareaPendiente)
                            .WithFecha(inf.Fecha)
                            .WithRelevador(inf.Coordinador)
                            .WithAnalistaMdD(inf.AnalistaMdD)
                            .WithObservaciones(inf.Observaciones)
                            .WithHiperlink(instalacion.Link)
                            .Build();
                    }


                    viewModels.Add(instalacionViewModel);
                }

                agregarCoordinacionesAInstalaciones(viewModels);

                return viewModels;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return null;
            }

        }

        public void GuardarFilas(IEnumerable<InstalacionViewModel> instalaciones)
        {
            guardarInfoAdicional(instalaciones);
            //guardarCoordinaciones(instalaciones);
        }
    }
}
