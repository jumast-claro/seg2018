using System;
using System.IO;
using System.Text;
using Jumast.Sisifo.Instalacion.ViewModel;
using Microsoft.Office.Interop.Outlook;

namespace Jumast.Claro.ObraCivilTendido.Mvvm
{

    public class TemplateParser
    {
        private readonly string _saludo;
        private readonly string _fecha;
        private readonly string _horario;
        private readonly string _coordinador;
        private readonly string _analistaMdD;
        private readonly string _contratista;

        public TemplateParser(string saludo, string fecha, string horario, string coordinador, string analistaMdD, string contratista)
        {
            _saludo = saludo;
            _fecha = fecha;
            _horario = horario;
            _coordinador = coordinador;
            _analistaMdD = analistaMdD;
            _contratista = contratista;
        }

        public string ParseTemplate(string bodyTemplate)
        {
            var sb = new StringBuilder(bodyTemplate);
            sb.Replace("{{analistaMdD}}", _analistaMdD.Split(' ')[0]);
            sb.Replace("{{contratista}}", _contratista);
            sb.Replace("{{saludo}}", _saludo);
            sb.Replace("{{fecha}}", _fecha);
            sb.Replace("{{horario}}", _horario);
            sb.Replace("{{relevador}}", _coordinador);

            var buenDiaMayus = $"{(DateTime.Now.Hour <= 13 ? "Buen d�a" : "Buenas tardes")}";
            var buenDiaMinus = $"{(DateTime.Now.Hour <= 13 ? "buen d�a" : "buenas tardes")}";
            sb.Replace("{{Buen d�a}}", buenDiaMayus);
            sb.Replace("{{buen d�a}}", buenDiaMinus);

            return sb.ToString();
        }
    }

    public sealed class NotaCRMStrategy
    {
        private const string PATH = @"C:\Users\Jumast\Desktop\SEG2018_DATA\templates_notas";
        private const string ENCODING_STRING = "iso-8859-1";

        public string ObtenerNota(InstalacionViewModel instalacion)
        {

            var inst = instalacion;
            var fecha = inst.FechaProperty.Value ?? DateTime.Now;

            var etapa = inst.EtapaProperty.Value;
            var estado = inst.EstadoProperty.Value;
            var responsable = inst.ResponsableProperty.Value;
            var tareaPendiente = inst.TareaPendienteProperty.Value;


            var fileNameWithExtension = $"{etapa}_{estado}_{responsable}_{tareaPendiente}.txt";
            var fullFileNameWithExtension = Path.Combine(PATH, fileNameWithExtension);

            if (File.Exists(fullFileNameWithExtension))
            {
                var saludo = $"{(DateTime.Now.Hour <= 13 ? "Buen d�a" : "Buenas tardes")} {inst.AnalistaMdDProperty.Value.Split(' ')[0]}.";
                var strFecha = $"{fecha:dddd dd/MM}";
                var strHorario = $"entre las {fecha:HH:mm} y las {fecha.AddHours(2):HH:mm}";

                var template = File.ReadAllText(fullFileNameWithExtension, Encoding.GetEncoding(ENCODING_STRING));
                var templateParser = new TemplateParser(saludo, strFecha, strHorario, inst.CoordinadorProperty.Value, instalacion.AnalistaMdDProperty.Value, instalacion.ContratistaEnCursoProperty.Value);
                return templateParser.ParseTemplate(template);
            }

            return  $"Template not found: {fullFileNameWithExtension}";
        }
    }


    public sealed class SendMailStrategy : ISendMailStrategy
    {
        private const string PATH = @"C:\Users\Jumast\Desktop\SEG2018_DATA\templates_mails";
        private const string ENCODING_STRING = "iso-8859-1";

        public void SendMail(InstalacionViewModel instalacion)
        {

            var inst = instalacion;
            var fecha = inst.FechaProperty.Value ?? DateTime.Now;

            var etapa = inst.EtapaProperty.Value;
            var estado = inst.EstadoProperty.Value;
            var responsable = inst.ResponsableProperty.Value;
            var tareaPendiente = inst.TareaPendienteProperty.Value;


            Application outlookApp = new Application();
            MailItem mailItem = outlookApp.CreateItem(OlItemType.olMailItem);

            AppointmentItem appointment = outlookApp.CreateItem(OlItemType.olAppointmentItem);

            //mailItem.Subject = $"{inst.Model.Cliente} ({inst.Model.Direccion}, {inst.Model.Partido}) Orden:{inst.Model.Orden}_Enlace:{inst.Model.Enlace} | {inst.TecnologiaProperty.Value}";
            mailItem.Subject = "asunto de mail";

            var fileNameWithExtension = $"{etapa}_{estado}_{responsable}_{tareaPendiente}.txt";
            var fullFileNameWithExtension = Path.Combine(PATH, fileNameWithExtension);

            //mailItem.To = ;

            mailItem.To = responsable == "MdD" ? $"{inst.AnalistaMdDProperty.Value}" : "";

            //mailItem.CC = $"{inst.CoordinadorProperty.Value}";

            if (File.Exists(fullFileNameWithExtension))
            {
                var saludo = $"{(DateTime.Now.Hour <= 13 ? "Buen d�a" : "Buenas tardes")} {inst.AnalistaMdDProperty.Value.Split(' ')[0]}.";
                var strFecha = $"{fecha:dddd dd/MM}";
                var strHorario = $"entre las {fecha:HH:mm} y las {fecha.AddHours(2):HH:mm}";

                var bodyTemplate = File.ReadAllText(fullFileNameWithExtension, Encoding.GetEncoding(ENCODING_STRING));
                var templateParser = new TemplateParser(saludo, strFecha, strHorario, inst.CoordinadorProperty.Value, instalacion.AnalistaMdDProperty.Value, instalacion.ContratistaEnCursoProperty.Value);
                mailItem.Body = templateParser.ParseTemplate(bodyTemplate);
            }

            else
            {
                mailItem.Body = $"Template not found: {fullFileNameWithExtension}";
            }

           
            mailItem.Display(false);
        }
    }
}