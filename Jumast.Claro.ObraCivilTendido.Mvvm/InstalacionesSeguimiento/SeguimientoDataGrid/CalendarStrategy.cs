using System.Collections.Generic;
using System.Windows;
using Jumast.Sisifo.Instalacion.ViewModel;
using Jumast.Wpf.Mvvm.BindableProperties;

namespace Jumast.Claro.ObraCivilTendido.Mvvm
{
    public sealed class ScheduleTypeProperty : ReadWritePropertyWithRestrictedValues<string>
    {
        public ScheduleTypeProperty(string initialVvalue) : base(initialVvalue)
        {
            _allowedValues = new List<string>
            {
                "Day",
                "Week",
                "TimeLine"
            };
        }
    }

    public sealed class CalendarioViewModel
    {

        public CalendarioViewModel()
        {
            ScheduleTypeProperty = new ScheduleTypeProperty("Week");
        }

        public IEnumerable<Appointment> Appointments { get; set; } = new List<Appointment>();

        public ScheduleTypeProperty ScheduleTypeProperty { get; private set; }
    }

    public sealed class CalendarStrategy : ICalendarStrategy
    {
        public void ShowCalendar(IEnumerable<InstalacionViewModel> instalaciones)
        {
            var appointments = new List<Appointment>();
            foreach (var instalacionViewModel in instalaciones)
            {
                foreach (var coordinacion in instalacionViewModel.Coordinaciones)
                {
                    //var model = instalacionViewModel.Model;
                    if (coordinacion.FechaProperty.Value != null)
                    {
                        var appointment = new Appointment(coordinacion.ContratistaProperty.Value, coordinacion.FechaProperty.Value.Value,
                            coordinacion.TipoDeTareaProperty.Value,
                            coordinacion.EstadoProperty.Value,
                            coordinacion.ResponsableProperty.Value,
                            coordinacion.TareaPendienteProperty.Value,
                            coordinacion.ResultadoProperty.Value,
                            coordinacion.ResponsableResultadoProperty.Value,
                            instalacionViewModel.EtapaProperty.StringValue)
                        {
                            Cliente = instalacionViewModel.ClienteProperty.Value,
                            Location = $"{instalacionViewModel.DireccionProperty.Value}, {instalacionViewModel.PartidoProperty.Value}",
                            MdD = instalacionViewModel.AnalistaMdDProperty.Value,
                            Coordinador = instalacionViewModel.CoordinadorProperty.Value
                        };
                        appointments.Add(appointment);
                    }
                }
            }

            var view = new CalendarioView();

            var calendarioViewModel = new CalendarioViewModel();
            calendarioViewModel.Appointments = appointments;
            view.DataContext = calendarioViewModel;

            var window = new Window();
            window.Owner = Application.Current.MainWindow;
            window.Content = view;
            window.WindowState = WindowState.Maximized;
            window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            window.Show();
        }
    }
}