﻿using Jumast.Claro.ObraCivilTendido.Mvvm.CalendarioDataGrid;
using Jumast.Claro.ObraCivilTendido.Mvvm.InstalacionesSeguimiento.SeguimientoDataGrid;
using Jumast.Claro.ObraCivilTendido.Mvvm.Notas;
using Jumast.Claro.ObraCivilTendido.Notas;
using Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel;
using Jumast.Sisifo.Instalacion.ViewModel;
using Jumast.Wpf.Mvvm;
using Jumast.Wpf.Mvvm.BindableProperties;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Jumast.Sisifo.Coordinacion.DataAcces;
using Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataModel;
using Syncfusion.Data.Extensions;

namespace Jumast.Claro.ObraCivilTendido.Mvvm
{
    /// <summary>
    /// Modela la vista de las instalaciones en seguimiento.
    /// <para>La parte principal de la vista es una grilla en la que cada fila corresponde a una instalación, y además se pueden ver y editar las notas y las coordinaciones.</para>
    /// </summary>
    public class SeguimientoDataGridViewModel : INotifyPropertyChanged
    {
        //-------------------------------------------------------------------//
        // Private fields
        //-------------------------------------------------------------------//
        private readonly ICoordinacionesRepository _coordinacionesRepository;

        private readonly ICalendarStrategy _calendarStrategy;

        private readonly ISendMailStrategy _sendMailStrategy;
        private readonly NotaCRMStrategy _notaCRMStrategy;
        private InstalacionViewModel _instalacionSeleccionada;


        private ObservableCollection<InstalacionViewModel> _allItems = new ObservableCollection<InstalacionViewModel>();
        private ObservableCollection<InstalacionViewModel> _viewItems = new ObservableCollection<InstalacionViewModel>();
        private NotasViewModel _notas;

        private readonly IDataGridViewModelPersistor _viewModelPersistor;
        private readonly ICoordinador _coordinador;


        private bool _isBusy = false;

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                OnPropertyChanged();
            }
        }

        //-------------------------------------------------------------------//
        // Constructor
        //-------------------------------------------------------------------//
        public SeguimientoDataGridViewModel(ICoordinacionesRepository coordinacionesRepository,IDataGridViewModelPersistor dataGridViewModelPersistor, INotasRepository notasRepository, ICalendarStrategy calendarStrategy, ISendMailStrategy sendMailStrategy, NotaCRMStrategy notaCRMStrategy, ICoordinador coordinador)
        {
            _coordinacionesRepository = coordinacionesRepository;
            _calendarStrategy = calendarStrategy;
            _sendMailStrategy = sendMailStrategy;
            _notaCRMStrategy = notaCRMStrategy;
            _coordinador = coordinador;
            _viewModelPersistor = dataGridViewModelPersistor;


            //_allItems = _viewModelPersistor.ObtenerFilas();
            //Instalaciones = _allItems.ToObservableCollection();
            //foreach (var instalacion in _allItems)
            //{
            //    foreach (var coordinacion in instalacion.Coordinaciones)
            //    {
            //        CalendarioDataGridViewModel.AgregarCoordinacion(coordinacion);
            //    }
            //}

            ActualizarCommand = new RelayCommand(_actualizarExecutd, o => true);
            Notas = new NotasViewModel(notasRepository);
            AbrirCarpetaCommand = new RelayCommand(_abrirCarpetaExecuted, _abrirCarpetaCanExecute);
            VerDetallesCommand = new RelayCommand(_verDetallesExecuted, _verDetallesCanExecute);
            GuardarInfoCommand = new RelayCommand(_guardarInfoCommand, _guardarInfoCanExecute);
            VerCalendarioCommand = new RelayCommand(_verCalendarioExecuted, _verCalendarioCanExecute);
            SendEMailCommand = new RelayCommand(sendEMailExecuted, sendEMailCanExecute);
            AgregarCoordinacionCommand = new RelayCommand(_agregarCoordinacionExecuted, _agregarCoordinacionCanExecute);
            EliminarCoordinacionCommand = new RelayCommand(_eliminarCoordinacionExecuted, _eliminarCoordinacionCanExecute);
            NotaCRMCommand = new RelayCommand(notaCRMExecuted, notaCRMCanExecute);
            CommandLineCommand = new RelayCommand(_commandLineCommandExecuted, _commandLineCommandCanExecute);
        }

        //-------------------------------------------------------------------//
        // Public properties
        //-------------------------------------------------------------------//
        public ObservableCollection<InstalacionViewModel> Instalaciones
        {
            get => _viewItems;
            set
            {
                _viewItems = value;
                OnPropertyChanged();
            }
        }

        public IBindableProperty<bool> AgregarNotaAutomaticamenteProperty { get; private set; } = new ReadWriteProperty<bool>(true);


        public CoordinacionViewModel SelectedCoordinacionViewModel { get; set; }
        public CalendarioDataGridViewModel CalendarioDataGridViewModel { get; set; } = new CalendarioDataGridViewModel();
        //public ObservableCollection<InstalacionViewModel> Instalaciones
        //{
        //    get { return _allItems; }
        //    set
        //    {
        //        if (value == _allItems) return;
        //        _allItems = value;
        //        OnPropertyChanged();
        //    }
        //}
        public InstalacionViewModel InstalacionSeleccionada
        {
            get { return _instalacionSeleccionada; }
            set
            {
                if (value != _instalacionSeleccionada)
                {
                    _instalacionSeleccionada = value;
                    OnPropertyChanged();


                    if (InstalacionSeleccionada == null)
                    {
                        Notas.Notas.Clear();
                    }
                    else
                    {
                        var enlace = InstalacionSeleccionada.NumeroDeEnlaceProperty.Value;
                        Notas.Enlace = enlace;
                    }
                }
            }
        }

        public NotasViewModel Notas
        {
            get { return _notas; }
            set
            {
                _notas = value;
                OnPropertyChanged();
            }
        }



        //-------------------------------------------------------------------//
        // Conmmands
        //-------------------------------------------------------------------//
        //public IBindableProperty<Visibility> DescripcionVisibilityProperty { get; } = new ReadWriteProperty<Visibility>(Visibility.Collapsed);

        public IBindableProperty<string> CommandInputProperty { get; } = new ReadWriteProperty<string>("");
        public IBindableProperty<string> CommandOutputProperty { get; } = new ReadWriteProperty<string>("");
        public ICommand CommandLineCommand { get; }

        private bool _commandLineCommandCanExecute(object o)
        {
            return true;
        }

        private void _commandLineCommandExecuted(object o)
        {
            var text = CommandInputProperty.Value.Split(';').Last().TrimStart();

          

            if (text == "clear")
            {
                CommandOutputProperty.Value = "";
                CommandInputProperty.Value = "";
                return;
            }

            //if (text == "visible")
            //{
            //    DescripcionVisibilityProperty.Value = Visibility.Visible;
            //    return;
            //}

            //if (text == "!visible")
            //{
            //    DescripcionVisibilityProperty.Value = Visibility.Collapsed;
            //    return;
            //}



            var comando = text.Split(':');

            if(comando[0] == "filas clear")
            {
                Instalaciones = _allItems.ToObservableCollection();
                CommandOutputProperty.Value = $"Comando '{text}' --> ok";
                return;
            }

            if (comando[0] == "filas mías")
            {
                if (_allItems.Any(i => i.AnalistaInstalacionesFOProperty.Value == "Straus"))
                {
                    Instalaciones = _allItems.Where(i => i.AnalistaInstalacionesFOProperty.Value == "Straus").ToObservableCollection();
                    CommandOutputProperty.Value = $"Comando '{text}' --> ok";
                    return;
                }
                CommandOutputProperty.Value = $"Hipervisor 'Straus' no encontrado.";
                return;
            }

            if (comando[0] == "filas hipervisor")
            {
                var nombre = comando[1].TrimStart();


                if (_allItems.Any(i => i.AnalistaInstalacionesFOProperty.Value == nombre))
                {
                    Instalaciones = _allItems.Where(i => i.AnalistaInstalacionesFOProperty.Value == nombre).ToObservableCollection();
                    CommandOutputProperty.Value = $"Comando '{text}' --> ok";
                    return;
                }

                CommandOutputProperty.Value = $"Hipervisor '{nombre}' no encontrado.";
                return;
            }


        
            if (comando[0] == "enlace")
            {
                var enlace = comando[1].TrimStart();

                var instalacion = _allItems.FirstOrDefault(i => i.NumeroDeEnlaceProperty.Value.Contains(enlace));

                if (instalacion != null)
                {
                    CommandOutputProperty.Value = instalacion.DescripcionProperty.Value;
                    return;
                }

                CommandOutputProperty.Value = $"Enlace '{enlace}' no encontrado.";
                return;
            }

            if (comando[0] == "orden")
            {
                var orden = comando[1].TrimStart();

                var instalacion = _allItems.FirstOrDefault(i => i.NumeroDeOrdenProperty.Value.Contains(orden));

                if (instalacion != null)
                {
                    CommandOutputProperty.Value = instalacion.DescripcionProperty.Value;
                    return;
                }

                CommandOutputProperty.Value = $"Orden '{orden}' no encontrada.";
                return;
            }

            if (comando[0] == "sync")
            {

                var instalacion = InstalacionSeleccionada;
                var coordinacion = SelectedCoordinacionViewModel;
                if (instalacion == null || coordinacion == null)
                {
                    CommandOutputProperty.Value = "Comando 'sync' falló";
                    return;
                }


                instalacion.EtapaProperty.Value = new EtapaEnumConverter().StringToEnum(coordinacion.TipoDeTareaProperty.StringValue);
                instalacion.EstadoProperty.Value = coordinacion.EstadoProperty.StringValue;
                instalacion.FechaProperty.Value = coordinacion.FechaProperty.Value;
                instalacion.ResponsableProperty.Value = coordinacion.ResponsableProperty.StringValue;
                instalacion.TareaPendienteProperty.Value = coordinacion.TareaPendienteProperty.Value;
                return;
            }


            CommandOutputProperty.Value = $"Comando '{text}' desconocido.";

          
          
        }

        public ICommand AgregarCoordinacionCommand { get; set; }
        private bool _agregarCoordinacionCanExecute(object o) => true;
        private void _agregarCoordinacionExecuted(object o)
        {
            var coordinacionViewModel = _coordinador.InferirCoordinacion(InstalacionSeleccionada);

            var id = _coordinacionesRepository.GetUniqueId();
            coordinacionViewModel.IdProperty.Value = id;
            CalendarioDataGridViewModel.AgregarCoordinacion(coordinacionViewModel);
            InstalacionSeleccionada.AgregarCoordinacion(coordinacionViewModel);

            var coordinacionDataModelBuilder = CooordinacionDataModelBuilder.Init()
                .WithId(id)
                .WithEnlace(coordinacionViewModel.NumerosDeEnlaceProperty.Value)
                .WithTipoDeCoordinacion(coordinacionViewModel.TipoDeTareaProperty.Value)
                .WithContratista(coordinacionViewModel.ContratistaProperty.Value)
                .WithFecha(coordinacionViewModel.FechaProperty.Value)
                .WithEstado(coordinacionViewModel.EstadoProperty.Value)
                .WithTareaPendiente(coordinacionViewModel.TareaPendienteProperty.Value)
                .WithCoordinador(coordinacionViewModel.CoordinadorProperty.Value)
                .WithResponsable(coordinacionViewModel.ResponsableProperty.Value)
                .WithResultado(coordinacionViewModel.ResultadoProperty.Value)
                .WithResponsableResultado(coordinacionViewModel.ResponsableResultadoProperty.Value)
                .WithObservacionesResultado(coordinacionViewModel.ObservacionesResultadoProperty.Value);

            var coordinacionDataModel = coordinacionDataModelBuilder.Build();

            _coordinacionesRepository.Agregar(coordinacionDataModel);
        }

        public ICommand EliminarCoordinacionCommand { get; private set; }
        private void _eliminarCoordinacionExecuted(object o)
        {
       
            var id = SelectedCoordinacionViewModel.IdProperty.Value;
            CalendarioDataGridViewModel.RemoverCoordinacion(SelectedCoordinacionViewModel);
            InstalacionSeleccionada.Coordinaciones.Remove(SelectedCoordinacionViewModel);
            _coordinacionesRepository.RemoverConId(id);

        }
        private bool _eliminarCoordinacionCanExecute(object o)
        {
            return SelectedCoordinacionViewModel != null;
        }

        public ICommand VerCalendarioCommand { get; set; }
        private void _verCalendarioExecuted(object o)
        {
            _calendarStrategy.ShowCalendar(_allItems);

        }
        private bool _verCalendarioCanExecute(object o) => true;

        public ICommand VerDetallesCommand { get; private set; }
        private void _verDetallesExecuted(object o)
        {
            var instalacionViewModel = InstalacionSeleccionada;
            var view = new InstalacionDetalleView();
            //var viewModel = new InstalacionDetalleViewModel(instalacionesViewModel, null);



            view.DataContext = instalacionViewModel;

            var window = new Window();
            window.Owner = Application.Current.MainWindow;
            window.Content = view;

            window.WindowState = WindowState.Maximized;
            window.WindowStartupLocation = WindowStartupLocation.CenterScreen;

            window.Show();

        }
        private bool _verDetallesCanExecute(object o)
        {
            return InstalacionSeleccionada != null;
        }

        public ICommand GuardarInfoCommand { get; private set; }
        private void _guardarInfoCommand(object o)
        {

            _viewModelPersistor.GuardarFilas(_allItems);
            //_viewModelPersistor.GuardarCoordinaciones(this.CalendarioDataGridViewModel.Coordinaciones);
            _notas.GuardarNotasCommand.Execute(null);

            foreach (var instalacion in _allItems)
            {
                foreach (var coordinacion in instalacion.Coordinaciones)
                {
                    var dataModel = coordinacion.GetDataModel();
                    _coordinacionesRepository.Guardar(dataModel.Id, dataModel);
                }
            }

            _coordinacionesRepository.Guardar();

        }
        private bool _guardarInfoCanExecute(object o)
        {
            return true;
        }

        public ICommand NotaCRMCommand { get; private set; }

        private bool notaCRMCanExecute(object o)
        {
            return InstalacionSeleccionada != null;
        }

        private void notaCRMExecuted(object o)
        {
            var nota = _notaCRMStrategy.ObtenerNota(InstalacionSeleccionada);
            Clipboard.SetText(nota);
        }

        public ICommand SendEMailCommand { get; set; }

        private void sendEMailExecuted(object o)
        {
            _sendMailStrategy.SendMail(InstalacionSeleccionada);
            if (AgregarNotaAutomaticamenteProperty.Value)
            {
                var nota = _notaCRMStrategy.ObtenerNota(InstalacionSeleccionada);
                _notas.AgregarNota(nota);
            }
        }

        private bool sendEMailCanExecute(object o)
        {
            return InstalacionSeleccionada != null;
        }


        private void getData()
        {
            _allItems = _viewModelPersistor.ObtenerFilas();
            Instalaciones = _allItems.ToObservableCollection();
        }


        public ICommand ActualizarCommand { get; private set; }
        private async void _actualizarExecutd(object o)
        {

            await Task.Run((() =>
            {
                IsBusy = true;
                getData();
                IsBusy = false;
            }));

            CalendarioDataGridViewModel.RemoverTodo();
            var cant = 0;
            foreach (var instalacion in _allItems)
            {
                foreach (var coordinacion in instalacion.Coordinaciones)
                {
                    CalendarioDataGridViewModel.AgregarCoordinacion(coordinacion);
                    cant++;
                }
            }


        }

        public ICommand AbrirCarpetaCommand { get; private set; }
        private void _abrirCarpetaExecuted(object o)
        {
            try
            {
                Process.Start(new ProcessStartInfo(InstalacionSeleccionada.HyperlinkProperty.Value));
            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
            }
        }
        private bool _abrirCarpetaCanExecute(object o)
        {
            return InstalacionSeleccionada != null && Directory.Exists(InstalacionSeleccionada.HyperlinkProperty.Value);
        }


       


        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
