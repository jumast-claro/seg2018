using System.Collections.Generic;
using System.Collections.ObjectModel;
using Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel;
using Jumast.Sisifo.Instalacion.ViewModel;

namespace Jumast.Claro.ObraCivilTendido.Mvvm.InstalacionesSeguimiento.SeguimientoDataGrid
{
    public interface IDataGridViewModelPersistor
    {
        ObservableCollection<InstalacionViewModel> ObtenerFilas();
        void GuardarFilas(IEnumerable<InstalacionViewModel> instalaciones);
    }
}