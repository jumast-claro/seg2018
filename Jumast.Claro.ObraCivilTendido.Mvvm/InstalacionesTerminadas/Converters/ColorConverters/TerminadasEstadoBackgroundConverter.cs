﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Jumast.Claro.ObraCivilTendido.Mvvm.Converters.ColorConverters
{
    public class TerminadasEstadoBackgroundConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var estado = value as string;
            switch (estado)
            {
                case "Terminada":
                    return Brushes.MediumSeaGreen;
                case "Fracasada":
                    return Brushes.Pink;
            }
            return Brushes.Transparent;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
