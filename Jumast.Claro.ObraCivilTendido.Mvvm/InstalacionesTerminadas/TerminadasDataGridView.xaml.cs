﻿using System.Windows;
using System.Windows.Controls;
using Syncfusion.UI.Xaml.Grid;

namespace Jumast.Claro.ObraCivilTendido.Mvvm
{
    /// <summary>
    /// Interaction logic for TerminadasDataGridView.xaml
    /// </summary>
    public partial class TerminadasDataGridView : UserControl
    {
        public static DependencyProperty SfDataGridProperty = DependencyProperty.Register("SfDataGrid", typeof(SfDataGrid), typeof(TerminadasDataGridView));

        public TerminadasDataGridView()
        {
            InitializeComponent();
            this.SfDataGrid = this._sfDataGridWrapper._sfDataGrid;
        }

        public SfDataGrid SfDataGrid
        {
            get { return (SfDataGrid)GetValue(SfDataGridProperty); }
            set
            {
                SetValue(SfDataGridProperty, value);
            }
        }
    }
}
