using Jumast.Claro.ObraCivilTendido.DataAcces.Abstract;
using Jumast.Claro.ObraCivilTendido.DataAcces.Concrete;
using Jumast.Claro.ObraCivilTendido.Notas;
using Jumast.Sisifo.Contratista.DataAcces;
using Jumast.Sisifo.Instalacion.ViewModel;
using Jumast.Sisifo.MesaDeDespacho.DataAcces;

namespace Jumast.Claro.ObraCivilTendido.Mvvm
{
    public class TerminadasDataGridViewModel : InstalacionesDataGridViewModel
    {
        public TerminadasDataGridViewModel(IInstalacionesFOExcelRepository excelRepository, INotasRepository notasRepository, IMdDRepository analistasMdDRepository, ICoordinadoresRepository coordinadoresRepo) : base(excelRepository, notasRepository, false, analistasMdDRepository, coordinadoresRepo)
        {
        }
    }
}