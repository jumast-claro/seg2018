﻿using System;
using System.Text;
using System.Windows;
using System.Windows.Media;
using Jumast.Sisifo.Coordinacion.Common;

namespace Jumast.Claro.ObraCivilTendido.Mvvm
{
    public class Appointment
    {
        private readonly string _contratista;
        private readonly DateTime _starTime;
        private readonly TipoDeCoordinacionEnum _tipoDeTarea;
        private readonly ResponsableDeCoordinacionEnum _responsable;
        private readonly EstadoDeCoordinacionEnum _estado;
        private readonly string _detalle;
        private readonly bool _allDay = false;

        private readonly string _resultado;
        private readonly string _responsableResultado;
        private readonly string _estadoActual;

        public string Subject => $"{Cliente} ({Location})";
        public DateTime StartTime => _starTime;


        public Appointment(string contratista, DateTime starTime, TipoDeCoordinacionEnum tipoDeTarea, EstadoDeCoordinacionEnum estado, ResponsableDeCoordinacionEnum responsable,string tareaPendiente, string resultado, string responsableResultado, string estadoActual)
        {
            _contratista = contratista;
            _starTime = starTime;
            if (starTime.Hour == 0)
            {
                //_allDay = true;

                switch (tipoDeTarea)
                {
                    case TipoDeCoordinacionEnum.ObraCivil:
                        _starTime = starTime.AddHours(7);
                        break;
                    default:
                        _starTime = starTime.AddHours(7).AddMinutes(30);
                        break;
                }

                //_starTime = starTime.AddHours(7);
            }

            //if ((estado == "Pendiente contratista" && tareaPendiente == "Proponer fecha"))
            //{
            //    _allDay = true;
            //    _starTime = DateTime.Today;
            //}




            _tipoDeTarea = tipoDeTarea;
            _resultado = resultado;
            _responsableResultado = responsableResultado;
            _estadoActual = estadoActual;
            _responsable = responsable;
            _estado = estado;
            _detalle = string.IsNullOrEmpty(tareaPendiente) ? "" : tareaPendiente;
        }


        public bool AllDay => _allDay;

        public DateTime EndTime => StartTime.AddHours(0.5);

        public TipoDeCoordinacionEnum Etapa => _tipoDeTarea;
        public EstadoDeCoordinacionEnum Estado => _estado;
        public ResponsableDeCoordinacionEnum Responsable => _responsable;
        public string Detalle => _detalle;
        public string Cliente { get; set; }
        public string MdD { get; set; }
        public string Coordinador { get; set; }
        public string Location { get; set; }
        public string Contratista => _contratista;

        public string Notes
        {
            get
            {
                var sb = new StringBuilder();
                sb.AppendLine(Etapa + ": " + "(" + Estado + " " + Detalle.ToLower() + ")");
                sb.AppendLine("MdD: " + MdD);
                sb.AppendLine("Coordinador: " + Coordinador);
                return sb.ToString();
            }
        }

        public Visibility AllDayVisibility => AllDay ? Visibility.Collapsed : Visibility.Visible;

        public Visibility CoordinacionVisibility
        {
            get
            {
                if (Estado == EstadoDeCoordinacionEnum.Coordinado)
                {
                    return Visibility.Collapsed;
                }
                return Visibility.Visible;
               
            }
        }

        public Visibility ResultadoVisibility
        {
            get
            {
                if (_resultado == "Éxito")
                {
                    return Visibility.Collapsed;
                }

                if (Estado == EstadoDeCoordinacionEnum.Coordinado && StartTime <= DateTime.Now)
                {
                    return Visibility.Visible;
                }
                return Visibility.Collapsed;

            }
        }

        public Brush EstadoActualBorderBrush
        {
            get
            {
                if (_estadoActual == "1-Análisis" || _estadoActual == "2-Definición de proyecto" || _estadoActual == "3-Asignación a contratista")
                {
                    return ApplicationResources.Estado_Definicion;
                }
                switch (_estadoActual)
                {
                    case "4-Relevamiento":
                        return ApplicationResources.Estado_Relevamiento;
                    case "5-Sondeos":
                        return ApplicationResources.Estado_Sondeos;
                    case "6-Permisos":
                        return ApplicationResources.Estado_Permisos;
                    case "7-Obra civil":
                        return ApplicationResources.Estado_ObraCivil;
                    case "7-Obra civil interna":
                        return ApplicationResources.Estado_ObraCivil;
                    case "8-Tendido":
                        return ApplicationResources.Estado_Tendido;
                    case "9-Empalmes":
                        return ApplicationResources.Estado_ODF;
                    case "9-Empalmes (ME)":
                        return ApplicationResources.Estado_ODF;
                    case "9-Empalmes (GPON)":
                        return ApplicationResources.Estado_ODF;
                    case "10-Mudanza":
                        return Brushes.RosyBrown;
                    case "11-Terminada":
                        return ApplicationResources.Estado_Terminada;
                    default:
                        return Brushes.Transparent;
                }
            }
        }

        public Brush ResponsableColor
        {
            get
            {
                if (Responsable == ResponsableDeCoordinacionEnum.Contratista)
                {
                    return Brushes.DarkViolet;
                }

                if (Responsable == ResponsableDeCoordinacionEnum.InstalacionesFO)
                {
                    return Brushes.Red;
                }

                if (Responsable == ResponsableDeCoordinacionEnum.MdD)
                {
                    return Brushes.Black;
                }

                return Brushes.Transparent;
            }
        }

        public Brush CoordinacionBorderBrush
        {
            get
            {
                if (Estado == EstadoDeCoordinacionEnum.Coordinacion)
                {
                    return Brushes.Yellow;
                }

                if (Estado == EstadoDeCoordinacionEnum.Coordinado)
                {
                    return Brushes.ForestGreen;
                }
                if (Estado == EstadoDeCoordinacionEnum.Cancelado)
                {
                    return Brushes.Red;
                }
                if (Estado == EstadoDeCoordinacionEnum.NoCoordinado)
                {
                    return Brushes.Black;
                }
                if (Estado == EstadoDeCoordinacionEnum.NoConfirmado)
                {
                    return Brushes.Black;
                }

                return Brushes.Transparent;
            }
        }

        public Brush ResultadoBorderBrush
        {
            get
            {

                if (_resultado == "Informado en cronograma")
                {
                    return Brushes.WhiteSmoke;
                }

                if (_resultado == "Ejecución")
                {
                    return Brushes.Yellow;
                }

                if (_resultado == "Cancelado")
                {
                    return Brushes.Black;
                }

                //if (_resultado == "Éxito")
                //{
                //    return Brushes.ForestGreen;
                //}

                if (_resultado == "Éxito parcial")
                {
                    return Brushes.DarkGreen;
                }

                if (_resultado == "Fracaso")
                {
                    return Brushes.Red;
                }

                return Brushes.Transparent;
            }
        }

        public Brush Background
        {
            get
            {
                switch (Etapa)
                {
                    case TipoDeCoordinacionEnum.Relevamiento:
                        return ApplicationResources.Estado_Relevamiento;
                    case TipoDeCoordinacionEnum.Sondeos:
                        return ApplicationResources.Estado_Sondeos;
                    case TipoDeCoordinacionEnum.ObraCivil:
                        return ApplicationResources.Estado_ObraCivil;
                    case TipoDeCoordinacionEnum.ObraCivilInterna:
                        return ApplicationResources.Estado_ObraCivil;
                    case TipoDeCoordinacionEnum.Tendido:
                        return ApplicationResources.Estado_Tendido;
                    case TipoDeCoordinacionEnum.Empalmes:
                        return ApplicationResources.Estado_ODF;
                    case TipoDeCoordinacionEnum.EmpalmesME:
                        return ApplicationResources.Estado_ODF;
                    case TipoDeCoordinacionEnum.EmpalmesGPON:
                        return ApplicationResources.Estado_ODF;
                    case TipoDeCoordinacionEnum.Mudanza:
                        return ApplicationResources.Estado_Mudanza;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
    }
}
