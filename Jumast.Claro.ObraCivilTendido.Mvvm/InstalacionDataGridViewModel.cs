﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Jumast.Claro.ObraCivilTendido.DataAcces.Abstract;
using Jumast.Claro.ObraCivilTendido.DataAcces.Concrete;
using Jumast.Claro.ObraCivilTendido.Mvvm.Notas;
using Jumast.Claro.ObraCivilTendido.Notas;
using Jumast.Sisifo.Contratista.DataAcces;
using Jumast.Sisifo.Instalacion.Common;
using Jumast.Sisifo.Instalacion.ViewModel;
using Jumast.Sisifo.MesaDeDespacho.DataAcces;
using Jumast.Wpf.Mvvm;
using Syncfusion.Data.Extensions;

namespace Jumast.Claro.ObraCivilTendido.Mvvm
{
    public class InstalacionesDataGridViewModel : INotifyPropertyChanged
    {

        private bool _isBusy = false;

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                OnPropertyChanged();
            }
        }

        //---------------------------------------------------------------------
        // Fields
        //---------------------------------------------------------------------
        private readonly IInstalacionesFOExcelRepository _instalacionesRepository;
        private readonly INotasRepository _notasRepository;
        private readonly bool _readDataOnInit;
        private readonly IMdDRepository _analistasMdDRepository;
        private readonly ICoordinadoresRepository _coordinadoresRepo;

        //---------------------------------------------------------------------
        // Constructor
        //---------------------------------------------------------------------
        public InstalacionesDataGridViewModel(IInstalacionesFOExcelRepository instalacionesRepository, INotasRepository notasRepository, bool readDataOnInit, IMdDRepository analistasMdDRepository, ICoordinadoresRepository coordinadoresRepo)
        {
            _instalacionesRepository = instalacionesRepository;
            _notasRepository = notasRepository;
            _readDataOnInit = readDataOnInit;
            _analistasMdDRepository = analistasMdDRepository;
            _coordinadoresRepo = coordinadoresRepo;

            Notas = new NotasViewModel(notasRepository);
            ActualizarCommand = new RelayCommand(_actualizarExecutd, o => true);
            AbrirCarpetaCommand = new RelayCommand(_abrirCarpetaExecuted, _abrirCarpetaCanExecute);
            //AgregarNotaCommand = new RelayCommand(agregarNotaExecuted, agregarNotaCanExecute);
            //EliminarNotaCommand = new RelayCommand(eleminarNotaExecuted, eliminarNotaCanExecute);
            //GuardarNotasCommand = new RelayCommand(_guardarNotasExecuted, o => true);

            VerDetallesCommand = new RelayCommand(_verDetallesExecuted, _verDetallesCanExecute);
        }



        //---------------------------------------------------------------------
        // Commands
        //---------------------------------------------------------------------
        public ICommand ActualizarCommand { get; private set; }

        private void _actualizarExecutd(object o)
        {

            try
            {
                Task.Run(() =>
                {
                    IsBusy = true;
                    _instalacionesRepository.Actualizar();
                    var instalaciones = _instalacionesRepository.ObtenerTodasLasInstalaciones();
                    foreach (var instalacion in instalaciones)
                    {
                        //var instalacionViewModel = new InstalacionViewModel(instalacion, null);
                        var instalacionViewModel = InstalacionViewModelBuilder.Init(_analistasMdDRepository, _coordinadoresRepo)
                            .WithEnlace(instalacion.Enlace)
                            .WithNumeroDeOrden(instalacion.Orden)
                            .WithCliente(instalacion.Cliente)
                            .WithDireccion(instalacion.Direccion)
                            .WithPartido(instalacion.Partido)
                            .WithFechaDeRecibido(instalacion.FechaDeRecibido)
                            .WithFechaDeCompromio(instalacion.FechaDeCompromisoVantive)
                            .WithTipoDeOrden(instalacion.TipoDeOrden)
                            .WithTecnologia(instalacion.Tecnologia)
                            .WithContratistaEnCurso(instalacion.ContratistaEnCurso)
                            .WithDiasDeRecibido(instalacion.DiasDeRecibido)
                            .WithFechaDeCierre(instalacion.FechaDeCierre)
                            .WithAnalistaInstalacionesFO(instalacion.Hipervisor)
                            .WithEtapa(EtapaEnum.indefinida)
                            .WithEstado("")
                            .WithResponsable("")
                            .WithTareaPendiente("")
                            .WithFecha(null)
                            .WithRelevador("")
                            .WithAnalistaMdD("")
                            .WithObservaciones("")
                            .WithHiperlink("")
                            .Build();

                        Instalaciones.Add(instalacionViewModel);
                    }
                    IsBusy = false;
                }).ContinueWith(t =>
                {
                    var flattened = t.Exception.Flatten();
                    flattened.Handle(ex =>
                    {
                        MessageBox.Show("Error:" + ex.Message);
                        IsBusy = false;
                        return false;
                    });
                }, TaskContinuationOptions.OnlyOnFaulted);
            }

            catch (AggregateException e)
            {
                MessageBox.Show(e.Message);
            }


        }

        public ICommand AbrirCarpetaCommand { get; private set; }
        private void _abrirCarpetaExecuted(object o)
        {
            try
            {
                Process.Start(new ProcessStartInfo(InstalacionSeleccionada.HyperlinkProperty.Value));
            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
            }
        }
        private bool _abrirCarpetaCanExecute(object o)
        {
            return InstalacionSeleccionada != null && Directory.Exists(InstalacionSeleccionada.HyperlinkProperty.Value);
        }

        public ICommand VerDetallesCommand { get; private set; }

        private void _verDetallesExecuted(object o)
        {
            //var instalacionesViewModel = InstalacionSeleccionada.Model;
            //var view = new InstalacionDetalleView();
            //var viewModel = new InstalacionDetalleViewModel(instalacionesViewModel, null);



            //view.DataContext = viewModel;

            //var window = new Window();
            //window.Owner = Application.Current.MainWindow;
            //window.Content = view;

            //window.WindowState = WindowState.Maximized;
            //window.WindowStartupLocation = WindowStartupLocation.CenterScreen;

            //window.Show();

        }
        private bool _verDetallesCanExecute(object o)
        {
            return InstalacionSeleccionada != null;
        }


        //---------------------------------------------------------------------
        // Properties
        //---------------------------------------------------------------------
        private ObservableCollection<InstalacionViewModel> _viewModels = new ObservableCollection<InstalacionViewModel>();
        public ObservableCollection<InstalacionViewModel> Instalaciones
        {
            get { return _viewModels; }
            set
            {
                if (value == _viewModels) return;
                _viewModels = value;
                OnPropertyChanged();
            }
        }

        private InstalacionViewModel _instalacionSeleccionada;
        public InstalacionViewModel InstalacionSeleccionada
        {
            get { return _instalacionSeleccionada; }
            set
            {
                if (value != _instalacionSeleccionada)
                {
                    _instalacionSeleccionada = value;
                    OnPropertyChanged();


                    if (InstalacionSeleccionada == null)
                    {
                        Notas.Notas.Clear();
                    }
                    else
                    {
                        var enlace = InstalacionSeleccionada.NumeroDeEnlaceProperty.Value;
                        //var notas = _notasRepository.TodasLasNotas().Where(n => enlace.Contains(n.Enlace));
                        //Notas.Notas = notas.ToObservableCollection();
                        Notas.Enlace = enlace;
                    }
                }
            }
        }

        private NotasViewModel _notas;
        public NotasViewModel Notas
        {
            get { return _notas; }
            set
            {
                _notas = value;
                OnPropertyChanged();
            }
        }

        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
