using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Jumast.Claro.ObraCivilTendido.Mvvm.Converters.ColorConverters
{
    public class EstadoColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var estado = value as string;

            if (string.IsNullOrEmpty(estado) || estado == "1-Análisis" || estado == "1-Análisis/Definición de tarea" || estado == "2-Definición de proyecto" || estado == "3-Asignación a contratista")
            {
                return ApplicationResources.Estado_Definicion;
            }
            if (stringsAreEqualIgnoringCase(estado, "4-Relevamiento"))
            {
                return ApplicationResources.Estado_Relevamiento;
            }
            if (stringsAreEqualIgnoringCase(estado, "5-Sondeos"))
            {
                return ApplicationResources.Estado_Sondeos;
            }
            if (stringsAreEqualIgnoringCase(estado, "6-Permisos"))
            {
                return ApplicationResources.Estado_Permisos;
            }
            if (stringsAreEqualIgnoringCase(estado, "7-Obra Civil"))
            {
                var r = ApplicationResources.Estado_ObraCivil;
                return r;
            }
            if (stringsAreEqualIgnoringCase(estado, "8-Tendido"))
            {
                return ApplicationResources.Estado_Tendido;
            }
            if (stringsAreEqualIgnoringCase(estado, "9-Empalmes") || estado == "9-Empalmes (ME)" || estado == "9-Empalmes (GPON)")
            {
                return ApplicationResources.Estado_ODF;
            }
            if (stringsAreEqualIgnoringCase(estado, "Cierre"))
            {
                return ApplicationResources.Estado_Terminada;
            }
            if (stringsAreEqualIgnoringCase(estado, "Terminado") || estado == "11-Terminada")
            {
                return ApplicationResources.Estado_Terminada;
            }
            if (stringsAreEqualIgnoringCase(estado, "10-Mudanza"))
            {
                return Brushes.RosyBrown;
            }
            return new SolidColorBrush(Colors.Transparent);

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private bool stringsAreEqualIgnoringCase(string a, string b)
        {
            return string.Compare(a, b, StringComparison.CurrentCultureIgnoreCase) == 0;
        }
    }
}