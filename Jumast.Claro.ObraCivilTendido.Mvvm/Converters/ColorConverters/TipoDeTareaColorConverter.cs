﻿using System;
using System.Globalization;
using System.Windows.Data;
using Jumas.Claro.ObraCivilTendido.Model;

namespace Jumast.Claro.ObraCivilTendido.Mvvm.Converters.ColorConverters
{
    public class TipoDeTareaColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var valor = (TipoDeTarea) value;
            switch (valor)
            {
                case TipoDeTarea.ObraCivil:
                    return ApplicationResources.TipoDeTarea_ObraCivil;
                case TipoDeTarea.Tendido:
                    return ApplicationResources.TipoDeTarea_Tendido;
                case TipoDeTarea.Indefinido:
                    return ApplicationResources.TipoDeTarea_Indefinido;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
