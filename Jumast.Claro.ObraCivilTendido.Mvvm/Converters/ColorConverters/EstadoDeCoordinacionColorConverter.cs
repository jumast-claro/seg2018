﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using Jumast.Sisifo.Coordinacion.Common;

namespace Jumast.Claro.ObraCivilTendido.Mvvm.Converters.ColorConverters
{
    public class EstadoDeCoordinacionColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var valor = (EstadoDeCoordinacionEnum) value;

            switch (valor)
            {
                case EstadoDeCoordinacionEnum.Pendiente:
                    return Brushes.Yellow;
                case EstadoDeCoordinacionEnum.Coordinacion:
                    return Brushes.Red;
                case EstadoDeCoordinacionEnum.Coordinado:
                    return Brushes.Green;
                case EstadoDeCoordinacionEnum.Cancelado:
                    return Brushes.DarkViolet;
                case EstadoDeCoordinacionEnum.NoConfirmado:
                    return Brushes.DarkViolet;
                case EstadoDeCoordinacionEnum.NoCoordinado:
                    return Brushes.DarkViolet;
                case EstadoDeCoordinacionEnum.Terminado:
                    return Brushes.Transparent;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}