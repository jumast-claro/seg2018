﻿using System.Collections.Generic;
using Jumast.Claro.ObraCivilTendido.Model.Abstract;

namespace Jumast.Claro.ObraCivilTendido.DataAcces.Abstract
{
    public interface IInstalacionesFOExcelRepository
    {
        void Actualizar();
        IInstalacionFO ObtenerInstalacionParaEnlace(string enlace);
        IEnumerable<IInstalacionFO> ObtenerTodasLasInstalaciones();
    }
   
}