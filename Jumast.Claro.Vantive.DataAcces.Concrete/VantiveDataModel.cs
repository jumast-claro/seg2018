﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jumast.Claro.Vantive.DataAcces.Abstract;

namespace Jumast.Claro.Vantive.DataAcces.Concrete
{
    public class VantiveDataModel : IVantiveDataModel
    {
        public string Inbox { get; set; }
        public DateTime? FechaDeInstalacion { get; set; }
        public string Identificador { get; set; }
        public string ID { get; set; }
        public string Cliente { get; set; }
        public string Localidad { get; set; }
        public string Provincia { get; set; }
        public string TomadoPor { get; set; }
        public string Estado { get; set; }
        public string MotivoInterrupcion { get; set; }
        public string MotivoOrden { get; set; }
        public DateTime? FechaDeRecibido { get; set; }
        public string Ejecutivo { get; set; }
        public string UnidadDeNegocio { get; set; }
        public string IdObjeto { get; set; }
        public string Clase { get; set; }
    }
}
