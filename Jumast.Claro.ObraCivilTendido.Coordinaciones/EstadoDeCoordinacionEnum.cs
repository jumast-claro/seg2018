namespace Jumast.Sisifo.Coordinacion.Common
{
    public enum EstadoDeCoordinacionEnum
    {
        Pendiente = 1,
        Coordinacion = 2,
        Coordinado = 3,
        Cancelado = 4,
        NoConfirmado = 5,
        NoCoordinado = 6,
        Terminado
    }
}