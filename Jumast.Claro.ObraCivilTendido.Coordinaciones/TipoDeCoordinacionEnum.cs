namespace Jumast.Sisifo.Coordinacion.Common
{
    public enum TipoDeCoordinacionEnum
    {
        Relevamiento,
        Sondeos,
        ObraCivil,
        ObraCivilInterna,
        Tendido,
        Empalmes,
        EmpalmesME,
        EmpalmesGPON,
        Mudanza
    }
}