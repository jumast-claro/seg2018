namespace Jumast.Sisifo.Coordinacion.Common
{
    public enum ResponsableDeCoordinacionEnum
    {
        MdD,
        PM,
        InstalacionesFO,
        Contratista,
        Cliente,
        none
    }
}