﻿namespace Jumas.Claro.ObraCivilTendido.Model
{
    public enum TipoDeTarea
    {
        ObraCivil,
        Tendido,
        Indefinido,
    }
}