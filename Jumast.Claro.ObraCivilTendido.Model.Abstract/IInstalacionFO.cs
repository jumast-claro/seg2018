﻿using System;
using Jumas.Claro.ObraCivilTendido.Model;
using Jumast.Claro.BusinessObject;

namespace Jumast.Claro.ObraCivilTendido.Model.Abstract
{
    public interface IInstalacionFO
    {
        string Id { get; set; }
        string Cliente { get; set; }
        string Link { get; set; }
        string ContratistaEnCurso { get; set; }
        string Direccion { get; set; }
        string DocumentadoWebGis { get; set; }
        string Enlace { get; set; }
        DateTime? FechaDeAsignado { get; set; }
        DateTime? FechaDeCierre { get; set; }
        DateTime? FechaDeCompromisoVantive { get; set; }
        DateTime? FechaDeInformeRecibido { get; set; }
        DateTime? FechaDeRecibido { get; set; }
        string Hipervisor { get; set; }
        double? MetrosDeObraCivil { get; set; }
        double? MetrosDeTendido { get; set; }
        string Orden { get; set; }
        string Partido { get; set; }
        string Tecnologia { get; set; }
        string TipoDeOrden { get; set; }
        double? ValorRelevado { get; set; }

        double? DiasDeRecibido { get; }
        Vencida Vencida { get; }
        double? DiasParaVencimiento { get; }

        TipoDeTarea TipoDeTarea { get; }

        double? DiasDeRelevamiento { get; }

        Zona Zona { get; }

        double? DiasHastaAsignarRelevamiento { get; }

        string Etapa { get; set; }
        string Observaciones { get; set; }

        double? DiasDeInstalacion { get; }

        Vencida VencidaInstalacionesFO { get; }
        double? DiasParaVencimientoInstalacionesFO { get; }


    }
}