﻿using System.Collections.Generic;

namespace Jumast.Sisifo.MesaDeDespacho.DataAcces
{
    /// <summary>
    /// Interfaz para acceder a información relacionada a la mesa de despacho.
    /// </summary>
    public interface IMdDRepository
    {
        /// <summary>
        /// Devuelve una lista con los nombres completos de los analistas de la mesa de despacho.
        /// </summary>
        List<string> NombresDeAnalistas();
    }
}