﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic.FileIO;

namespace Jumast.Sisifo.MesaDeDespacho.DataAcces
{
    public class MdDCsvRepository : IMdDRepository
    {
        private readonly string _fullFilePathWithExtension;

        /// <summary>
        /// Inicializa una nueva instancia del repositorio.
        /// </summary>
        /// <param name="fullFilePathWithExtension">Nombre completo con extensión del archivo csv que tiene la información.
        /// Por ejemplo: @"C:\Users\Jumast\Desktop\SEG2018_DATA\analistas_mdd.csv
        /// </param>
        public MdDCsvRepository(string fullFilePathWithExtension)
        {
            _fullFilePathWithExtension = fullFilePathWithExtension;
        }

        /// <summary>
        /// Devuelve una lista con los nombres completos de los analistas de la mesa de despacho.
        /// </summary>
        /// <exception>Todas las que puedan ocurrir al leer el archivoi csv.</exception>
        public List<string> NombresDeAnalistas()
        {
            var nombres = new List<string>();
            using (var parser = new TextFieldParser(_fullFilePathWithExtension, Encoding.UTF8))
            {
                parser.SetDelimiters(",");
                while (!parser.EndOfData)
                {
                    string[] fields = parser.ReadFields();
                    var nombre = fields[0];
                    nombres.Add(nombre);
                }
            }
            return nombres;
        }

    }
}
