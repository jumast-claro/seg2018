﻿using System;
using System.Collections.Generic;

namespace Jumast.Claro.Vantive.Model.Abstract
{
    public interface ITareasVantiveModelRepository
    {
        IEnumerable<ITareaVantive> SelectAll();
    }
}