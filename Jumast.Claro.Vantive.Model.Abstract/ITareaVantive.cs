﻿using System;
using Jumast.Claro.BusinessObject.Abstract;

namespace Jumast.Claro.Vantive.Model.Abstract
{
    public interface ITareaVantive
    {
        string Inbox { get; set; }
        string Clase { get; set; }
        string Cliente { get; set; }
        string Ejecutivo { get; set; }
        string Estado { get; set; }
        //DateTime? FechaInstalacion { get; set; }
        //DateTime FechaDeRecibido { get; set; }
        string NumeroDeOrden { get; set; }
        string Identificador { get; set; }
        string IdTarea { get; set; }
        string Localidad { get; set; }
        string MotivoInterrupcion { get; set; }
        string MotivoOrden { get; set; }
        string Provincia { get; set; }
        string TomadoPor { get; set; }
        string UnidadDeNegocio { get; set; }

        string Producto { get; }
        string Enlace { get; }

        IFechaDeInicio FechaInicioTarea { get; }
        IFechaDeCompromiso FechaInstalacion { get; }
    }
}