using Jumast.Claro.ObraCivilTendido.DesktopUI.TareasVantive;
using Jumast.Claro.Vantive.DataAcces.Abstract;
using Jumast.Claro.Vantive.DataAcces.Concrete;
using Jumast.Claro.Vantive.Model.Abstract;
using Ninject.Modules;

namespace Jumast.Claro.ObraCivilTendido.DesktopUI.DependencyInjection.Ninject
{
    public class TareasVantiveNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ITareasVantiveModelRepository>().To<TareasVantiveModelRepository>();


            var repo = new TareasVantiveCsvDataRepository();
            repo.PermitirTareasDuplicadas = true;
            repo.ReadFile(@"C:\Users\Jumast\Desktop\SEG2018_DATA\db_Obra Civil Tend AMBA", "Obra Civil Tend AMBA");
            repo.ReadFile(@"C:\Users\Jumast\Desktop\SEG2018_DATA\db_Instalaciones FO", "Instalaciones FO");
            repo.ReadFile(@"C:\Users\Jumast\Desktop\SEG2018_DATA\db_Permisos Municipales", "Permisos Municipales");


            Bind<ITareasVantiveDataRepository>().ToConstant(repo);
        }
    }
}