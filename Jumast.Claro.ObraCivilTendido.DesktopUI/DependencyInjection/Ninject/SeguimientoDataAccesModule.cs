﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Jumast.Claro.ObraCivilTendido.DataAcces.Abstract;
using Jumast.Claro.ObraCivilTendido.DataAcces.Concrete;
using Jumast.Claro.ObraCivilTendido.DataAcces.ExcelRepository;
using Jumast.Claro.ObraCivilTendido.Mvvm;
using Jumast.Claro.ObraCivilTendido.Mvvm.InstalacionesSeguimiento.SeguimientoDataGrid;
using Jumast.Claro.ObraCivilTendido.Notas;
using Jumast.Sisifo.Contratista.DataAcces;
using Jumast.Sisifo.Coordinacion.DataAcces;
using Jumast.Sisifo.Instalacion.ViewModel;
using Jumast.Sisifo.MesaDeDespacho.DataAcces;
using Ninject.Modules;

namespace Jumast.Claro.ObraCivilTendido.DesktopUI.DependencyInjection.Ninject
{
    public class SeguimientoDataAccesModule : NinjectModule
    {
        public override void Load()
        {

            var ruta_compartida = @"S:\Implantacion\Servicios Fijos\Servicios Fijos\AMBA\Instalaciones\Planta Externa\Seguimiento Instalaciones FO.xlsx";
            var ruta_local = @"C:\Users\Jumast\Desktop\SEG2018_DATA\Seguimiento Instalaciones FO.xlsx";
            string ruta = null;

            if (!File.Exists(ruta_compartida))
            {
                MessageBox.Show("No se econtró el archivo en la ruta compartida. Se utilizará la versión local");

                if (!File.Exists(ruta_local))
                {
                    MessageBox.Show("No se econtró el archivo en la ruta loca. El programa no puede continuar");
                    return;
                }

                else
                {
                    ruta = ruta_local;
                }
            }

            else
            {
                ruta = ruta_compartida;
            }



            Bind<IInstalacionesFOExcelRepository>().ToConstant(new SeguimientoExcelRepository(ruta));
            Bind<IInstalacionesFOExcelRepository>().ToConstant(new TerminadasExcelRepository(ruta)).WhenInjectedExactlyInto<TerminadasDataGridViewModel>(); ;
            //Bind<IInstalacionesFOExcelRepository>().ToConstant(new SeguimientoExcelRepository(@"C:\Users\Jumast\Desktop\SEG2018_DATA\Seguimiento Instalaciones FO.xlsx"));
            //Bind<IInstalacionesFOExcelRepository>().ToConstant(new TerminadasExcelRepository(@"C:\Users\Jumast\Desktop\SEG2018_DATA\Seguimiento Instalaciones FO.xlsx")).WhenInjectedExactlyInto<TerminadasDataGridViewModel>();
            Bind<InfoAdicionalRepository>().ToConstant(new InfoAdicionalRepository(@"C:\Users\Jumast\Desktop\SEG2018_DATA\InfoAdicional.xlsx"));
            Bind<ICoordinacionesRepository>().ToConstant(new CoordinacionesRepository(@"C:\Users\Jumast\Desktop\SEG2018_DATA\Coordinaciones.xlsx"));


            Bind<INotasRepository>().ToConstant(new CsvNotasRepository(@"C:\Users\Jumast\Desktop\SEG2018_DATA\db_notas.csv"));

            Bind<ICalendarStrategy>().To<CalendarStrategy>();
            Bind<ISendMailStrategy>().To<SendMailStrategy>();

            Bind<IMdDRepository>().ToConstant(new MdDCsvRepository(@"C:\Users\Jumast\Desktop\SEG2018_DATA\analistas_mdd.csv"));
            Bind<ICoordinadoresRepository>().ToConstant(new CoordinadoresCsvRepository(@"C:\Users\Jumast\Desktop\SEG2018_DATA\contratistas_coordinadores.csv"));
            Bind<IDataGridViewModelPersistor>().To<DataGridViewModelPersistor>();
            Bind<ICoordinador>().To<Coordinador>().InSingletonScope();
        }
    }
}
