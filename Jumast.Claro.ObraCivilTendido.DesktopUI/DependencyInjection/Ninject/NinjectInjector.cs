﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using Ninject.Modules;

namespace Jumast.Claro.ObraCivilTendido.DesktopUI.DependencyInjection.Ninject
{
    public class NinjectInjector
    {

        //---------------------------------------------------------------------
        // Campos
        //---------------------------------------------------------------------
        private readonly IKernel _kernel;

        //---------------------------------------------------------------------
        // Constructores
        //---------------------------------------------------------------------
        public NinjectInjector()
        {
            this._kernel = new StandardKernel();
        }

        public NinjectInjector(INinjectModule module)
        {
            this._kernel = new StandardKernel(module);
        }

        public NinjectInjector(INinjectModule[] modules)
        {
            this._kernel = new StandardKernel(modules);
        }

        //---------------------------------------------------------------------
        // Métodos
        //---------------------------------------------------------------------
        public T Resolve<T>()
        {
            return _kernel.Get<T>();
        }

    }
}
