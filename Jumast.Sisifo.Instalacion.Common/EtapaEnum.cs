﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumast.Sisifo.Instalacion.Common
{
    public enum EtapaEnum
    {
        indefinida,
        Analisis,
        DefinicionDeProyecto,
        AsignacionAContratista,
        Relevamiento,
        Sondeos,
        Permisos,
        ObraCivil,
        ObraCivilInterna,
        Tendido,
        Empalmes,
        EmpalmesME,
        EmpalmesGPON,
        Mudanza,
        Terminada,
    }
}
