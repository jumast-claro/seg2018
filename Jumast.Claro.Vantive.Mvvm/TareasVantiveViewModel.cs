﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Jumast.Claro.Vantive.Model.Abstract;
using Jumast.Wpf.Mvvm;
using Syncfusion.Data.Extensions;

namespace Jumast.Claro.Vantive.Mvvm
{
    public class TareasVantiveViewModel : INotifyPropertyChanged
    {
       

        public TareasVantiveViewModel(ITareasVantiveModelRepository modelRepository)
        {
            var tareas = modelRepository.SelectAll();

            foreach (var tarea in tareas)
            {
                var viewModel = new TareaVantiveViewModel(tarea);   
                _all.Add(viewModel);
                _current.Add(viewModel);
            }

            MostrarDetallesCommand = new RelayCommand(_mostrarDetallesExecuted, _mostrarDetallesCanExecute);
            EliminarTareasDuplicadasCommand = new RelayCommand(_eliminarTareasDuplicadasExecuted, _eliminarTareasDuplicadasCanExecute);
            VerTodoCommand = new RelayCommand(_verTodoExecuted, _verTodoCanExecute);


        }

        private readonly ObservableCollection<TareaVantiveViewModel> _all = new ObservableCollection<TareaVantiveViewModel>();
        private ObservableCollection<TareaVantiveViewModel> _current = new ObservableCollection<TareaVantiveViewModel>();

        public ObservableCollection<TareaVantiveViewModel> Tareas
        {
            get { return _current; }
            set
            {
                _current = value; 
                OnPropertyChanged();
            }
        }


        private TareaVantiveViewModel _tareaSeleccionada;

        public TareaVantiveViewModel TareaSeleccionada
        {
            get
            {
                return _tareaSeleccionada;
            }
            set
            {
                if(value == _tareaSeleccionada) return;
                _tareaSeleccionada = value; 
                OnPropertyChanged();
            }
        }

        public ICommand VerTodoCommand { get; set; }

        public void _verTodoExecuted(object o)
        {
            Tareas = _all;
        }

        public bool _verTodoCanExecute(object o)
        {
            return Tareas != _all;
        }

        public ICommand EliminarTareasDuplicadasCommand { get; set; }
        public void _eliminarTareasDuplicadasExecuted(object o)
        {
            var tareas = _all.Where(t => t.InboxProperty.Value == "Obra Civil Tend AMBA").ToList();
            var tareasInstalaciones = _all.Where(t => t.InboxProperty.Value == "Instalaciones FO");
            var tareasPermisos = _all.Where(t => t.InboxProperty.Value == "Permisos Municipales");

            foreach (var tarea in tareasInstalaciones)
            {
                if (tareas.All(t => t.IdentificadorProperty.Value != tarea.IdentificadorProperty.Value))
                {
                    tareas.Add(tarea);
                }
            }

            foreach (var tarea in tareasPermisos)
            {
                //if (tareas.All(t => t.IdentificadorProperty.Value != tarea.IdentificadorProperty.Value))
                //{
                    tareas.Add(tarea);
                //}
            }

            Tareas = tareas.ToObservableCollection();

        }

        public bool _eliminarTareasDuplicadasCanExecute(object o)
        {
            return true;
        }


        public ICommand MostrarDetallesCommand { get; private set; }

        private void _mostrarDetallesExecuted(object o)
        {
            var detallesView = new DetallesView();
            var detallesViewModel = new DetalleViewModel(TareaSeleccionada.Model);
            detallesView.DataContext = detallesViewModel;

            var window = new Window();
            window.Width = 950;
            window.Height = 600;
            window.Content = detallesView;
            window.Show();

        }

        private bool _mostrarDetallesCanExecute(object o)
        {
            return TareaSeleccionada != null;
        }

        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
