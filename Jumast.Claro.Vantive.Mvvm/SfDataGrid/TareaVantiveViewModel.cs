﻿using System;
using Jumast.Claro.BusinessObject.Mvvm;
using Jumast.Claro.Vantive.Model.Abstract;
using Jumast.Wpf.Mvvm.BindableProperties;

namespace Jumast.Claro.Vantive.Mvvm
{
    public class TareaVantiveViewModel
    {
        private readonly ITareaVantive _model;

        public TareaVantiveViewModel(ITareaVantive model)
        {
            _model = model;
            InboxProperty = new ReadOnlyProperty<string>(model.Inbox);
            //FechaDeInstalacionProperty = new ReadOnlyProperty<DateTime?>(model.FechaInstalacion);
            IdentificadorProperty = new ReadOnlyProperty<string>(model.Identificador);
            NumeroDeOrdenProperty = new ReadOnlyProperty<string>(model.NumeroDeOrden);
            ClienteProperty = new ReadOnlyProperty<string>(model.Cliente);
            LocalidadProperty = new ReadOnlyProperty<string>(model.Localidad);
            ProvinciaProperty = new ReadOnlyProperty<string>(model.Provincia);
            TomadoPorProperty = new ReadOnlyProperty<string>(model.TomadoPor);
            EstadoProperty = new ReadOnlyProperty<string>(model.Estado);
            MotivoDeInterrupcionProperty = new ReadOnlyProperty<string>(model.MotivoInterrupcion);
            //FechaDeRecibidoProperty = new ReadOnlyProperty<DateTime>(model.FechaInicioTarea.Fecha);
            EjecutivoProperty = new ReadOnlyProperty<string>(model.Ejecutivo);
            UnidadDeNegocioProperty = new ReadOnlyProperty<string>(model.UnidadDeNegocio);
            IdTareaProperty = new ReadOnlyProperty<string>(model.IdTarea);
            ClaseProperty = new ReadOnlyProperty<string>(model.Clase);

            ProductoProperty = new ReadOnlyProperty<string>(model.Producto);
            EnlaceProperty = new ReadOnlyProperty<string>(model.Enlace);

            FechaInicioTareaProperty = new FechaDeInicioProperty(model.FechaInicioTarea);
            FechaInstalacionProperty = new FechaCompromisoProperty(model.FechaInstalacion);
        }


        public IBindableProperty<string> InboxProperty { get; private set; }
        //public IBindableProperty<DateTime?> FechaDeInstalacionProperty { get; private set; }
        public IBindableProperty<string> IdentificadorProperty { get; private set; }
        public IBindableProperty<string> NumeroDeOrdenProperty { get; private set; }
        public IBindableProperty<string> ClienteProperty { get; set; }
        public IBindableProperty<string> LocalidadProperty { get; private set; }
        public IBindableProperty<string> ProvinciaProperty { get; private set; }
        public IBindableProperty<string> TomadoPorProperty { get; set; }
        public IBindableProperty<string> EstadoProperty { get; private set; }
        public IBindableProperty<string> MotivoDeInterrupcionProperty { get; private set; }
        //public IBindableProperty<DateTime> FechaDeRecibidoProperty { get; private set; }
        public IBindableProperty<string> EjecutivoProperty { get; private set; } 
        public IBindableProperty<string> UnidadDeNegocioProperty { get; private set; }
        public IBindableProperty<string> IdTareaProperty { get; private set; }
        public IBindableProperty<string> ClaseProperty { get; private set; }

        public IBindableProperty<string> ProductoProperty { get; private set; }
        public IBindableProperty<string> EnlaceProperty { get; private set; }

        public FechaDeInicioProperty FechaInicioTareaProperty { get; private set; }
        public FechaCompromisoProperty FechaInstalacionProperty { get; private set; }

        public ITareaVantive Model => _model;
    }
}
