﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Jumast.Wpf.CustomControls
{
    public class PropertyView : UserControl
    {

        public static readonly DependencyProperty PropertyNameProperty = DependencyProperty.Register("PropertyName", typeof(string), typeof(PropertyView), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static readonly DependencyProperty PropertyValueProperty = DependencyProperty.Register("PropertyValue", typeof(string), typeof(PropertyView), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));


        public string PropertyName
        {
            get { return (string)GetValue(PropertyNameProperty); }
            set
            {
                SetValue(PropertyNameProperty, value);
            }
        }

        public string PropertyValue
        {
            get { return (string)GetValue(PropertyValueProperty); }
            set
            {
                SetValue(PropertyValueProperty, value);
            }
        }
    }
}
