﻿using System.Windows;
using System.Windows.Media;

namespace Jumast.Wpf.CustomControls
{
    public class CustomButton : System.Windows.Controls.Button
    {
        public static readonly DependencyProperty MouseOverBackgroundProperty = DependencyProperty.Register("MouseOverBackground", typeof(Brush), typeof(CustomButton), new FrameworkPropertyMetadata(Brushes.DarkMagenta, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public Brush MouseOverBackground
        {
            get
            {
                return (Brush)GetValue(MouseOverBackgroundProperty);
            }
            set
            {
                SetValue(MouseOverBackgroundProperty, value);
            }
        }
    }
}
