﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Jumast.Wpf.Mvvm.BindableProperties
{

    public class ReadOnlyProperty<T> : IBindableProperty<T>
    {
      

        public ReadOnlyProperty(T value)
        {
            _value = value;
        }

        private T _value;
        public T Value
        {
            get { return _value; }
            set
            {
                throw new InvalidOperationException();
            }
        }

        public string DisplayString
        {
            get { return _value.ToString(); }
           
        }

        //------------------------------------------------------ 
        //  INotifyPropertyChanged
        //------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class ReadWriteProperty<T> : IBindableProperty<T>
    {
        public ReadWriteProperty(T initialVvalue)
        {
            _value = initialVvalue;
        }

        private T _value;
        public T Value
        {
            get { return _value; }
            set
            {
                if (value != null && value.Equals(_value))
                {
                    return;
                }
                _value = value;
                OnPropertyChanged();
            }
        }

        public string DisplayString
        {
            get { return _value.ToString(); }
            //set
            //{
            //    throw new InvalidOperationException();
            //}
        }

        //------------------------------------------------------ 
        //  INotifyPropertyChanged
        //------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class ReadWritePropertyWithRestrictedValues<T> : IBindableProperty<T>
    {
        public ReadWritePropertyWithRestrictedValues(T initialVvalue)
        {
            _value = initialVvalue;
            _allowedValues = new List<T>(){default(T)};
        }

        private T _value;
        public virtual T Value
        {
            get { return _value; }
            set
            {
                if (value != null && value.Equals(_value))
                {
                    return;
                }
                _value = value;
                OnPropertyChanged();
            }
        }

        public string DisplayString
        {
            get { return _value.ToString(); }
          
        }

        protected IEnumerable<T> _allowedValues;
        public virtual IEnumerable<T> AllowedValues => _allowedValues;


        //------------------------------------------------------ 
        //  INotifyPropertyChanged
        //------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }


    public interface IEnumBindableProperty<T> : INotifyPropertyChanged
    {
        T Value { get; set; }
        string StringValue { get; }
    }

   

    public abstract class EnumBindableProperty<T> : IEnumBindableProperty<T>
    {
        protected abstract string EnumToString(T @enum);
        protected abstract T StringToEnum(string s);

        protected EnumBindableProperty(T initialValue)
        {
            //_value = initialVvalue;
            Value = initialValue;
        }

        private string _stringValue;
        public string StringValue
        {
            //get => _stringValue;
            get => EnumToString(_value);
            set
            {
                _stringValue = value;
                _value = StringToEnum(value);
                OnPropertyChanged();
                OnPropertyChanged(nameof(Value));
            }
        }

        private T _value;
        public  T Value
        {
            get { return _value; }
            set
            {
                _value = value;
                _stringValue = EnumToString(value);
                OnPropertyChanged(nameof(StringValue));
            }
            
        }

        private List<string> _allowedValues;

        public virtual IEnumerable<string> AllowedValues
        {
            get
            {
                if (_allowedValues == null)
                {
                    _allowedValues= new List<string>();
                    var values = Enum.GetValues(typeof(T)).Cast<T>();
                    foreach (var value in values)
                    {
                        _allowedValues.Add(EnumToString(value));
                    }
                }
                return _allowedValues;
            }
        }


        //------------------------------------------------------ 
        //  INotifyPropertyChanged
        //------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }


}
