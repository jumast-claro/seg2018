using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Jumast.Wpf.Mvvm.BindableProperties
{
    public interface IBindableProperty<TValue> : INotifyPropertyChanged
    {

        TValue Value { get; set; }
        string DisplayString { get; }
    }

    public class LambaBindbableProperty<TValue> : IBindableProperty<TValue>
    {
        private readonly Func<TValue> _func;

        public LambaBindbableProperty(Func<TValue> funcion)
        {
            _func = funcion;
        }

        public TValue Value
        {
            get => _func.Invoke();
            set => throw new NotImplementedException();
        }

        public string DisplayString { get; }

        //------------------------------------------------------ 
        //  INotifyPropertyChanged
        //------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}