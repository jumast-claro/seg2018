﻿using System;
using Jumast.Sisifo.Coordinacion.Common;

namespace Jumast.Claro.ObraCivilTendido.DataAcces.ExcelRepository
{
    internal class ResponsableEnumConverter
    {
        public ResponsableDeCoordinacionEnum StringToEnum(string s)
        {
            switch (s)
            {
                case "":
                    return ResponsableDeCoordinacionEnum.none;
                case "MdD":
                    return ResponsableDeCoordinacionEnum.MdD;
                case "PM":
                    return ResponsableDeCoordinacionEnum.PM;
                case "Instalaciones FO":
                    return ResponsableDeCoordinacionEnum.InstalacionesFO;
                case "Contratista":
                    return ResponsableDeCoordinacionEnum.Contratista;
                case "Cliente":
                    return ResponsableDeCoordinacionEnum.Cliente;
                default:
                    throw new ArgumentOutOfRangeException(nameof(s), s, null);
            }
        }

        public string EnumToString(ResponsableDeCoordinacionEnum @enum)
        {
            switch (@enum)
            {
                case ResponsableDeCoordinacionEnum.none:
                    return "";
                case ResponsableDeCoordinacionEnum.MdD:
                    return "MdD";
                case ResponsableDeCoordinacionEnum.PM:
                    return "PM";
                case ResponsableDeCoordinacionEnum.InstalacionesFO:
                    return "Instalaciones FO";
                case ResponsableDeCoordinacionEnum.Contratista:
                    return "Contratista";
                case ResponsableDeCoordinacionEnum.Cliente:
                    return "Cliente";
                default:
                    throw new ArgumentOutOfRangeException(nameof(@enum), @enum, null);
            }
        }
    }
}