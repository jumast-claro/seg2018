﻿using System;
using Jumast.Sisifo.Coordinacion.Common;

namespace Jumast.Claro.ObraCivilTendido.DataAcces.ExcelRepository
{
    internal class EstadoDeCoordinacionEnumConverter
    {
        public EstadoDeCoordinacionEnum ToEnum(string s)
        {
            switch (s)
            {
                case "1-Pendiente":
                    return EstadoDeCoordinacionEnum.Pendiente;
                case "2-Coordinación":
                    return EstadoDeCoordinacionEnum.Coordinacion;
                case "3-Coordinado":
                    return EstadoDeCoordinacionEnum.Coordinado;

                case "2-Coordinado":
                    return EstadoDeCoordinacionEnum.Coordinado;

                case "4-Cancelado":
                    return EstadoDeCoordinacionEnum.Cancelado;
                case "5-No confirmado":
                    return EstadoDeCoordinacionEnum.NoConfirmado;
                case "5-No coordinado":
                    return EstadoDeCoordinacionEnum.NoCoordinado;
                default:
                    throw new ArgumentOutOfRangeException(nameof(s), s, null);
            }
        }

        public string ToString(EstadoDeCoordinacionEnum @enum)
        {
            switch (@enum)
            {
                case EstadoDeCoordinacionEnum.Pendiente:
                    return "1-Pendiente";
                case EstadoDeCoordinacionEnum.Coordinacion:
                    return "2-Coordinación";
                case EstadoDeCoordinacionEnum.Coordinado:
                    return "3-Coordinado";
                case EstadoDeCoordinacionEnum.Cancelado:
                    return "4-Cancelado";
                case EstadoDeCoordinacionEnum.NoConfirmado:
                    return "5-No confirmado";
                case EstadoDeCoordinacionEnum.NoCoordinado:
                    return "5-No coordinado";
                default:
                    throw new ArgumentOutOfRangeException(nameof(@enum), @enum, null);
            }
        }
    }
}