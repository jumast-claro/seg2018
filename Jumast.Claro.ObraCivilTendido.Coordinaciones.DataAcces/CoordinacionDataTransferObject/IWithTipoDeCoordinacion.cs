namespace Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataTransferObject
{
    public interface IWithTipoDeCoordinacion
    {
        IWithContratista WithTipoDeCoordinacion(string tipoDeCoordinacion);
    }
}