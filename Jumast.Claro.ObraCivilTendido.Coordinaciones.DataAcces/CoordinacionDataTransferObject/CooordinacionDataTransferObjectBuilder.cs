namespace Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataTransferObject
{
    public sealed class CooordinacionDataTransferObjectBuilder : 
        IWithId, 
        IWithEnlace,
        IWithTipoDeCoordinacion, 
        IWithContratista, 
        IWithFecha, 
        IWithEstado, 
        IWithTareaPendiente, 
        IWithCoordinador, 
        IWithResponsable, 
        IWithResultado, 
        IWithResponsableResultado, 
        IWithObservacionesResultado, 
        IBuild
    {
        //-------------------------------------------------------------------//
        // Private fields
        //-------------------------------------------------------------------//
        private readonly CoordinacionCsvLine.CoordinacionDataTransferObject _dataTransferObject;

        //-------------------------------------------------------------------//
        // Constructor
        //-------------------------------------------------------------------//
        private CooordinacionDataTransferObjectBuilder()
        {
            _dataTransferObject = new CoordinacionCsvLine.CoordinacionDataTransferObject();
        }

        //-------------------------------------------------------------------//
        // Public methods
        //-------------------------------------------------------------------//
        public static IWithId Init()
        {
            return new CooordinacionDataTransferObjectBuilder();
        }

        public IWithEnlace WithId(string id)
        {
            _dataTransferObject.Id = id;
            return this;
        }

        public IWithTipoDeCoordinacion WithEnlace(string enlace)
        {
            _dataTransferObject.Enlace = enlace;
            return this;
        }

        public IWithContratista WithTipoDeCoordinacion(string tipoDeCoordinacion)
        {
            _dataTransferObject.TipoDeTarea = tipoDeCoordinacion;
            return this;
        }

        public IWithFecha WithContratista(string contratista)
        {
            _dataTransferObject.Contratista = contratista;
            return this;
        }

        public IWithEstado WithFecha(string fecha)
        {
            _dataTransferObject.Fecha = fecha;
            return this;
        }

        public IWithTareaPendiente WithEstado(string estado)
        {
            _dataTransferObject.Estado = estado;
            return this;
        }

        public IWithCoordinador WithTareaPendiente(string tareaPendiente)
        {
            _dataTransferObject.TareaPendiente = tareaPendiente;
            return this;
        }

        public IWithResponsable WithCoordinador(string coordinador)
        {
            _dataTransferObject.Coordinador = coordinador;
            return this;
        }

        public IWithResultado WithResponsable(string responsable)
        {
            _dataTransferObject.Responsable = responsable;
            return this;
        }

        public IWithResponsableResultado WithResultado(string resultado)
        {
            _dataTransferObject.Resultado = resultado;
            return this;
        }

        public IWithObservacionesResultado WithResponsableResultado(string responsableResultado)
        {
            _dataTransferObject.ResponsableResultado = responsableResultado;
            return this;
        }

        public IBuild WithObservacionesResultado(string observacionesResultado)
        {
            _dataTransferObject.ObservacionesResultado = observacionesResultado;
            return this;
        }

        public CoordinacionCsvLine.CoordinacionDataTransferObject Build() 
        {
            return _dataTransferObject;
        }

      
    }
}