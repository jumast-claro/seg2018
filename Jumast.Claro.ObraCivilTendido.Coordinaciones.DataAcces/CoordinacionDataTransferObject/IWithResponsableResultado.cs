namespace Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataTransferObject
{
    public interface IWithResponsableResultado
    {
        IWithObservacionesResultado WithResponsableResultado(string responsableResultado);
    }
}