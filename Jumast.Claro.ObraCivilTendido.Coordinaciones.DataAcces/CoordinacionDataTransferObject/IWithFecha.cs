namespace Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataTransferObject
{
    public interface IWithFecha
    {
        IWithEstado WithFecha(string fecha);
    }
}