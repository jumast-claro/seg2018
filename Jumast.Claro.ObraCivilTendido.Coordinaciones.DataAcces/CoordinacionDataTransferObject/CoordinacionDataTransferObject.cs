namespace Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionCsvLine
{
    /// <summary>
    /// Representa una l�nea del archivo csv en el que se guardan las coordinaciones. No valida ni procesa los campos de ninguna manera, s�lo hace de contenedor para los campos de cada l�nea.
    /// </summary>
    public class CoordinacionDataTransferObject
    {
        public string Id { get; set; }
        public string Enlace { get; set; }
        public string TipoDeTarea { get; set; }
        public string Contratista { get; set; }
        public string Fecha { get; set; }
        public string Estado { get; set; }
        public string TareaPendiente { get; set; }
        public string Coordinador { get; set; }
        public string Responsable { get; set; }
        public string Resultado { get; set; }
        public string ResponsableResultado { get; set; }
        public string ObservacionesResultado { get; set; }
    }
}