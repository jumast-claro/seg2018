namespace Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataTransferObject
{
    public interface IWithTareaPendiente
    {
        IWithCoordinador WithTareaPendiente(string tareaPendiente);
    }
}