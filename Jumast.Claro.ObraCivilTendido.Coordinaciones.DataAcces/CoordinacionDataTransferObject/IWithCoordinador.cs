namespace Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataTransferObject
{
    public interface IWithCoordinador
    {
        IWithResponsable WithCoordinador(string coordinador);
    }
}