namespace Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataTransferObject
{
    public interface IWithId
    {
        IWithEnlace WithId(string id);
    }

    public interface IWithEnlace
    {
        IWithTipoDeCoordinacion WithEnlace(string enlace);
    }
}