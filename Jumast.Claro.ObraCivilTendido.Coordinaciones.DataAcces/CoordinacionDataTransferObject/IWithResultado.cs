namespace Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataTransferObject
{
    public interface IWithResultado
    {
        IWithResponsableResultado WithResultado(string resultado);
    }
}