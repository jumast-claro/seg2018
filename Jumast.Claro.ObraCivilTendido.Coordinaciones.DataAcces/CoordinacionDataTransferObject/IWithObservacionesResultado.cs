namespace Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataTransferObject
{
    public interface IWithObservacionesResultado
    {
        IBuild WithObservacionesResultado(string observacionesResultado);
    }
}