namespace Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataTransferObject
{
    public interface IWithContratista
    {
        IWithFecha WithContratista(string contratista);
    }
}