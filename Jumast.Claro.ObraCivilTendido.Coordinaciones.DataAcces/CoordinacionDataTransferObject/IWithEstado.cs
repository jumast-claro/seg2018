namespace Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataTransferObject
{
    public interface IWithEstado
    {
        IWithTareaPendiente WithEstado(string estado);
    }
}