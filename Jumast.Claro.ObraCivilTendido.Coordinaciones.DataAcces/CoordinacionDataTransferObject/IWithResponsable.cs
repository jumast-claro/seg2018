namespace Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataTransferObject
{
    public interface IWithResponsable
    {
        IWithResultado WithResponsable(string responsable);
    }
}