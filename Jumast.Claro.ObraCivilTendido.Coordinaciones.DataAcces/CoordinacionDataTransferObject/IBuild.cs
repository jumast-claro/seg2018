namespace Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataTransferObject
{
    public interface IBuild
    {
        CoordinacionCsvLine.CoordinacionDataTransferObject Build();
    }
}