﻿using System.Collections.Generic;
using Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataModel;

namespace Jumast.Sisifo.Coordinacion.DataAcces
{
    public interface ICoordinacionesRepository
    {
        IEnumerable<CoordinacionDataModel.CoordinacionDataModel> ObtenerTodo();
        List<CoordinacionDataModel.CoordinacionDataModel> Obtener();
        void Guardar();
        void Agregar(CoordinacionDataModel.CoordinacionDataModel dataModel);
        void RemoverConId(string id);

        string GetUniqueId();
        void Guardar(string id, CoordinacionDataModel.CoordinacionDataModel dataModel);
    }
}