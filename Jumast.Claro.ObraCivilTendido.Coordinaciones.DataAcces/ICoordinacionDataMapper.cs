﻿namespace Jumast.Sisifo.Coordinacion.DataAcces
{
    public interface ICoordinacionDataMapper
    {
        CoordinacionDataModel.CoordinacionDataModel MapDataTransferObjectToDataModel(CoordinacionCsvLine.CoordinacionDataTransferObject dto);
        CoordinacionCsvLine.CoordinacionDataTransferObject MapDataModelToDataTransferObject(CoordinacionDataModel.CoordinacionDataModel dataModel);
    }
}