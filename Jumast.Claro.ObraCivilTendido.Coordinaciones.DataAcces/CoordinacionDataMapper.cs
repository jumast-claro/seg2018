﻿using System;
using System.Globalization;
using Jumast.Sisifo.Coordinacion.DataAcces;
using Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionCsvLine;
using Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataModel;
using Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataTransferObject;

namespace Jumast.Claro.ObraCivilTendido.DataAcces.ExcelRepository
{
    public sealed class CoordinacionDataMapper : ICoordinacionDataMapper
    {
        private DateTime? parseDate(string strFecha)
        {
            DateTime fecha;
            if (DateTime.TryParseExact(strFecha, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out fecha))
            {
                return fecha;
            }
            else
            {
                return null;
            }
        }

        private string dateToString(DateTime? fecha)
        {
            return fecha?.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) ?? "";
        }

        private readonly TipoDeTareaEnumConverter _tipoDeTareaEnumConverter = new TipoDeTareaEnumConverter();
        private readonly EstadoDeCoordinacionEnumConverter _estadoDeCoordinacionEnumConverter = new EstadoDeCoordinacionEnumConverter();
        private readonly ResponsableEnumConverter _responsableEnumConverter = new ResponsableEnumConverter();

        public CoordinacionDataModel MapDataTransferObjectToDataModel(CoordinacionDataTransferObject dto)
        {
            var dataModelBuilder = CooordinacionDataModelBuilder.Init()
                .WithId(dto.Id)
                .WithEnlace(dto.Enlace)
                .WithTipoDeCoordinacion(_tipoDeTareaEnumConverter.StringToEnum(dto.TipoDeTarea))
                .WithContratista(dto.Contratista)
                .WithFecha(parseDate(dto.Fecha))
                .WithEstado(_estadoDeCoordinacionEnumConverter.ToEnum(dto.Estado))
                .WithTareaPendiente(dto.TareaPendiente)
                .WithCoordinador(dto.Coordinador)
                .WithResponsable(_responsableEnumConverter.StringToEnum(dto.Responsable))
                .WithResultado(dto.Resultado)
                .WithResponsableResultado(dto.ResponsableResultado)
                .WithObservacionesResultado(dto.ObservacionesResultado);
            var dataModel =  dataModelBuilder.Build();
            return dataModel;
        }

        public CoordinacionDataTransferObject MapDataModelToDataTransferObject(CoordinacionDataModel dataModel)
        {
            var dataTransferObjectBuilder = CooordinacionDataTransferObjectBuilder.Init()
                .WithId(dataModel.Id)
                .WithEnlace(dataModel.Enlace)
                .WithTipoDeCoordinacion(_tipoDeTareaEnumConverter.EnumToString(dataModel.TipoDeTarea))
                .WithContratista(dataModel.Contratista)
                .WithFecha(dateToString(dataModel.Fecha))
                .WithEstado(_estadoDeCoordinacionEnumConverter.ToString(dataModel.Estado))
                .WithTareaPendiente(dataModel.TareaPendiente)
                .WithCoordinador(dataModel.Coordinador)
                .WithResponsable(_responsableEnumConverter.EnumToString(dataModel.Responsable))
                .WithResultado(dataModel.Resultado)
                .WithResponsableResultado(dataModel.ResponsableResultado)
                .WithObservacionesResultado(dataModel.ObservacionesResultado);

            var dto =  dataTransferObjectBuilder.Build();
            dto.Id = dataModel.Id;

            return dto;
        }
    }
}