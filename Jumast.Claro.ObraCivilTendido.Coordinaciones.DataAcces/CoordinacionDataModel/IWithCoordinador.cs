namespace Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataModel
{
    public interface IWithCoordinador
    {
        IWithResponsable WithCoordinador(string coordinador);
    }
}