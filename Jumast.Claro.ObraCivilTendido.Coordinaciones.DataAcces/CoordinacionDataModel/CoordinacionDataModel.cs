﻿using System;
using System.Dynamic;
using Jumast.Sisifo.Coordinacion.Common;

namespace Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataModel
{

    public  class CoordinacionDataModel
    {
        internal CoordinacionDataModel()
        {
            
        }

        public string Id { get; internal set; }
        public string Enlace { get; set; }
        public TipoDeCoordinacionEnum TipoDeTarea { get; set; }
        public string Contratista { get; set; }
        public DateTime? Fecha { get; set; }
        public EstadoDeCoordinacionEnum Estado { get; set; }
        public string TareaPendiente { get; set; }
        public string Coordinador { get; set; }
        public ResponsableDeCoordinacionEnum Responsable { get; set; }

        public string Resultado { get; set; }
        public string ResponsableResultado { get; set; }
        public string ObservacionesResultado { get; set; }

    }
}
