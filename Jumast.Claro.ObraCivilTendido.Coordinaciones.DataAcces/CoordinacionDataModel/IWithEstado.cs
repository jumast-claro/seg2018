using Jumast.Sisifo.Coordinacion.Common;

namespace Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataModel
{
    public interface IWithEstado
    {
        IWithTareaPendiente WithEstado(EstadoDeCoordinacionEnum estado);
    }
}