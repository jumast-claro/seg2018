using Jumast.Sisifo.Coordinacion.Common;

namespace Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataModel
{
    public interface IWithTipoDeCoordinacion
    {
        IWithContratista WithTipoDeCoordinacion(TipoDeCoordinacionEnum tipoDeCoordinacion);
    }
}