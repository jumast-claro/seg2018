namespace Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataModel
{
    public interface IWithTareaPendiente
    {
        IWithCoordinador WithTareaPendiente(string tareaPendiente);
    }
}