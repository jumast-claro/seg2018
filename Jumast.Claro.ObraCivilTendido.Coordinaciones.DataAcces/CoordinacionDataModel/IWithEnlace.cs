namespace Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataModel
{
    public interface IWithEnlace
    {
        IWithTipoDeCoordinacion WithEnlace(string enlace);
    }

    public interface IWithId
    {
        IWithEnlace WithId(string id);
    }
}