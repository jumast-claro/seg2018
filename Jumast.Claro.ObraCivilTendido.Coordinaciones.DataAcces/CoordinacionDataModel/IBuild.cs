namespace Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataModel
{
    public interface IBuild
    {
        CoordinacionDataModel Build();
    }
}