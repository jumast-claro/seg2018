using System;

namespace Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataModel
{
    public interface IWithFecha
    {
        IWithEstado WithFecha(DateTime? fecha);
    }
}