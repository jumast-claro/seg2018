namespace Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataModel
{
    public interface IWithResponsableResultado
    {
        IWithObservacionesResultado WithResponsableResultado(string responsableResultado);
    }
}