namespace Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataModel
{
    public interface IWithResultado
    {
        IWithResponsableResultado WithResultado(string resultado);
    }
}