using Jumast.Sisifo.Coordinacion.Common;

namespace Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataModel
{
    public interface IWithResponsable
    {
        IWithResultado WithResponsable(ResponsableDeCoordinacionEnum responsable);
    }
}