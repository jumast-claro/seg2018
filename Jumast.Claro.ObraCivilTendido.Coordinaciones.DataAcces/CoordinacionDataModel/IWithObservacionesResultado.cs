namespace Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataModel
{
    public interface IWithObservacionesResultado
    {
        IBuild WithObservacionesResultado(string observacionesResultado);
    }
}