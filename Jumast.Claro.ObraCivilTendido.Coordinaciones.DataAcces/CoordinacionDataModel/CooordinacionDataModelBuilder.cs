using System;
using Jumast.Sisifo.Coordinacion.Common;
using Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataTransferObject;

namespace Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataModel
{
    public sealed class CooordinacionDataModelBuilder : 
        IWithId,
        IWithEnlace, 
        IWithTipoDeCoordinacion, 
        IWithContratista, 
        IWithFecha, 
        IWithEstado, 
        IWithTareaPendiente, 
        IWithCoordinador, 
        IWithResponsable, 
        IWithResultado, 
        IWithResponsableResultado, 
        IWithObservacionesResultado, 
        IBuild
    {
        //-------------------------------------------------------------------//
        // Private fields
        //-------------------------------------------------------------------//
        private readonly CoordinacionDataModel _coordinacionDataModel;

        //-------------------------------------------------------------------//
        // Constructor
        //-------------------------------------------------------------------//
        private CooordinacionDataModelBuilder()
        {
            _coordinacionDataModel = new CoordinacionDataModel {Id = new Guid().ToString()};
        }

        //-------------------------------------------------------------------//
        // Public methods
        //-------------------------------------------------------------------//
        public static IWithId Init()
        {
            return new CooordinacionDataModelBuilder();
        }

        public IWithEnlace WithId(string id)
        {
            _coordinacionDataModel.Id = id;
            return this;
        }


        public IWithTipoDeCoordinacion WithEnlace(string enlace)
        {
            _coordinacionDataModel.Enlace = enlace;
            return this;
        }

        public IWithContratista WithTipoDeCoordinacion(TipoDeCoordinacionEnum tipoDeCoordinacion)
        {
            _coordinacionDataModel.TipoDeTarea = tipoDeCoordinacion;
            return this;
        }

        public IWithFecha WithContratista(string contratista)
        {
            _coordinacionDataModel.Contratista = contratista;
            return this;
        }

        public IWithEstado WithFecha(DateTime? fecha)
        {
            _coordinacionDataModel.Fecha = fecha;
            return this;
        }

        public IWithTareaPendiente WithEstado(EstadoDeCoordinacionEnum estado)
        {
            _coordinacionDataModel.Estado = estado;
            return this;
        }

        public IWithCoordinador WithTareaPendiente(string tareaPendiente)
        {
            _coordinacionDataModel.TareaPendiente = tareaPendiente;
            return this;
        }

        public IWithResponsable WithCoordinador(string coordinador)
        {
            _coordinacionDataModel.Coordinador = coordinador;
            return this;
        }

        public IWithResultado WithResponsable(ResponsableDeCoordinacionEnum responsable)
        {
            _coordinacionDataModel.Responsable = responsable;
            return this;
        }

        public IWithResponsableResultado WithResultado(string resultado)
        {
            _coordinacionDataModel.Resultado = resultado;
            return this;
        }

        public IWithObservacionesResultado WithResponsableResultado(string responsableResultado)
        {
            _coordinacionDataModel.ResponsableResultado = responsableResultado;
            return this;
        }

        public IBuild WithObservacionesResultado(string observacionesResultado)
        {
            _coordinacionDataModel.ObservacionesResultado = observacionesResultado;
            return this;
        }

        public CoordinacionDataModel Build()
        {
            return _coordinacionDataModel;
        }

       
    }
}