namespace Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataModel
{
    public interface IWithContratista
    {
        IWithFecha WithContratista(string contratista);
    }
}