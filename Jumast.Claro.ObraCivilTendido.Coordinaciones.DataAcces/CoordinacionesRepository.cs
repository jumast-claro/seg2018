﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jumast.Sisifo.Coordinacion.DataAcces;
using Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionCsvLine;
using Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataModel;
using Jumast.Sisifo.Coordinacion.DataAcces.CoordinacionDataTransferObject;
using Syncfusion.XlsIO;

namespace Jumast.Claro.ObraCivilTendido.DataAcces.ExcelRepository
{
    public class CoordinacionesRepository : ICoordinacionesRepository
    {
        private const int ENLACE = 1;
        private const int TIPO_DE_TAREA = 2;
        private const int CONTRATISTA = 3;
        private const int FECHA = 4;
        private const int ESTADO = 5;
        private const int TAREA_PENDIENTE = 6;
        private const int COORDINADOR = 7;
        private const int RESPONSABLE = 8;

        private const int RESULTADO = 9;
        private const int RESULTADO_RESPONSABLE = 10;
        private const int RESULTADO_OBSERVACIONES = 11;
        private const int ID = 12;


        private readonly string _rutaCompletaConExtension;

        private List<CoordinacionDataModel> _coordinaciones = new List<CoordinacionDataModel>();
        readonly ICoordinacionDataMapper _coordinacionDataMapper = new CoordinacionDataMapper();

        //-------------------------------------------------------------------//
        // Constructor
        //-------------------------------------------------------------------//
        public CoordinacionesRepository(string rutaCompletaConExtension)
        {
            _rutaCompletaConExtension = rutaCompletaConExtension;
        }

        //-------------------------------------------------------------------//
        // Public Methods
        //-------------------------------------------------------------------//
        public IEnumerable<CoordinacionDataModel> ObtenerTodo()
        {
            return _coordinaciones;
        }

        public void Agregar(CoordinacionDataModel dataModel)
        {
            if(_coordinaciones.Exists(c => c.Id == dataModel.Id)) return;
            _coordinaciones.Add(dataModel);
        }

        public void RemoverConId(string id)
        {
            var coordinacion = _coordinaciones.Find(c => c.Id == id);

            if (coordinacion == null)
            {
                throw new ArgumentException();
            }
            _coordinaciones.Remove(coordinacion);
        }

        public string GetUniqueId()
        {
            string id = Guid.NewGuid().ToString();
            //if(_coordinaciones.Exists(c => c.Id == id)) throw new ArgumentException();
            return id;
        }

        public void Guardar(string id, CoordinacionDataModel dataModel)
        {
            RemoverConId(id);
            _coordinaciones.Add(dataModel);
        }


        private CoordinacionDataTransferObject getDataTransferObject(IWorksheet ws, int i)
        {

            var id = ws.Range[i, ID].Value;
            //var id = GetUniqueId();

            var enlace = ws.Range[i, ENLACE].Value;
            var tipoDeTarea = ws.Range[i, TIPO_DE_TAREA].Value;
            var contratista = ws.Range[i, CONTRATISTA].Value;
            var estado = ws.Range[i, ESTADO].Value;
            var tareaPendiente = ws.Range[i, TAREA_PENDIENTE].Value;
            var coordinador = ws.Range[i, COORDINADOR].Value;
            var responsable = ws.Range[i, RESPONSABLE].Value;
            var strFecha = ws.Range[i, FECHA].Value;

            var coordinacionesDataTransferObject = new CoordinacionDataTransferObject()
            {
                Id = id,

                Enlace = enlace,
                TipoDeTarea = tipoDeTarea,
                Contratista = contratista,
                Estado = estado,
                TareaPendiente = tareaPendiente,
                Coordinador = coordinador,
                Responsable = responsable,
                Fecha = strFecha,

                Resultado = ws.Range[i, RESULTADO].Text,
                ResponsableResultado = ws.Range[i, RESULTADO_RESPONSABLE].Text,
                ObservacionesResultado = ws.Range[i, RESULTADO_OBSERVACIONES].Text
            };

            return coordinacionesDataTransferObject;
        }

        public List<CoordinacionDataModel> Obtener()
        {
            ExcelEngine excelEngine = new ExcelEngine();
            IApplication application = excelEngine.Excel;
            IWorkbook wb = application.Workbooks.Open(_rutaCompletaConExtension);
            IWorksheet ws = wb.Worksheets[0];
            var rowCount = ws.Rows.Length;


            var dic = new Dictionary<string, CoordinacionDataModel>();

            for (int i = 1; i <= rowCount; i++)
            {
                var coordinacionesDataTransferObject = getDataTransferObject(ws, i);
                var coordinacionDataModel = _coordinacionDataMapper.MapDataTransferObjectToDataModel(coordinacionesDataTransferObject);

                dic.Add(coordinacionDataModel.Id, coordinacionDataModel);

               
            }

            wb.Close(true);
            excelEngine.Dispose();

            _coordinaciones = dic.Values.ToList();
            return _coordinaciones;

        }

        public void Guardar()
        {
            ExcelEngine excelEngine = new ExcelEngine();
            IApplication application = excelEngine.Excel;
            IWorkbook wb = application.Workbooks.Create(1);
            IWorksheet ws = wb.Worksheets[0];

            int i = 1;
            foreach (var coordinacion in _coordinaciones)
            {
                var dto = _coordinacionDataMapper.MapDataModelToDataTransferObject(coordinacion);

                ws.Range[i, ENLACE].Text = dto.Enlace;
                ws.Range[i, TIPO_DE_TAREA].Text = dto.TipoDeTarea;
                ws.Range[i, CONTRATISTA].Text = dto.Contratista;
                ws.Range[i, ESTADO].Value = dto.Estado;
                ws.Range[i, TAREA_PENDIENTE].Text = dto.TareaPendiente;
                ws.Range[i, COORDINADOR].Text = dto.Coordinador;
                ws.Range[i, FECHA].Text = dto.Fecha;
                ws.Range[i, RESPONSABLE].Text = dto.Responsable;

                ws.Range[i, RESULTADO].Text = dto.Resultado;
                ws.Range[i, RESULTADO_RESPONSABLE].Text = dto.ResponsableResultado;
                ws.Range[i, RESULTADO_OBSERVACIONES].Text = dto.ObservacionesResultado;

                ws.Range[i, ID].Text = dto.Id;

                i++;
            }

            ws.Range[1, 1, ws.Rows.Length, 7].WrapText = false;
            wb.SaveAs(_rutaCompletaConExtension);
            wb.Close();
            excelEngine.Dispose();
        }
    }
}
