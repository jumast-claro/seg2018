﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic.FileIO;

namespace Jumast.Sisifo.Contratista.DataAcces
{
    public class CoordinadoresCsvRepository : ICoordinadoresRepository
    {
        private readonly string _fullFilePathWithExtension;

        /// <summary>
        /// Inicializa una nueva instancia del repositorio.
        /// </summary>
        /// <param name="fullFilePathWithExtension">Nombre completo con extensión del archivo csv que tiene la información.
        /// Por ejemplo: @"C:\Users\Jumast\Desktop\SEG2018_DATA\contratistas_coordinadores.csv
        /// </param>
        public CoordinadoresCsvRepository(string fullFilePathWithExtension)
        {
            _fullFilePathWithExtension = fullFilePathWithExtension;
        }

        /// <summary>
        /// Devuelve una lista con los nombres completos de los coordinadores de las contratistas.
        /// </summary>
        /// <exception>Todas las que puedan ocurrir al leer el archivoi csv.</exception>
        public List<string> NombresDeSupervisores()
        {
            var nombres = new List<string>();
            using (var parser = new TextFieldParser(_fullFilePathWithExtension, Encoding.UTF8))
            {
                parser.SetDelimiters(",");
                while (!parser.EndOfData)
                {
                    string[] fields = parser.ReadFields();
                    var nombre = fields[0];
                    nombres.Add(nombre);
                }
            }
            return nombres;
        }
    }
}
