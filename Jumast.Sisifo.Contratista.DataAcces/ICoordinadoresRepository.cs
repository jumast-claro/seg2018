﻿using System.Collections.Generic;

namespace Jumast.Sisifo.Contratista.DataAcces
{
    public interface ICoordinadoresRepository
    {
        List<string> NombresDeSupervisores();
    }
}