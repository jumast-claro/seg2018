﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Syncfusion.CompoundFile.XlsIO.Native;
using Syncfusion.XlsIO;
using Syncfusion.XlsIO.Parser.Biff_Records;

namespace Jumast.Claro.ObraCivilTendido.Notas
{
    public class InfoAdicional
    {
        public string Id { get; set; }
        public string Etapa { get; set; } 
        public string Proceso { get; set; }
        public string Subestado { get; set; }
        public string Detalle { get; set; }
        public string Observaciones { get; set; }
        public string Coordinador { get; set; }
        public string AnalistaMdD { get; set; }
        public string TareaPendiente { get; set; }
        public DateTime? Fecha { get; set; }
    }

    public class InfoAdicionalRepository
    {
        private const string DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm";
        private readonly CultureInfo INVARIANT_CULTURE = CultureInfo.InvariantCulture;
        private const int ID = 1;
        private const int ETAPA = 2;
        private const int ESTADO = 3;
        private const int PROCESO = 4;
        private const int COORDINADOR = 5;
        private const int ANALISTA_MDD = 6;
        private const int RESPONSABLE = 7;
        private const int FECHA = 8;
        private const int TAREAPENDIENTE = 9;

        //-------------------------------------------------------------------//
        // Private fields
        //-------------------------------------------------------------------//
        private readonly string _rutaCompletaConExtension;
        private readonly List<InfoAdicional> _lista = new List<InfoAdicional>();

        //-------------------------------------------------------------------//
        // Private methods
        //-------------------------------------------------------------------//
        private List<InfoAdicional> leerExcel(string rutaCompletaConExtension)
        {
            ExcelEngine excelEngine = new ExcelEngine();
            IApplication application = excelEngine.Excel;
            IWorkbook wb = application.Workbooks.Open(rutaCompletaConExtension);
            IWorksheet ws = wb.Worksheets[0];
            var rowCount = ws.Rows.Length;

            var ans = new List<InfoAdicional>();
            for (int i = 1; i <= rowCount; i++)
            {
                var info = leerFila(ws, i);
                ans.Add(info);
            }

            wb.Close(true);
            excelEngine.Dispose();
            return ans;
        }
        private InfoAdicional leerFila(IWorksheet ws, int i)
        {
            var id = ws.Range[i, ID].Value;
            var etapa = ws.Range[i, ETAPA].Value;
            var estado = ws.Range[i, ESTADO].Value;
            var subestado = ws.Range[i, PROCESO].Value;
            var coordinador = ws.Range[i, COORDINADOR].Value;
            var analistaMdD = ws.Range[i, ANALISTA_MDD].Value;
            //var observaciones = ws.Range[i, OBSERVACIONES].Value;
            var detalle = ws.Range[i, RESPONSABLE].Value;
            var tareaPendiente = ws.Range[i, TAREAPENDIENTE].Value;


            var infoAdicional = new InfoAdicional()
            {
                Id = id,
                Etapa = etapa,
                Proceso = estado,
                Subestado = subestado,
                Coordinador = coordinador,
                AnalistaMdD = analistaMdD,
                Detalle = detalle,
                TareaPendiente = tareaPendiente

            };

            var strFecha = ws.Range[i, FECHA].Value;
            DateTime fecha;
            if (DateTime.TryParseExact(strFecha, DATE_TIME_FORMAT, INVARIANT_CULTURE, DateTimeStyles.None, out fecha))
            {
                infoAdicional.Fecha = fecha;
            }
            else
            {
                infoAdicional.Fecha = null;
            }

            return infoAdicional;
        }

        private void guardar(IEnumerable<InfoAdicional> datos, string rutaCompletaConExtension)
        {
            ExcelEngine excelEngine = new ExcelEngine();
            IApplication application = excelEngine.Excel;
            //IWorkbook wb = application.Workbooks.Open(_rutaCompletaConExtension);
            IWorkbook wb = application.Workbooks.Create(1);
            //wb.Worksheets.Create();
            IWorksheet ws = wb.Worksheets[0];
            var rowCount = ws.Rows.Length;


            int i = 1;
            foreach (var info in datos)
            {
                ws.Range[i, ID].Text = info.Id;
                ws.Range[i, ETAPA].Text = info.Etapa;
                ws.Range[i, ESTADO].Value = info.Proceso;
                ws.Range[i, PROCESO].Text = info.Subestado;
                ws.Range[i, COORDINADOR].Text = info.Coordinador;
                ws.Range[i, ANALISTA_MDD].Text = info.AnalistaMdD;
                //ws.Range[i, OBSERVACIONES].Text = info.Observaciones;
                ws.Range[i, RESPONSABLE].Text = info.Detalle;
                ws.Range[i, TAREAPENDIENTE].Text = info.TareaPendiente;

                ws.Range[i, FECHA].Text = info.Fecha?.ToString(DATE_TIME_FORMAT, INVARIANT_CULTURE) ?? "";
                i++;
            }

            ws.Range[1, 1, ws.Rows.Length, 7].WrapText = false;
            //wb.Worksheets[0].Remove();
            wb.SaveAs(rutaCompletaConExtension);

            wb.Close();
            excelEngine.Dispose();
        }

        //-------------------------------------------------------------------//
        // Constructor
        //-------------------------------------------------------------------//
        public InfoAdicionalRepository(string rutaCompletaConExtension)
        {
            _rutaCompletaConExtension = rutaCompletaConExtension;
        }

        //-------------------------------------------------------------------//
        // Public methods
        //-------------------------------------------------------------------//
        public IEnumerable<InfoAdicional> ObtenerTodo()
        {
            return _lista;
        }

        public List<InfoAdicional> Obtener()
        {
            return leerExcel(_rutaCompletaConExtension);
        }

        public void Guardar(IEnumerable<InfoAdicional> infoAdicional)
        {
            guardar(infoAdicional, _rutaCompletaConExtension);
        }
    }
}
