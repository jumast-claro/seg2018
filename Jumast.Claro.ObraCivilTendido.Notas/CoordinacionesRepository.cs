﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Syncfusion.XlsIO;

namespace Jumast.Claro.ObraCivilTendido.Notas
{
    //public class CoordinacionesRepository
    //{
    //    private const int ID = 1;
    //    private const int TIPO_DE_TAREA = 2;
    //    private const int CONTRATISTA = 3;
    //    private const int FECHA = 4;
    //    private const int ESTADO = 5;
    //    private const int TAREA_PENDIENTE = 6;
    //    private const int COORDINADOR = 7;
    //    private const int RESPONSABLE = 8;

    //    private const int RESULTADO = 9;
    //    private const int RESULTADO_RESPONSABLE = 10;
    //    private const int RESULTADO_OBSERVACIONES = 11;


    //    private readonly string _rutaCompletaConExtension;

    //    private List<CoordinacionDataModel> _coordinaciones = new List<CoordinacionDataModel>();

    //    //-------------------------------------------------------------------//
    //    // Constructor
    //    //-------------------------------------------------------------------//
    //    public CoordinacionesRepository(string rutaCompletaConExtension)
    //    {
    //        _rutaCompletaConExtension = rutaCompletaConExtension;
    //    }

    //    //-------------------------------------------------------------------//
    //    // Public Methods
    //    //-------------------------------------------------------------------//
    //    public IEnumerable<CoordinacionDataModel> ObtenerTodo()
    //    {
    //        return _coordinaciones;
    //    }

    //    public List<CoordinacionDataModel> Obtener()
    //    {
    //        ExcelEngine excelEngine = new ExcelEngine();
    //        IApplication application = excelEngine.Excel;
    //        IWorkbook wb = application.Workbooks.Open(_rutaCompletaConExtension);
    //        //wb.Worksheets.Create();
    //        IWorksheet ws = wb.Worksheets[0];
    //        var rowCount = ws.Rows.Length;


    //        var ans = new List<CoordinacionDataModel>();

    //        for (int i = 1; i <= rowCount; i++)
    //        {
    //            var id = ws.Range[i, ID].Value;
    //            var tipoDeTarea = ws.Range[i, TIPO_DE_TAREA].Value;

               

    //            var contratista = ws.Range[i, CONTRATISTA].Value;
    //            var estado = ws.Range[i, ESTADO].Value;



    //            var tareaPendiente = ws.Range[i, TAREA_PENDIENTE].Value;
    //            var coordinador = ws.Range[i, COORDINADOR].Value;
    //            var responsable = ws.Range[i, RESPONSABLE].Value;

    //            var coordinacionesDataModel = new CoordinacionDataModel()
    //            {
    //                Id = id,
    //                TipoDeTarea = tipoDeTarea,
    //                Contratista = contratista,
    //                Estado = estado,
    //                TareaPendiente = tareaPendiente,
    //                Coordinador = coordinador,
    //                Responsable = responsable,

    //                Resultado = ws.Range[i, RESULTADO].Text,
    //                ResponsableResultado = ws.Range[i, RESULTADO_RESPONSABLE].Text,
    //                ObservacionesResultado = ws.Range[i, RESULTADO_OBSERVACIONES].Text
    //            };

    //            var strFecha = ws.Range[i, FECHA].Value;
    //            DateTime fecha;
    //            if (DateTime.TryParseExact(strFecha, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out fecha))
    //            {
    //                coordinacionesDataModel.Fecha = fecha;
    //            }
    //            else
    //            {
    //                coordinacionesDataModel.Fecha = null;
    //            }

    //            ans.Add(coordinacionesDataModel);
    //        }

    //        wb.Close(true);
    //        excelEngine.Dispose();
    //        return ans;

    //    }

    //    public void Guardar(IEnumerable<CoordinacionDataModel> coordinaciones)
    //    {
    //        ExcelEngine excelEngine = new ExcelEngine();
    //        IApplication application = excelEngine.Excel;
    //        //IWorkbook wb = application.Workbooks.Open(_rutaCompletaConExtension);
    //        IWorkbook wb = application.Workbooks.Create(1);
    //        //wb.Worksheets.Create();
    //        IWorksheet ws = wb.Worksheets[0];
    //        var rowCount = ws.Rows.Length;


    //        int i = 1;
    //        foreach (var coordinacion in coordinaciones)
    //        {
    //            ws.Range[i, ID].Text = coordinacion.Id;
    //            ws.Range[i, TIPO_DE_TAREA].Text = coordinacion.TipoDeTarea;
    //            ws.Range[i, CONTRATISTA].Text = coordinacion.Contratista;
    //            ws.Range[i, ESTADO].Value = coordinacion.Estado;
    //            ws.Range[i, TAREA_PENDIENTE].Text = coordinacion.TareaPendiente;
    //            ws.Range[i, COORDINADOR].Text = coordinacion.Coordinador;
    //            ws.Range[i, FECHA].Text = coordinacion.Fecha?.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) ?? "";
    //            ws.Range[i, RESPONSABLE].Text = coordinacion.Responsable;

    //            ws.Range[i, RESULTADO].Text = coordinacion.Resultado;
    //            ws.Range[i, RESULTADO_RESPONSABLE].Text = coordinacion.ResponsableResultado;
    //            ws.Range[i, RESULTADO_OBSERVACIONES].Text = coordinacion.ObservacionesResultado;

    //            i++;
    //        }

    //        ws.Range[1, 1, ws.Rows.Length, 7].WrapText = false;
    //        wb.SaveAs(_rutaCompletaConExtension);
    //        wb.Close();
    //        excelEngine.Dispose();
    //    }
    //}
}
