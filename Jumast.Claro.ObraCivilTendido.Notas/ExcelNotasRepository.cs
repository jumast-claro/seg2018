﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using Microsoft.VisualBasic.FileIO;
using Syncfusion.XlsIO;

namespace Jumast.Claro.ObraCivilTendido.Notas
{
    public class ExcelNotasRepository : INotasRepository
    {
        //-------------------------------------------------------------------//
        // Private fields
        //-------------------------------------------------------------------//
        private readonly string _rutaCompletaConExtension;
        private readonly List<INota> _notas = new List<INota>();

        //-------------------------------------------------------------------//
        // Private methods
        //-------------------------------------------------------------------//
        private void leerNotas(string rutaCompletaConExtension)
        {
            ExcelEngine excelEngine = new ExcelEngine();
            IApplication application = excelEngine.Excel;
            IWorkbook wb = application.Workbooks.Open(rutaCompletaConExtension);
            IWorksheet ws = wb.Worksheets[0];
            var rowCount = ws.Rows.Length;

            for (int i = 1; i <= rowCount; i++)
            {
                var enlace = ws.Range[i, 1].Value;
                var fecha = ws.Range[i, 2].DateTime;
                var nota = ws.Range[i, 3].Value;

                var notaModel = new NotaModel()
                {
                    Enlace = enlace,
                    Fecha = fecha,
                    Nota = nota
                };

                _notas.Add(notaModel);
            }
        }

        private void guardarNotas(string rutaCompletaConExtension)
        {
            ExcelEngine excelEngine = new ExcelEngine();
            IApplication application = excelEngine.Excel;
            IWorkbook wb = application.Workbooks.Open(rutaCompletaConExtension);
            wb.Worksheets.Create();
            IWorksheet ws = wb.Worksheets[1];

            int i = 1;
            foreach (var nota in _notas)
            {
                ws.Range[i, 1].Value = nota.Enlace;
                ws.Range[i, 2].Value2 = nota.Fecha;
                ws.Range[i, 3].Value = nota.Nota;
                i++;
            }


            ws.Range[1, 1, ws.Rows.Length, 3].WrapText = false;
            wb.Worksheets[0].Remove();
            wb.Save();
        }


        //-------------------------------------------------------------------//
        // Constructor
        //-------------------------------------------------------------------//
        public ExcelNotasRepository(string rutaCompletaConExtension)
        {
            _rutaCompletaConExtension = rutaCompletaConExtension;
            leerNotas(rutaCompletaConExtension);
        }

        //-------------------------------------------------------------------//
        // Public methods
        //-------------------------------------------------------------------//
        public void Guardar()
        {
            guardarNotas(_rutaCompletaConExtension);
        }

        public void AgregarNota(INota nota)
        {
            _notas.Add(nota);
        }

        public void EliminarNota(INota nota)
        {
            _notas.Remove(nota);
        }

        public IEnumerable<INota> TodasLasNotas()
        {
            return _notas;
        }

        public IEnumerable<INota> TodasLasNotasParaEnlace(string enlace)
        {
            var r = _notas.Where(n => n.Enlace == enlace);
            return r;
        }
    }
}

