﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using CsvHelper;
using Syncfusion.XlsIO;

namespace Jumast.Claro.ObraCivilTendido.Notas
{
    public class CsvNotasRepository : INotasRepository
    {
        private const string DELIMITER = ",";
        private const bool HAS_HEADER_RECORD = false;
        private const string DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm:ss";
        private readonly CultureInfo INVARIANT_CULTURE = CultureInfo.InvariantCulture;

        //-------------------------------------------------------------------//
        // Private fields
        //-------------------------------------------------------------------//
        private readonly string _rutaCompletaConExtension;
        private readonly List<INota> _notas = new List<INota>();
      

        //-------------------------------------------------------------------//
        // Private methods
        //-------------------------------------------------------------------//
        private void leerNotas(string rutaCompletaConExtension)
        {
            using (var reader = new StreamReader(rutaCompletaConExtension))
            {
                using (var csvReader = new CsvReader(reader))
                {
                    csvReader.Configuration.HasHeaderRecord = HAS_HEADER_RECORD;
                    csvReader.Configuration.Delimiter = DELIMITER;
                    while (csvReader.Read())
                    {
                        _notas.Add(getRecord(csvReader));
                    }
                };
            };
        }
        private NotaModel getRecord(CsvReader csvReader)
        {
            var enlace = csvReader.GetField(0);
            var strFecha = csvReader.GetField(1);
            var nota = csvReader.GetField(2);
            DateTime fecha = DateTime.ParseExact(strFecha, DATE_TIME_FORMAT, INVARIANT_CULTURE);

            var notaModel = new NotaModel()
            {
                Enlace = enlace,
                Fecha = fecha,
                Nota = nota
            };

            return notaModel;
        }

        private void guardarNotas(string rutaCompletaConExtension)
        {
            using (var writer = new StreamWriter(rutaCompletaConExtension))
            {
                using (var csvWriter = new CsvWriter(writer))
                {
                    csvWriter.Configuration.Delimiter = DELIMITER;
                    csvWriter.Configuration.HasHeaderRecord = HAS_HEADER_RECORD;

                    foreach (var nota in _notas)
                    {
                      writeRecord(csvWriter, nota); 
                    }
                }

            }
        }
        private void writeRecord(CsvWriter csvWriter, INota nota)
        {
            csvWriter.WriteField(nota.Enlace);
            csvWriter.WriteField(nota.Fecha.ToString(DATE_TIME_FORMAT, INVARIANT_CULTURE));
            csvWriter.WriteField(nota.Nota);
            csvWriter.NextRecord();
        }


        //-------------------------------------------------------------------//
        // Constructor
        //-------------------------------------------------------------------//
        public CsvNotasRepository(string rutaCompletaConExtension)
        {
            _rutaCompletaConExtension = rutaCompletaConExtension;
            leerNotas(_rutaCompletaConExtension);
        }

        //-------------------------------------------------------------------//
        // Public methods
        //-------------------------------------------------------------------//
        public void Guardar()
        {
            guardarNotas(_rutaCompletaConExtension);
        }

        public void AgregarNota(INota nota)
        {
            _notas.Add(nota);
        }

        public void EliminarNota(INota nota)
        {
            _notas.Remove(nota);
        }

        public IEnumerable<INota> TodasLasNotas()
        {
            return _notas;
        }

        public IEnumerable<INota> TodasLasNotasParaEnlace(string enlace)
        {
            var r = _notas.Where(n => n.Enlace == enlace);
            return r;
        }
    }
}