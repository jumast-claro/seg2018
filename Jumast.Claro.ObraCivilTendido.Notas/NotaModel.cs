﻿using System;
using System.Text;
using System.Threading.Tasks;

namespace Jumast.Claro.ObraCivilTendido.Notas
{
    public class NotaModel : INota
    {
        public string Enlace { get; set; }
        public string Nota { get; set; }
        public DateTime Fecha { get; set; }


    }
}
