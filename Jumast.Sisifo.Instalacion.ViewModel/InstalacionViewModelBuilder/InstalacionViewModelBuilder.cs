﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using Jumast.Sisifo.Contratista.DataAcces;
using Jumast.Sisifo.Instalacion.Common;
using Jumast.Sisifo.MesaDeDespacho.DataAcces;
using Jumast.Wpf.Mvvm.BindableProperties;
using Microsoft.VisualBasic.FileIO;

namespace Jumast.Sisifo.Instalacion.ViewModel
{
    public class InstalacionViewModelBuilder :
        IWithNumeroDeEnlace,
        IWithNumeroDeOrden,
        IWithCliente,
        IWithDireccion,
        IWithPartido,
        IWithFechaDeRecibido,
        IWithFechaDeCompromiso,
        IWithTipoDeOrden,
        IWithTecnologia,
        IWithContratistaEnCurso,
        IWithDiasDeRecibido,
        IWithFechaDeCierre,
        IWithAnalistaInstalacionesFO,
        IWithEtapa,
        IWithEstado,
        IWithTareaPendiente,
        IWithFecha,
        IWithRelevador,
        IWithAnalistaMdD,
        IWithResponsable,
        IWithObservaciones,
        IWithHiperlink,
        IBuild


    {
        private readonly InstalacionViewModel _product;
        private List<string> _analistasMdD;
        private List<string> _coordinadores;
        private  EtapaEnumProperty _etapaProperty;

        private InstalacionViewModelBuilder()
        {
            _product = new InstalacionViewModel();
        }

        internal List<string> AnalistasMdD
        {
            get { return _analistasMdD; }
            set { _analistasMdD = value; }
        }

        public List<string> Coordinadores
        {
            get { return _coordinadores; }
            set { _coordinadores = value; }
        }


        public static IWithNumeroDeEnlace Init(IMdDRepository mddRepo, ICoordinadoresRepository coordinadoresRepo)
        {
            var builder = new InstalacionViewModelBuilder();
            builder.AnalistasMdD = mddRepo.NombresDeAnalistas();
            builder.Coordinadores = coordinadoresRepo.NombresDeSupervisores();
            return builder;
        }

        public IWithNumeroDeOrden WithEnlace(string enlace)
        {
            _product.NumeroDeEnlaceProperty = new ReadWriteProperty<string>(enlace);
            return this;
        }

        public IWithCliente WithNumeroDeOrden(string orden)
        {
            _product.NumeroDeOrdenProperty = new ReadWriteProperty<string>(orden);
            return this;
        }

        public IWithDireccion WithCliente(string cliente)
        {
            _product.ClienteProperty = new ReadWriteProperty<string>(cliente);
            return this;
        }

        public IWithPartido WithDireccion(string direccion)
        {
            _product.DireccionProperty = new ReadWriteProperty<string>(direccion);
            return this;
        }

        public IWithFechaDeRecibido WithPartido(string partido)
        {
            _product.PartidoProperty = new ReadWriteProperty<string>(partido);
            return this;
        }

        public IWithFechaDeCompromiso WithFechaDeRecibido(DateTime? fechaDeRecibido)
        {
            _product.FechaDeRecibidoProperty = new ReadOnlyFechaProperty(fechaDeRecibido);
            return this;
        }

        public IWithTipoDeOrden WithFechaDeCompromio(DateTime? fechaDeCompromiso)
        {
            _product.FechaDeCompromisoVantiveProperty = new ReadWriteProperty<DateTime?>(fechaDeCompromiso);
            return this;
        }

        public IWithTecnologia WithTipoDeOrden(string tipoDeOrden)
        {
            _product.TipoDeOrdenProperty = new ReadWriteProperty<string>(tipoDeOrden);
            return this;
        }

        public IWithContratistaEnCurso WithTecnologia(string tecnologia)
        {
            _product.TecnologiaProperty = new ReadWriteProperty<string>(tecnologia);
            return this;
        }

        public IWithDiasDeRecibido WithContratistaEnCurso(string contratista)
        {
            _product.ContratistaEnCursoProperty = new ReadWriteProperty<string>(contratista);
            return this;
        }

        public IWithFechaDeCierre WithDiasDeRecibido(double? diasDeRecibido)
        {
            _product.DiasDeRecibidoProperty = new ReadWriteProperty<double?>(diasDeRecibido);
            return this;
        }

        public IWithAnalistaInstalacionesFO WithFechaDeCierre(DateTime? fechaDeCierre)
        {
            _product.FechaDeCierreProperty = new ReadOnlyFechaProperty(fechaDeCierre);
            return this;
        }

        public IBuild WithAnalistaInstalacionesFO(string analistaInstalacionesFO)
        {
            _product.AnalistaInstalacionesFOProperty = new ReadWriteProperty<string>(analistaInstalacionesFO);
            return this;
        }

        

        IWithEtapa IWithAnalistaInstalacionesFO.WithAnalistaInstalacionesFO(string analistaInstalacionesFO)
        {
            _product.AnalistaInstalacionesFOProperty = new ReadWriteProperty<string>(analistaInstalacionesFO);
            return this;
        }

        public IWithEstado WithEtapa(EtapaEnum etapa)
        {
            var etapaProperty = new EtapaEnumProperty(etapa);
            _product.EtapaProperty = etapaProperty;
            _etapaProperty = etapaProperty;

            return this;
        }

        public IWithResponsable WithEstado(string estado)
        {
            var estadoProperty = new EstadoProperty(estado);
            _product.EstadoProperty = estadoProperty;
            estadoProperty.AllowedValuesDependOn(_etapaProperty);
            return this;
        }

        public IWithTareaPendiente WithResponsable(string responsableClaro)
        {
            _product.ResponsableProperty = new ResponsableProperty(responsableClaro);
            _product.ResponsableProperty.AllowedValuesDependOn(_product.EstadoProperty);
            return this;
        }

        public IWithFecha WithTareaPendiente(string tareaPendiente)
        {
            _product.TareaPendienteProperty = new TareaPendienteProperty(tareaPendiente);
            _product.TareaPendienteProperty.AllowedValuesDependOn(_product.ResponsableProperty);
            return this;
        }

        public IWithRelevador WithFecha(DateTime? fecha)
        {
            _product.FechaProperty = new ReadWriteProperty<DateTime?>(fecha);
            return this;
        }

        public IWithAnalistaMdD WithRelevador(string responsableContratista)
        {
            _product.CoordinadorProperty = new CoordinadorProperty(responsableContratista, this.Coordinadores);
            return this;
        }

        public IWithObservaciones WithAnalistaMdD(string analistaMdD)
        {
            _product.AnalistaMdDProperty = new AnalistaMdDProperty(analistaMdD, AnalistasMdD);
            return this;
        }


        public IWithHiperlink WithObservaciones(string observaciones)
        {
            _product.ObservacionesProperty = new ReadWriteProperty<string>(observaciones);
            return this;
        }

        public IBuild WithHiperlink(string hyperlink)
        {
            _product.HyperlinkProperty = new ReadWriteProperty<string>(hyperlink);
            return this;
        }

        public InstalacionViewModel Build()
        {
            var descripcion = $"{_product.ClienteProperty.Value} ({_product.DireccionProperty.Value}, {_product.PartidoProperty.Value}) Orden:{_product.NumeroDeOrdenProperty.Value}_Enlace:{_product.NumeroDeEnlaceProperty.Value} | {_product.TecnologiaProperty.Value}";
            _product.DescripcionProperty = new ReadOnlyProperty<string>(descripcion);
            return _product;
        }
    }
}