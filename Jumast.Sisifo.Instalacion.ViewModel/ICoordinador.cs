﻿using Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel;

namespace Jumast.Sisifo.Instalacion.ViewModel
{
    public interface ICoordinador
    {
        CoordinacionViewModel InferirCoordinacion(InstalacionViewModel instalacionViewModel);
    }
}