﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jumast.Sisifo.Contratista.DataAcces;
using Jumast.Sisifo.Coordinacion.Common;
using Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel;
using Jumast.Sisifo.Instalacion.Common;

namespace Jumast.Sisifo.Instalacion.ViewModel
{
    public sealed class Coordinador : ICoordinador
    {
        private readonly ICoordinadoresRepository _repo;

        public Coordinador(ICoordinadoresRepository repo)
        {
            _repo = repo;
        }

        public CoordinacionViewModel InferirCoordinacion(InstalacionViewModel instalacionViewModel)
        {


            var etapa = instalacionViewModel.EtapaProperty.Value;
            var tipoDeCoordinacion = map(etapa);

            var coordinacionViewModelBuilder = CoordinacionViewModelBuilder.Init(_repo)
                .WithCliente(instalacionViewModel.ClienteProperty.Value)
                .WithDireccion(instalacionViewModel.DireccionProperty.Value)
                .WithPartido(instalacionViewModel.DireccionProperty.Value)
                .WithEnlace(instalacionViewModel.NumeroDeEnlaceProperty.Value) //
                .WithTipoDeCoordinacion(tipoDeCoordinacion) //
                .WithContratista(instalacionViewModel.ContratistaEnCursoProperty.Value)
                .WithFecha(instalacionViewModel.FechaProperty.Value ?? DateTime.Today)
                .WithEstado(EstadoDeCoordinacionEnum.Coordinacion)
                .WithTareaPendiente("")
                .WithCoordinador(instalacionViewModel.CoordinadorProperty.Value)
                .WithAnalistaMdD(instalacionViewModel.AnalistaMdDProperty.Value)
                .WithResponsable(ResponsableDeCoordinacionEnum.InstalacionesFO)
                .WithResultado("")
                .WithResponsableResultado("")
                .WithObservacionesResultado("");
            var coordinacionViewModel = coordinacionViewModelBuilder.Build();

            return coordinacionViewModel;
        }

        private TipoDeCoordinacionEnum map(EtapaEnum etapa)
        {
            switch (etapa)
            {
                case EtapaEnum.indefinida:
                    return TipoDeCoordinacionEnum.Relevamiento;
                case EtapaEnum.Analisis:
                    return TipoDeCoordinacionEnum.Relevamiento;
                case EtapaEnum.DefinicionDeProyecto:
                    return TipoDeCoordinacionEnum.Relevamiento;
                case EtapaEnum.AsignacionAContratista:
                    return TipoDeCoordinacionEnum.Relevamiento;
                case EtapaEnum.Relevamiento:
                    return TipoDeCoordinacionEnum.Relevamiento;
                case EtapaEnum.Sondeos:
                    return TipoDeCoordinacionEnum.Sondeos;
                case EtapaEnum.Permisos:
                    return TipoDeCoordinacionEnum.Relevamiento;
                case EtapaEnum.ObraCivil:
                    return TipoDeCoordinacionEnum.ObraCivil;
                case EtapaEnum.ObraCivilInterna:
                    return TipoDeCoordinacionEnum.ObraCivilInterna;
                case EtapaEnum.Tendido:
                    return TipoDeCoordinacionEnum.Tendido;
                case EtapaEnum.Empalmes:
                    return TipoDeCoordinacionEnum.Empalmes;
                case EtapaEnum.EmpalmesME:
                    return TipoDeCoordinacionEnum.EmpalmesME;
                case EtapaEnum.EmpalmesGPON:
                    return TipoDeCoordinacionEnum.EmpalmesGPON;
                case EtapaEnum.Mudanza:
                    return TipoDeCoordinacionEnum.Mudanza;
                case EtapaEnum.Terminada:
                    return TipoDeCoordinacionEnum.Relevamiento;
                default:
                    throw new ArgumentException();
            }
        }
    }
}
