﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Jumast.Sisifo.Coordinacion.Common;
using Jumast.Sisifo.Coordinacion.ViewModels;
using Jumast.Sisifo.Coordinacion.ViewModels.CoordinacionViewModel;
using Jumast.Sisifo.Instalacion.Common;
using Jumast.Wpf.Mvvm.BindableProperties;

namespace Jumast.Sisifo.Instalacion.ViewModel
{

    public interface IWithNumeroDeEnlace
    {
        IWithNumeroDeOrden WithEnlace(string enlace);
    }

    public interface IWithNumeroDeOrden
    {
        IWithCliente WithNumeroDeOrden(string orden);
    }

    public interface IWithCliente
    {
        IWithDireccion WithCliente(string cliente);
    }

    public interface IWithDireccion
    {
        IWithPartido WithDireccion(string direccion);
    }

    public interface IWithPartido
    {
        IWithFechaDeRecibido WithPartido(string partido);
    }

    public interface IWithFechaDeRecibido
    {
        IWithFechaDeCompromiso WithFechaDeRecibido(DateTime? fechaDeRecibido);
    }

    public interface IWithFechaDeCompromiso
    {
        IWithTipoDeOrden WithFechaDeCompromio(DateTime? FechaDeCompromiso);
    }

    public interface IWithTipoDeOrden
    {
        IWithTecnologia WithTipoDeOrden(string tipoDeOrden);
    }

    public interface IWithTecnologia
    {
        IWithContratistaEnCurso WithTecnologia(string tecnologia);
    }

    public interface IWithContratistaEnCurso
    {
        IWithDiasDeRecibido WithContratistaEnCurso(string contratista);
    }

    public interface IWithDiasDeRecibido
    {
        IWithFechaDeCierre WithDiasDeRecibido(double? diasDeRecibido);
    }

    public interface IWithFechaDeCierre
    {
        IWithAnalistaInstalacionesFO WithFechaDeCierre(DateTime? fechaDeCierre);
    }

    public interface IWithAnalistaInstalacionesFO
    {
        IWithEtapa WithAnalistaInstalacionesFO(string analistaInstalacionesFO);
    }

    public interface IWithEtapa
    {
        IWithEstado WithEtapa(EtapaEnum etapa);
    }

    public interface IWithEstado
    {
        IWithResponsable WithEstado(string estado);
    }

    public interface IWithResponsable
    {
        IWithTareaPendiente WithResponsable(string responsableClaro);
    }

    public interface IWithTareaPendiente
    {
        IWithFecha WithTareaPendiente(string tareaPendiente);
    }

    public interface IWithFecha
    {
        IWithRelevador WithFecha(DateTime? fecha);
    }


    public interface IWithRelevador
    {
        IWithAnalistaMdD WithRelevador(string responsableContratista);
    }

    public interface IWithAnalistaMdD
    {
        IWithObservaciones WithAnalistaMdD(string analistaMdD);
    }

   

    public interface IWithObservaciones
    {
        IWithHiperlink WithObservaciones(string observaciones);
    }

    public interface IWithHiperlink
    {
        IBuild WithHiperlink(string hyperlink);
    }


    public interface IBuild
    {
        InstalacionViewModel Build();
    }

    public class InstalacionViewModel
    {

        internal InstalacionViewModel()
        {
            
        }

        //-------------------------------------------------------------------//
        // Public Methods
        //-------------------------------------------------------------------//
        public void AgregarCoordinacion(CoordinacionViewModel coordinacionViewModel)
        {

            sincronizarConCoordinacion(coordinacionViewModel);
            Coordinaciones.Add(coordinacionViewModel);
        }
        private void sincronizarConCoordinacion(CoordinacionViewModel coordinacionViewModel)
        {
            coordinacionViewModel.EstadoProperty.PropertyChanged += (sender, args) =>
            {
                if (!esUltima(coordinacionViewModel))
                {
                    return;
                }

                if (EtapaProperty.StringValue == coordinacionViewModel.TipoDeTareaProperty.StringValue)
                {
                    EstadoProperty.Value = coordinacionViewModel.EstadoProperty.StringValue;
                }
            };

            coordinacionViewModel.FechaProperty.PropertyChanged += (sender, args) =>
            {
                if (!esUltima(coordinacionViewModel))
                {
                    return;
                }

                if (EtapaProperty.StringValue == coordinacionViewModel.TipoDeTareaProperty.StringValue)
                {
                    FechaProperty.Value = coordinacionViewModel.FechaProperty.Value;
                }
            };

            coordinacionViewModel.ResponsableProperty.PropertyChanged += (sender, args) =>
            {


                if (!esUltima(coordinacionViewModel))
                {
                    return;
                }
                if (EtapaProperty.StringValue == coordinacionViewModel.TipoDeTareaProperty.StringValue)
                {
                    ResponsableProperty.Value = coordinacionViewModel.ResponsableProperty.StringValue;
                }

            };


            coordinacionViewModel.TareaPendienteProperty.PropertyChanged += (sender, args) =>
            {

                if (!esUltima(coordinacionViewModel))
                {
                    return;
                }

                if (EtapaProperty.StringValue == coordinacionViewModel.TipoDeTareaProperty.StringValue)
                {
                    TareaPendienteProperty.Value = coordinacionViewModel.TareaPendienteProperty.Value;
                }
            };
        }
        private bool esUltima(CoordinacionViewModel coordinacionViewModel)
        {
            var candidatos = new List<CoordinacionViewModel>(Coordinaciones)
                .Where(c => c.TipoDeTareaProperty.Value == coordinacionViewModel.TipoDeTareaProperty.Value)
                //.Where(c => c.EstadoProperty.Value == EstadoDeCoordinacionEnum.Coordinado || c.EstadoProperty.Value == EstadoDeCoordinacionEnum.Coordinacion)
                .OrderBy(c => c.FechaProperty.Value);

            if (!candidatos.Any())
            {
                return false;
            }

            var coordinacion = candidatos.Last();
            return coordinacion == coordinacionViewModel;
        }

        public CoordinacionesDataGridViewModel Coordinaciones { get; set; } = new CoordinacionesDataGridViewModel();

        //-------------------------------------------------------------------//
        // Bindable properties
        //-------------------------------------------------------------------//
        public IBindableProperty<string> NumeroDeEnlaceProperty { get; internal set; }
        public IBindableProperty<string> NumeroDeOrdenProperty { get; internal set; }
        public IBindableProperty<string> ClienteProperty { get; internal set; }
        public IBindableProperty<string> DireccionProperty { get; internal set; }
        public IBindableProperty<string> PartidoProperty { get; internal set; }
        public ReadOnlyFechaProperty FechaDeRecibidoProperty { get; internal set; }
        public IBindableProperty<DateTime?> FechaDeCompromisoVantiveProperty { get; internal set; }
        public IBindableProperty<string> TipoDeOrdenProperty { get; internal set; }
        public IBindableProperty<string> TecnologiaProperty { get; set; }
        public IBindableProperty<string> ContratistaEnCursoProperty { get; internal set; }
        public IBindableProperty<double?> DiasDeRecibidoProperty { get; internal set; }
        public ReadOnlyFechaProperty FechaDeCierreProperty { get; internal set; }
        public IBindableProperty<string> AnalistaInstalacionesFOProperty { get; internal set; }
        public IBindableProperty<string> HyperlinkProperty { get; internal set; }
        //public IBindableProperty<string> VencidaProperty { get; internal set; }

        // Info adicional
        public IEnumBindableProperty<EtapaEnum> EtapaProperty { get; internal set; }
        public EstadoProperty EstadoProperty { get; internal set; }
        public TareaPendienteProperty TareaPendienteProperty { get; internal set; }
        public IBindableProperty<DateTime?> FechaProperty { get; internal set; }
        public CoordinadorProperty CoordinadorProperty { get; internal set; }
        public AnalistaMdDProperty AnalistaMdDProperty { get; internal set; }
        public ResponsableProperty ResponsableProperty { get; internal set; }
        public IBindableProperty<string> ObservacionesProperty { get; internal set; }

        public ReadOnlyProperty<string> DescripcionProperty { get; internal set; }

        public IBindableProperty<bool> IsChecked { get; set; } = new ReadWriteProperty<bool>(false);
    }
}
