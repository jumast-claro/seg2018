﻿using System.Collections.Generic;
using Jumast.Wpf.Mvvm.BindableProperties;

namespace Jumast.Sisifo.Instalacion.ViewModel
{
    public sealed class ProcesoProperty : ReadWritePropertyWithRestrictedValues<string>
    {
        public ProcesoProperty(string initialVvalue) : base(initialVvalue)
        {
            _allowedValues = new List<string>()
            {
                "En proceso",
                "Interrumpida"
            };
        }
    }
}
