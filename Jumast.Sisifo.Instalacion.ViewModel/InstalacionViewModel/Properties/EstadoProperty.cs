﻿using System;
using System.Collections.Generic;
using Jumast.Sisifo.Instalacion.Common;
using Jumast.Wpf.Mvvm.BindableProperties;

namespace Jumast.Sisifo.Instalacion.ViewModel
{
    public sealed class EstadoProperty : ReadWritePropertyWithRestrictedValues<string>
    {
        private EtapaEnumProperty _etapaProperty;
        private readonly Dictionary<EtapaEnum, List<string>> _estados = new Dictionary<EtapaEnum, List<string>>()
        {
            {EtapaEnum.Analisis, new List<string>(){"", "1-Pendiente", "7-Semi interrumpido", "8-Interrumpido"}},
            {EtapaEnum.DefinicionDeProyecto, new List<string>(){"", "1-Pendiente"}},
            {EtapaEnum.AsignacionAContratista, new List<string>(){"", "1-Pendiente"}},

            {EtapaEnum.Relevamiento, new List<string>(){"", "1-Pendiente", "2-Coordinación", "3-Coordinado", "5-Ejecución", "6-Terminado", "7-Semi interrumpido", "8-Interrumpido"}},
            {EtapaEnum.Sondeos, new List<string>(){"", "1-Pendiente", "2-Coordinación", "3-Coordinado", "5-Ejecución", "6-Terminado"}},
            {EtapaEnum.Permisos, new List<string>(){"", "1-Pendiente", "8-Interrumpido"}},

            {EtapaEnum.ObraCivil, new List<string>(){"", "1-Pendiente", "2-Coordinación", "3-Coordinado", "4-Informado en cronograma","5-Ejecución", "6-Terminado", "7-Semi interrumpido", "8-Interrumpido"}},
            {EtapaEnum.ObraCivilInterna, new List<string>(){"", "1-Pendiente", "2-Coordinación", "3-Coordinado", "4-Informado en cronograma","5-Ejecución", "6-Terminado", "7-Semi interrumpido", "8-Interrumpido"}},

            {EtapaEnum.Tendido, new List<string>(){"", "1-Pendiente", "2-Coordinación", "3-Coordinado", "4-Informado en cronograma","5-Ejecución", "6-Terminado", "7-Semi interrumpido", "8-Interrumpido"}},
            {EtapaEnum.Empalmes, new List<string>(){"", "1-Pendiente", "2-Coordinación", "3-Coordinado", "4-Informado en cronograma","5-Ejecución", "6-Terminado", "7-Semi interrumpido", "8-Interrumpido"}},
            {EtapaEnum.EmpalmesME, new List<string>(){"", "1-Pendiente", "2-Coordinación", "3-Coordinado", "4-Informado en cronograma","5-Ejecución", "6-Terminado", "7-Semi interrumpido", "8-Interrumpido"}},
            {EtapaEnum.EmpalmesGPON, new List<string>(){"", "1-Pendiente", "2-Coordinación", "3-Coordinado", "4-Informado en cronograma","5-Ejecución", "6-Terminado", "7-Semi interrumpido", "8-Interrumpido"}},
            {EtapaEnum.Mudanza, new List<string>(){"", "1-Pendiente", "2-Coordinación", "3-Coordinado", "4-Informado en cronograma","5-Ejecución", "6-Terminado", "7-Semi interrumpido", "8-Interrumpido"}},
            {EtapaEnum.Terminada, new List<string>(){"", "1-Pendiente", "2-Sacar de plana"}}
        };

        public EstadoProperty(string initialVvalue) : base(initialVvalue)
        {
        }



        public override IEnumerable<string> AllowedValues
        {
            get
            {
                var etapa = this._etapaProperty.Value;
                var estado = _estados.ContainsKey(etapa) ? _estados[etapa] : null;
                return estado;
            }
        }

        public void AllowedValuesDependOn(EtapaEnumProperty etapaProperty)
        {
            if (_etapaProperty != null)
            {
                throw new InvalidOperationException();
            }
            _etapaProperty = etapaProperty;
            etapaProperty.PropertyChanged += (sender, args) =>
            {
                if (etapaProperty.Value == EtapaEnum.Analisis)
                {
                    this.Value = "Pendiente";
                }
                else
                {
                    this.Value = "";
                }
            };
        }

    }
}