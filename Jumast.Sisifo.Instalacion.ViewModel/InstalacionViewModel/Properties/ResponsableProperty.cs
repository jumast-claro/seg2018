﻿using System;
using System.Collections.Generic;
using Jumast.Wpf.Mvvm.BindableProperties;

namespace Jumast.Sisifo.Instalacion.ViewModel
{
    public sealed class ResponsableProperty : ReadWritePropertyWithRestrictedValues<string>
    {
        private EstadoProperty _estadoProperty;

        private readonly Dictionary<string, List<string>> _responsables = new Dictionary<string, List<string>>()
        {
            {"1-Pendiente", new List<string>(){"","Ingeniería", "PM", "MdD", "Cliente","EECC","Comercial", "Administración FO", "Gestión de Ingresos","Instalaciones FO", "Permisos", "Contratista", "Cycsa"} },
            {"2-Coordinación", new List<string>(){"", "PM", "MdD", "Cliente", "Instalaciones FO", "Inst. FO","Gestión de Ingresos","Contratista"} },
            {"3-Coordinado", new List<string>(){""}},
            {"4-Informado en cronograma", new List<string>(){""}},
            {"5-Ejecución", new List<string>(){""}},
            {"6-Terminado", new List<string>(){"","Contratista", "Instalaciones FO", "Inst. FO"}},
            {"7-Semi interrumpido", new List<string>(){"","Ingeniería", "PM", "MdD", "Cliente", "EECC","Comercial", "Administración FO", "Gestión de Ingresos","Instalaciones FO", "Permisos", "Contratista", "Cycsa"} },
            {"8-Interrumpido", new List<string>(){"","Ingeniería", "PM", "MdD", "Cliente", "EECC","Comercial", "Administración FO", "Gestión de Ingresos","Instalaciones FO", "Permisos", "Contratista", "Cycsa"} },

            {"", new List<string>(){"", "Pendiente MdD", "Pendiente contratista", "Pendiente Inst. FO"} },
            //{"Pendiente Ingeniería", new List<string>(){""} },
            //{"Pendiente Consultoría", new List<string>(){""} },
            //{"Pendiente Admin. FO", new List<string>(){""}},
            //{"Pendiente MdD", new List<string>(){"", "Primer contacto con cliente", "Informar requisitos", "Confirmar fecha", "Ok documentación", "Datos de contacto", "Autorización del cliente", "Sitio en construcción", "Sala de rack en construcción", "Provisión de rack", "No confirmado"} },
            //{"Pendiente Inst. FO", new List<string>(){"", "Asignar a contratista", "Proponer fecha a MdD", "Confirmar fecha a contratista", "Analizar informe", "Solicitar documentación", "Enviar documentación","Coordinar con cliente", "Consultar estado"} },
            //{"Pendiente contratista", new List<string>(){"", "Coordinar", "Asignar coordinador","Proponer fecha", "Confirmar fecha","Nómina de personal", "Enviar documentación","Informe de relevamiento", "Cierre de tendido", "Cierre de empalmes","Informe de sondeos", "Enviar informe"} },


            //{"Éxito", new List<string>(){"", "Pendiente MdD", "Pendiente Inst. FO", "Pendiente contratista"} },
            //{"Fracaso", new List<string>(){"", "Pendiente MdD", "Pendiente Inst. FO", "Pendiente contratista"} },
            //{"Terminado", new List<string>(){"","Pendiente Inst. FO", "Pendiente contratista"} }
        };

        public ResponsableProperty(string initialVvalue) : base(initialVvalue)
        {
        }

        public override IEnumerable<string> AllowedValues
        {
            get
            {
                var estado = _estadoProperty.Value;
                var responsable = _responsables.ContainsKey(estado) ? _responsables[estado] : new List<string>() { "" };
                return responsable;
            }
        }

        public void AllowedValuesDependOn(EstadoProperty estadoProperty)
        {
            if (_estadoProperty != null)
            {
                throw new InvalidOperationException();
            }
            _estadoProperty = estadoProperty;
            estadoProperty.PropertyChanged += (sender, args) =>
            {
                this.Value = "";
            };

        }
    }
}
