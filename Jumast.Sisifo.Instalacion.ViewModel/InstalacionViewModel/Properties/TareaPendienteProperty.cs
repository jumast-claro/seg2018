﻿using System;
using System.Collections.Generic;
using Jumast.Wpf.Mvvm.BindableProperties;

namespace Jumast.Sisifo.Instalacion.ViewModel
{
    public sealed class TareaPendienteProperty : ReadWritePropertyWithRestrictedValues<string>
    {
        private ResponsableProperty _responsableProperty;

        private readonly Dictionary<string, List<string>> _tareaPendienteItemsSource = new Dictionary<string, List<string>>()
        {
            //{"Pendiente MdD", new List<string>(){"", "Primer contacto con cliente", "Informar requisitos", "Confirmar fecha", "Ok documentación", "Datos de contacto", "Autorización del cliente", "Sitio en construcción", "Sala de rack en construcción", "Provisión de rack", "No confirmado"} },
            //{"Pendiente Inst. FO", new List<string>(){"", "Asignar a contratista", "Proponer fecha a MdD", "Confirmar fecha a contratista", "Analizar informe", "Solicitar documentación", "Enviar documentación","Coordinar con cliente", "Consultar estado"} },
            //{"Pendiente contratista", new List<string>(){"", "Coordinar", "Asignar coordinador","Proponer fecha", "Confirmar fecha","Nómina de personal", "Enviar documentación","Informe de relevamiento", "Cierre de tendido", "Cierre de empalmes","Informe de sondeos", "Enviar informe"} },

            {"Ingeniería", new List<string>(){"", "Confirmar equipo existente", "Datos de contacto"}},
            {"Administración FO", new List<string>(){"", "Adelanto de tendido", "Asignación de FO", "Confirmar cable existente"}},
            {"MdD", new List<string>(){"", "Primer contacto con cliente", "Informar requisitos", "Confirmar fecha", "Ok documentación", "Datos de contacto", "Autorización del cliente", "Sitio en construcción", "Sala de rack en construcción", "Provisión de rack", "No confirmó fecha", "Respuesta del cliente"} },
            {"Cliente", new List<string>(){"", "Informar requisitos", "Confirmar fecha", "Ok documentación", "Datos de contacto", "Autorización", "Sitio en construcción", "Sala de rack en construcción", "Provisión de rack", "No confirmó fecha"} },
            {"PM", new List<string>(){"", "Primer contacto con cliente", "Informar requisitos", "Confirmar fecha", "Ok documentación", "Datos de contacto", "Autorización del cliente", "Sitio en construcción", "Sala de rack en construcción", "Provisión de rack", "No confirmó fecha"} },
            {"Instalaciones FO", new List<string>(){"", "Asignar a contratista", "Proponer fecha a MdD", "Confirmar fecha a contratista", "Analizar informe", "Solicitar documentación", "Enviar documentación","Coordinar con cliente", "Averiguar estado"} },
            {"Contratista", new List<string>(){"", "Coordinar", "Iniciar ejecución","Asignar relevador","Proponer fecha", "Confirmar fecha", "Confirmar asistencia","Nómina de personal", "Enviar documentación","Informe de relevamiento", "Cierre de tendido", "Cierre de empalmes","Informe de sondeos", "Enviar informe", "Informado en cronograma"} },
        };

        public TareaPendienteProperty(string initialVvalue) : base(initialVvalue)
        {
        }

        public override IEnumerable<string> AllowedValues
        {
            get
            {

                var responsable = _responsableProperty.Value;
                var tareaPendienteList = _tareaPendienteItemsSource.ContainsKey(responsable) ? _tareaPendienteItemsSource[responsable] : new List<string>() { "" };
                return tareaPendienteList;
            }
        }

        public void AllowedValuesDependOn(ResponsableProperty responsableProperty)
        {
            if (_responsableProperty != null)
            {
                throw new InvalidOperationException();
            }
            _responsableProperty = responsableProperty;
            responsableProperty.PropertyChanged += (sender, args) =>
            {
                this.Value = "";
            };

        }
    }
}
