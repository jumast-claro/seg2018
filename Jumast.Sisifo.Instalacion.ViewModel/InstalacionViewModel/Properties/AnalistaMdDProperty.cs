﻿using System.Collections.Generic;
using Jumast.Wpf.Mvvm.BindableProperties;

namespace Jumast.Sisifo.Instalacion.ViewModel
{
    public sealed class AnalistaMdDProperty : ReadWritePropertyWithRestrictedValues<string>
    {
        public AnalistaMdDProperty(string initialVvalue, IEnumerable<string> allowedValues) : base(initialVvalue)
        {
            _allowedValues = new List<string>(allowedValues);
        }
    }
}
