﻿using System;

namespace Jumast.Sisifo.Instalacion.ViewModel
{
    public class ReadOnlyFechaProperty
    {
        private readonly DateTime? _fecha;

        public ReadOnlyFechaProperty(DateTime? fecha)
        {
            _fecha = fecha;
        }

        public DateTime? Fecha => _fecha;

        public int? Annio => _fecha?.Year;

        public int? Mes => _fecha?.Month;
    }
}
