﻿using System.Collections.Generic;
using Jumast.Wpf.Mvvm.BindableProperties;

namespace Jumast.Sisifo.Instalacion.ViewModel
{
    public sealed class CoordinadorProperty : ReadWritePropertyWithRestrictedValues<string>
    {
        public CoordinadorProperty(string initialVvalue, IEnumerable<string> coordinadores) : base(initialVvalue)
        {
            _allowedValues = new List<string>(coordinadores);
        }
    }
}
