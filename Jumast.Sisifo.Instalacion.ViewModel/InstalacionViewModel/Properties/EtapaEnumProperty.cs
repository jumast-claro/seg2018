﻿using Jumast.Sisifo.Instalacion.Common;
using Jumast.Wpf.Mvvm.BindableProperties;

namespace Jumast.Sisifo.Instalacion.ViewModel
{
    public sealed class EtapaEnumProperty : EnumBindableProperty<EtapaEnum>
    {
        private readonly EtapaEnumConverter _enumConverter = new EtapaEnumConverter();

        public EtapaEnumProperty(EtapaEnum initialVvalue) : base(initialVvalue)
        {
        }

        protected override string EnumToString(EtapaEnum @enum)
        {
            return _enumConverter.EnumToString(@enum);
        }

        protected override EtapaEnum StringToEnum(string s)
        {
            return _enumConverter.StringToEnum(s);
        }
    }
}
