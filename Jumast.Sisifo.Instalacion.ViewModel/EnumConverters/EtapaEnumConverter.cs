﻿using System;
using Jumast.Sisifo.Instalacion.Common;

namespace Jumast.Sisifo.Instalacion.ViewModel
{
    public class EtapaEnumConverter
    {
        public string EnumToString(EtapaEnum @enum)
        {
            switch (@enum)
            {
                case EtapaEnum.indefinida:
                    return "";
                case EtapaEnum.Analisis:
                    return "1-Análisis";
                case EtapaEnum.DefinicionDeProyecto:
                    return "2-Definición de proyecto";
                case EtapaEnum.AsignacionAContratista:
                    return "3-Asignación a contratista";
                case EtapaEnum.Relevamiento:
                    return "4-Relevamiento";
                case EtapaEnum.Sondeos:
                    return "5-Sondeos";
                case EtapaEnum.Permisos:
                    return "6-Permisos";
                case EtapaEnum.ObraCivil:
                    return "7-Obra civil";
                case EtapaEnum.ObraCivilInterna:
                    return "7-Obra civil interna";
                case EtapaEnum.Tendido:
                    return "8-Tendido";
                case EtapaEnum.Empalmes:
                    return "9-Empalmes";
                case EtapaEnum.EmpalmesME:
                    return "9-Empalmes (ME)";
                case EtapaEnum.EmpalmesGPON:
                    return "9-Empalmes (GPON)";
                case EtapaEnum.Mudanza:
                    return "10-Mudanza";
                case EtapaEnum.Terminada:
                    return "11-Terminada";
                default:
                    throw new ArgumentOutOfRangeException(nameof(@enum), @enum, null);
            }
        }

        public EtapaEnum StringToEnum(string s)
        {
            switch (s)
            {
                case "":
                    return EtapaEnum.indefinida;
                case "1-Análisis":
                    return EtapaEnum.Analisis;
                case "2-Definición de proyecto":
                    return EtapaEnum.DefinicionDeProyecto;
                case "3-Asignación a contratista":
                    return EtapaEnum.AsignacionAContratista;
                case "4-Relevamiento":
                    return EtapaEnum.Relevamiento;
                case "5-Sondeos":
                    return EtapaEnum.Sondeos;
                case "6-Permisos":
                    return EtapaEnum.Permisos;
                case "7-Obra civil":
                    return EtapaEnum.ObraCivil;
                case "7-Obra civil interna":
                    return EtapaEnum.ObraCivilInterna;
                case "8-Tendido":
                    return EtapaEnum.Tendido;
                case "9-Empalmes":
                    return EtapaEnum.Empalmes;
                case "9-Empalmes (ME)":
                    return EtapaEnum.EmpalmesME;
                case "9-Empalmes (GPON)":
                    return EtapaEnum.EmpalmesGPON;
                case "10-Mudanza":
                    return EtapaEnum.Mudanza;
                case "11-Terminada":
                    return EtapaEnum.Terminada;
                default:
                    return EtapaEnum.indefinida;
            }
        }
    }
}
