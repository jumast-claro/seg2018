﻿using System;
using System.Globalization;
using Jumast.Claro.BusinessObject.Abstract;

namespace Jumast.Claro.BusinessObject.Mvvm
{
    public class FechaCompromisoProperty
    {
        private readonly IFechaDeCompromiso _fechaDeCompromiso;

        public FechaCompromisoProperty(IFechaDeCompromiso fechaDeCompromiso)
        {
            _fechaDeCompromiso = fechaDeCompromiso;
        }

        public DateTime? Fecha => _fechaDeCompromiso.Fecha;
        public int? TiempoRestanteEnDias => _fechaDeCompromiso.TiempoRestanteEnDias;
        public Vencida EstaVencida => _fechaDeCompromiso.EstaVencida;

        public string Texto => _fechaDeCompromiso.TieneFecha ? _fechaDeCompromiso.Fecha.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : string.Empty;
    }
}