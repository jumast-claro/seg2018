﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumast.IO.Csv
{
    public class ReflectiveCsvRepository<TDataModel>
    {
        private readonly string _path;
        private string _delimiter = ",";
        private bool _hasFieldsEnclosedInQuotes = false;

        public ReflectiveCsvRepository(string path, bool hasFieldsEnclosedInQuotes)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException($"No se encontró el archivo {path}");
            }
            _path = path;
            //_delimiter = delimiter;
            //_hasFieldsEnclosedInQuotes = hasFieldsEnclosedInQuotes;
        }

        public string Delimiter
        {
            get { return _delimiter; }
            set { _delimiter = value; }
        }

        public bool HasFieldsEnclosedInQuotes
        {
            get { return _hasFieldsEnclosedInQuotes; }
            set { _hasFieldsEnclosedInQuotes = value; }
        }

        public IEnumerable<TDataModel> SelectAll()
        {
            var resultSet = new List<TDataModel>();

            var csvReader = new CsvFileReader(_path, _delimiter, _hasFieldsEnclosedInQuotes);
            IEnumerable<string[]> fieldsArray = csvReader.ReadAllFields();

            var csvHelper = new CsvHelper();

            foreach (var fields in fieldsArray)
            {
                var dataTransferObject = Activator.CreateInstance<TDataModel>();
                csvHelper.SetValues(dataTransferObject, fields);
                resultSet.Add(dataTransferObject);
            }
            return resultSet;
        }

        public void SaveAll(IEnumerable<TDataModel> data)
        {
            List<List<KeyValuePair<int, string>>> list = new List<List<KeyValuePair<int, string>>>();
            var csvHelper = new CsvHelper();


            StringBuilder stringBuilder = new StringBuilder();
            foreach (var dto in data)
            {
                csvHelper.AddQuotesToDataTransferObject(dto);
                string line = csvHelper.createLine(dto);
                stringBuilder.AppendLine(line);
            }


            File.WriteAllText(_path, stringBuilder.ToString());
        }
    }
}
